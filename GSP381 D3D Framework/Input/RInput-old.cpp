#include "RInput-old.h"
#include "../Graphics/D3DApp.h"


RInput::RInput(void)
{
	m_fDCounter = 99999999.0f;
}


RInput::~RInput(void)
{
	
}

RInput* RInput::Instance()
{
	static RInput instance;

	return &instance;
}

void RInput::InitRawInput()
{
	//rawinput init
	RAWINPUTDEVICE Rid[2];

	// Keyboard
	Rid[0].usUsagePage = 1;
	Rid[0].usUsage = 6;
	Rid[0].dwFlags = 0;
	Rid[0].hwndTarget=NULL;

	// Mouse
	Rid[1].usUsagePage = 1;
	Rid[1].usUsage = 2;
	Rid[1].dwFlags = 0;
	Rid[1].hwndTarget=NULL;

	if (RegisterRawInputDevices(Rid,2,sizeof(RAWINPUTDEVICE))==FALSE)
        return ;
}

bool RInput::KeyPressed(USHORT key)
{
	if (key == m_keyCode)
	{
		//has counter gone past time hold?
		if (m_fDCounter > KEYPRESSDELAY)
		{
			m_fDCounter = 0;
			return true;

		}
		else
			return false;
	}
	return false;
}

void RInput::Update(float dt)
{
	if (m_fDCounter < KEYPRESSDELAY)
 		m_fDCounter += dt;
	
	GetCursorPos(&m_pMousePos); //screen coords
	ScreenToClient(D3DAPPI->GetHWND(),&m_pMousePos);
}

POINT RInput::GetMousePos() 
{

	return m_pMousePos;
}

void RInput::SetLeftMouseDown(bool mouseDown)
{
	m_leftMouseDown = mouseDown;
}
void RInput::SetLeftMouseUp(bool mouseUp)
{
	m_leftMouseUp = mouseUp;
}
void RInput::SetRightMouseDown(bool mouseDown) 
{
	m_rightMouseDown = mouseDown;
}	
void RInput::SetRightMouseUp(bool mouseUp) 
{
	m_rightMouseUp = mouseUp;
}
