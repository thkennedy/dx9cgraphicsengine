#include "Plane.h"


Plane::Plane(int id, Vector3 A, Vector3 B, Vector3 C)
{
	m_vNormal = (B-A)%(C-A);
	m_fOffset = m_vNormal* A;
	
}

Plane::Plane(int id, Vector3 vNormal, float fOffset)
	:m_vNormal(vNormal), m_fOffset(fOffset)
{
	m_iBoundingVolumeID = id;
}


Plane::~Plane(void)
{
}
