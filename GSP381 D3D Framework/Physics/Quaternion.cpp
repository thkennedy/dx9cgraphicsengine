#include "Quaternion.h"

Quaternion::Quaternion(float iw,float ix,float iy,float iz)
	:D3DXQUATERNION(ix,iy,iz,iw)
{
}

Quaternion::Quaternion(Vector3 axis,float theta)
{
	//just in case, normalizing the axis

	axis.Normalize();

	w= cos(theta/2);
	x= axis.x*sin(theta/2);
	y= axis.y*sin(theta/2);
	z= axis.z*sin(theta/2);


}

Quaternion::Quaternion(float iw,Vector3 qVec)
{

	if (iw + qVec.x + qVec.y + qVec.z <= 0)
		iw = 1.00;

	w=iw;
	x=qVec.x;
	y=qVec.y;
	z=qVec.z;


}

Quaternion::Quaternion(const D3DXQUATERNION &quat)
	:D3DXQUATERNION(quat)
{

}

Quaternion::Quaternion(const Quaternion &quat)
	:D3DXQUATERNION(quat.x,quat.y,quat.z,quat.w)
{

}


void Quaternion::Normalize()
{
	D3DXQuaternionNormalize(this,this);

}
Quaternion Quaternion::operator +(Quaternion multiplier)
{
	return (D3DXQUATERNION) *this + multiplier;
}
void Quaternion::operator+=(Quaternion q){

	*this = (D3DXQUATERNION) *this + q;
}



Quaternion Quaternion::operator *(Quaternion &m)
{
	return (D3DXQUATERNION) *this * m;
}


void Quaternion::operator *= (Quaternion q)
{
	
	*this = *this * q;

}


void Quaternion::rotateByVector(const Vector3& vector) {

	Quaternion q(0.0,vector.x,vector.y,vector.z);
	(*this) *=q;
		
}

void Quaternion::AddScaledVector(const Vector3& vector,float scale)
{

	Quaternion q(0.0,vector.x*scale,vector.y*scale,vector.z*scale);
	q *= *this;


	w+= (q.w * .5);
	x+= (q.x * .5);
	y+= (q.y * .5);
	z+= (q.z * .5);


}

Quaternion Quaternion::operator *(float duration)
{
	
	
	return (D3DXQUATERNION) *this * duration;
}


//void Quaternion::CreateBasis(Vector3* pA, Vector3* pB, Vector3* pC)
//{
//	//create rotate matrix
//	Matrix3 Rot = Rotate(*this);
//
//	*pA = Rot * Vector3(1, 0, 0);
//	*pB = Rot * Vector3(0, 1, 0);
//	*pC = Rot * Vector3(0, 0, 1);
//}

//Matrix3 Quaternion::Rotate(Quaternion pQuat)
//{
//	Matrix3 temp;
//
//	 temp.m_data[0][0] = 1-(2*(pQuat.y*pQuat.y)+2*(pQuat.z*pQuat.z));
//	 temp.m_data[0][1] = 2*pQuat.x*pQuat.y+2*pQuat.z*pQuat.w;
//	 temp.m_data[0][2] = 2*pQuat.x*pQuat.z-2*pQuat.y*pQuat.w;
//	 temp.m_data[1][0] = 2*pQuat.x*pQuat.y-2*pQuat.z*pQuat.w;
//	 temp.m_data[1][1] = 1-(2*(pQuat.x*pQuat.x)+2*(pQuat.z*pQuat.z));
//	 temp.m_data[1][2] = 2*pQuat.y*pQuat.z+2*pQuat.x*pQuat.w;
//	 temp.m_data[2][0] = 2*pQuat.x*pQuat.z+2*pQuat.y*pQuat.w;
//	 temp.m_data[2][1] = 2*pQuat.y*pQuat.z-2*pQuat.x*pQuat.w;
//	 temp.m_data[2][2] = 1-(2*(pQuat.x*pQuat.x)+2*(pQuat.x*pQuat.y));
//	 
//	 
//	 return temp;
//}
//
//Matrix3 Quaternion::GetRotationMatrix()
//{
//	Matrix3 temp;
//
//	 temp.m_data[0][0] = 1-(2*(this->y*this->y)+2*(this->z*this->z));
//	 temp.m_data[0][1] = 2*this->x*this->y+2*this->z*this->w;
//	 temp.m_data[0][2] = 2*this->x*this->z-2*this->y*this->w;
//	 temp.m_data[1][0] = 2*this->x*this->y-2*this->z*this->w;
//	 temp.m_data[1][1] = 1-(2*(this->x*this->x)+2*(this->z*this->z));
//	 temp.m_data[1][2] = 2*this->y*this->z+2*this->x*this->w;
//	 temp.m_data[2][0] = 2*this->x*this->z+2*this->y*this->w;
//	 temp.m_data[2][1] = 2*this->y*this->z-2*this->x*this->w;
//	 temp.m_data[2][2] = 1-(2*(this->x*this->x)+2*(this->x*this->y));
//	 
//	 
//	 return temp;
//}

Quaternion Quaternion::GetConjugate()
{
	Quaternion q;

	q.x = this->x * -1;
	q.y = this->y * -1;
	q.z = this->z * -1;
	return (q);
}