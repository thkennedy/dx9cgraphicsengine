#pragma once
#include "boundingvolume.h"


class Plane :
	public BoundingVolume
{
private:
	
    // plane normal
	Vector3 m_vNormal;

    // ofset from the origin
    float m_fOffset;

public:
	Plane(int id, Vector3 vNormal, float fOffset);
	Plane(int id, Vector3 A, Vector3 B, Vector3 C);
	~Plane(void);

	/////////////////
	// UTILITIES
	////////////////

	////////////////
	// ACCESSORS
	////////////////
	Vector3 GetNormal()const {return m_vNormal;}
	float GetOffset() const  {return m_fOffset;}

	////////////////
	// MUTATORS
	////////////////
	void SetNormal(Vector3 vNormal) {m_vNormal = vNormal;}
	void SetOffset(float fOffset) {m_fOffset = fOffset;}


};

