#pragma once
#include "RigidBody.h"



/*

	This is my ball class. It is a rigid body, and has a radius. Best choice BV
	is a circle

*/

//forward declarations


class Ball :
	public RigidBody
{
protected:

	//ball's radius
	float m_fRadius;
	


public:
	Ball();
	Ball(int id, Vector3 vPosition, float fRadius, int iBVid = -1);
	~Ball();

	virtual void VUpdate(float dt);
};

