#pragma once
#include "../Objects/BaseGameEntity.h"
#include "Matrix3.h"
#include <d3dx9.h>
#include "Quaternion.h"
#include "Matrix4.h"


#define SLEEPEPSILON 0.5f
// for bias, book recommends between .5 and .8. Value must be between 0 and 1
#define BASEBIAS	0.2f
#define MOVEMENTEPSILON 0.01f

class RigidBody :
	public BaseGameEntity
{
protected:

	float m_fInverseMass;				// object's inverse mass (1/mass) - 0 means object immovable
	
	Matrix3 m_mInverseInertiaTensor;	// inertia tensor is body space
	Matrix3	m_mInverseInertiaTensorW;	// same as above, but in world space

	float m_fLinearDamping;				// damping to simulate loss of energy
	float m_fAngularDamping;			//	angular slowing - loss of energy

	Quaternion	m_qOrientation;			//orientation is quaternions
	Vector3		m_vOrientation;			// orientation in Vector3

	Vector3		m_vVelocity;			// linear velocity
	Vector3		m_vRotation;			// angular velocity

	Vector3		m_vTorque;				// angular acceleration
	Vector3		m_vAcceleration;		// linear acceleration

	Vector3		m_vLastFrameAccel;		// last frame's acceleration	

	Matrix4		m_mTransformMatrix;		//converts body to/from world space

	Vector3		m_vForceAccum;			// force accumulator - applied then cleared each cycle
	Vector3		m_vTorqueAccum;			// torque accumulator - applied then cleared each cycle

	float		m_fRestitution;			// elasticity of body (how squishy is it)

	// Rigid Body's ID (assigned by Movement Manager)
	int			m_iBodyID;	
	//graphics ID object
	int			m_iGraphicsID;
	//bounding volume ID
	int			m_iBoundingVolumeID;

	//////////////
	// Sleep
	/////////////
	bool m_bIsAwake;			// is entity sleeping - default is false	

	bool m_bCanSleep;			// can I put the entity to sleep

	float m_fMotion;			// recently weighted average motion

	float m_fCurrentMotion;		// velocity and rotation dotted with themselves and added together

	/////////////////
	// REST/STACKING
	/////////////////
	// stores body's last position and rotation - used to see if it should be moved.
	Vector3 m_vLastPosition;
	Quaternion m_qLastOrientation;

	
public:
	RigidBody();
	RigidBody(int iBodyID, Vector3 vPosition, int iBoundID = -1, Vector3 vVelocity = Vector3(0.0f,0.0f,0.0f));
	~RigidBody();


	////////////////
	// UTILITIES
	////////////////

	// all physics updates go here - virtual function
	virtual void VUpdate(float dt);
	// checks to see if body has a bounding volume. if it does, 
	//	it updates its position, orientation, etc
	void UpdateBV();
	// Calculates internal data from state data.
	inline void CalculateDerivedData();						
	// Converts a given point from world space to the body space.
	Vector3 GetPointInLocalSpace(const Vector3 &point); 
	// Converts the given point from object's body space to world space
	Vector3 GetPointInWorldSpace(const Vector3 &point);	
	// Calculates the transform matrix for the object
	void CalculateTransformMatrix(Matrix4 &transformMatrix, Vector3 &position,
		Quaternion &orientation);
	// Clears force and torque accumulators
	void ClearAccumulators();
	
	

	////////////////
	// ACCESSORS
	////////////////
	int GetBoundingVolumeID() {return m_iBoundingVolumeID;}
	// Get Graphics object
	int GetGraphicsID() {return m_iGraphicsID;}
	// Get Body's ID
	int GetID() const {return m_iBodyID;}
	// Gets object's mass
	float GetMass() const;	
	// Returns object's inverse mass (if 0, means object is immobile)
	float GetInverseMass() const;
	// Returns a bool telling if an object can move or not
	bool HasFiniteMass() const;
	// Gets an object's inertial tensor (in body space)via the passed in variable
	void GetInertiaTensor(Matrix3 &inertiaTensor) const;
	// Returns the value of the object's inertial tensor in body space
	Matrix3 GetInertiaTensor() const;
	// Gets an object's inertial tensor in world space via the passed in variable
	void GetInertiaTensorWorld(Matrix3 &inertiaTensor) const;
	// Returns the value of the object's inertial tensor in world space 
    Matrix3 GetInertiaTensorWorld() const;
	// Gets object's inverse inertial tensor in body space
	void GetInverseInertiaTensor(Matrix3 &inverseInertiaTensor) const;
	// Returns value of object's inverse inertial tensor in body space
	Matrix3 GetInverseInertiaTensor() const;
	//Passes value of inverse inertial tensor back via the passed var
	void GetInverseInertiaTensorWorld(Matrix3 &inverseInertiaTensor) const;
	// Returns value of object's inverse inertial tensor
	Matrix3 GetInverseInertiaTensorWorld() const;
	// Returns Linear Damping Value
	float GetLinearDamping() const;
	// REturns Angular Damping Value
	float GetAngularDamping() const;
	// Sets passed var with the value of the object's orientation
	void GetOrientation(Quaternion &orientation) const;
	// Returns the values of the object's orientation
    Quaternion GetOrientation() const;
	// Sets the matrix 3 with the rotation matrix
    void GetOrientation(Matrix3 &matrix) const;
	//returns the rotation aspect of the transform matrix
    void GetOrientation(float matrix[9]) const;
	// Sets passed variable with the value of the object's transform matrix
	void GetTransform(Matrix4 &transform) const;
	// Returns transform matrix into a 16-value array
    void GetGLTransform(float matrix[16]) const;
	// Returns value of transform matrix
    Matrix4 GetTransform() const;
	// returns a vector of the object's orientation in local space
    Vector3 GetDirectionInLocalSpace(const Vector3 &direction) const;
	// transforms the object's orientation into world space and returns it
    Vector3 GetDirectionInWorldSpace(const Vector3 &direction) const;
	// Sets passed variable to the object's velocity
	void GetVelocity(Vector3 &velocity) const;
    // Returns value of object's velocity
	Vector3 GetVelocity() const;
	// sets passed var with object's rotation value
	void GetRotation(Vector3 &rotation) const;
	// returns rotation value item
    Vector3 GetRotation() const;
	// sets passed var with last frame acceleration's value
	void GetLastFrameAcceleration(Vector3 &linearAcceleration) const;
	// returns last fram accel
    Vector3 GetLastFrameAcceleration() const;
	// sets passed var with acceleration value
	void GetAcceleration(Vector3 &acceleration) const;
	// returns acceleration value
    Vector3 GetAcceleration() const;
	// gets awake state
	bool GetAwake() {return m_bIsAwake;}
	// gets cansleep state
	bool GetCanSleep() {return m_bCanSleep;}

	////////////////
	// MUTATORS
	////////////////
	// set object's id
	void SetID(const int id){m_iBodyID = id;}
	// Sets object's mass
	void SetMass(const float mass);	
	// Sets an object' inverse mass 
	void SetInverseMass(const float fInverseMass);
	// Sets an object's Torque (rotational acceleration)
	void SetTorque(Vector3);	   
    // Sets the object's inertial tensor
	void SetInertiaTensor(const Matrix3 &inertiaTensor);
	// Sets the object's Inverse Inertial tensor
	void SetInverseInertiaTensor(const Matrix3 &inverseInertiaTensor);
	// transforms Inertia Tensor from body to world space
	void TransformInertiaTensor(Matrix3 &iitWorld,
                                        const Quaternion &q,
                                        const Matrix3 &iitBody,
                                        const Matrix4 &rotmat);
	//sets both linear and angular damping
	void SetDamping(const float linearDamping, const float angularDamping);
    //sets linear damping
	void SetLinearDamping(const float linearDamping);
	//sets angular damping    	
    void SetAngularDamping(const float angularDamping);
    // sets orientation    
	void SetOrientation(const Quaternion &orientation);
    // sets orientation by passing individual values
	void SetOrientation(const float w, const float x,const float y, const float z);
    // sets velocity 
	void SetVelocity(const Vector3 &velocity);
    // sets velocity by passing individual values
	void SetVelocity(const float x, const float y, const float z);
    // affects a change in velocity using the current velocity in the calculation
    void AddVelocity(const Vector3 &deltaVelocity);
    // sets the rotation value   
	void SetRotation(const Vector3 &rotation);
    // sets rotation values individually
	void SetRotation(const float x, const float y, const float z);
	// adds force to center of mass
    void AddForce(const Vector3 &force);
	// add force to point on object in world space
    void AddForceAtPoint(const Vector3 &force, const Vector3 &point);
	// add force to point on object (in body space)
    void AddForceAtBodyPoint(const Vector3 &force, const Vector3 &point);
	// adds torque (angular accel) to object
    void AddTorque(const Vector3 &torque);  
	// adds rotation (angular vel) to object
	void AddRotation(const Vector3 &deltaRotation);
	// sets acceleration for object
    void SetAcceleration(const Vector3 &acceleration);
    // sets acceleration by individual axis
	void SetAcceleration(const float x, const float y, const float z);
    // manually rotates body by given quaternion
	void RotateBody(Quaternion &qRotation);
	// changes the object's awake state
	void SetAwake(bool bAwake = true);
	// sets whether or not an entity can sleep
	void SetCanSleep(const bool bCanSleep);
	



};

