#include "Vector3.h"


Vector3::Vector3()// default constructor
	:D3DXVECTOR3()
{
	
}
Vector3::Vector3(float xval, float yval, float zval )  //para const.
	:D3DXVECTOR3(xval,yval,zval)
{

	
}

Vector3::Vector3( const Vector3& other)		// copy const
	:D3DXVECTOR3(other.x,other.y,other.z)
{

}

Vector3::Vector3( const D3DXVECTOR3& other) // copy const
	:D3DXVECTOR3(other)
{

}

Vector3::~Vector3 (){}// destructor

void Vector3::Scale(float c) // scales the vector by a number - modifies the vector
{
	D3DXVec3Scale(this, this, c);
}

Vector3 Vector3::ScaleReturn(float c) // scales the vector and returns the result - does not modify vector
{
	return (D3DXVECTOR3) *this * c;
	
}

float Vector3::Magnitude()	// returns Magnitude of the vector
{
	
	float mx = x*x;
	float my = y*y;
	float mz = z*z;

	float sub = mx+my+mz;

	return sqrt(sub);
}

float Vector3::MagnitudeSquared() const
{
	
	float mx = x*x;
	float my = y*y;
	float mz = z*z;

	float sub = mx+my+mz;

	return (sub);
}

void Vector3::Normalize() // normalizes the vector
{
	D3DXVec3Normalize(this,this);
}

Vector3 Vector3::NormalizeRet() // returns the value of the normalized vector - does not modify the vector
{

	Vector3 temp = *this;

	temp.Normalize();

	return temp;
}



Vector3 Vector3::CrossProductReturn (const Vector3 &rhs) // returns the cross product of two 3d vectors, but does not modify the vector
{

	Vector3 temp;

	D3DXVec3Cross(&temp,this,&rhs);	

	return temp;
}

Vector3 Vector3::CrossProduct (Vector3 vectA, Vector3 vectB) // takes two vectors and returns the cross product of them - does not modify either vector
{
	Vector3 temp;

	D3DXVec3Cross(&temp,&vectA,&vectB);	
	
	return temp;
}

Vector3 Vector3::ComponentProduct(const Vector3 &vector) const
{
	return Vector3(x * vector.x, y * vector.y, z * vector.z);
}

/**
    * Performs a component-wise product with the given vector and
    * sets this vector to its result.
    */
void Vector3::ComponentProductUpdate(const Vector3 &vector)
{
    x *= vector.x;
    y *= vector.y;
    z *= vector.z;
}

void Vector3::Clear()
{
	x = y = z = 0.0f;
}
void Vector3::Invert()
{
	 x = -x;
     y = -y;
     z = -z;
}
Vector3 Vector3::InvertReturn()
{
	Vector3 temp;

	temp = *this;

	temp.Scale(-1);

	return temp;

}

/**
* Adds the given vector to this, scaled by the given amount.
 */
void Vector3::AddScaledVector(const Vector3& vector, float Scale)
{
    x += vector.x * Scale;
    y += vector.y * Scale;
    z += vector.z * Scale;
}

void Vector3::Average(int divisor)
{
	x = x/divisor;
	y = y/divisor;
	z = z/divisor;
}


/**********************/
/*OVERLOADED OPERATORS*/
/**********************/

//cross product
Vector3 Vector3::operator %(const Vector3 &rhs)
{
	Vector3 temp;

	D3DXVec3Cross(&temp,this,&rhs);	

	return temp;
}

Vector3 Vector3::operator %=(const Vector3 &rhs)
{
	D3DXVec3Cross(this,this,&rhs);	

	return *this;
}




//Vector3 Vector3::operator -= (const Vector3 &rhs) //overloaded operators
//{
//	return *this -= rhs;
//}
//Vector3 Vector3::operator += (const Vector3 &rhs)
//{
//	x += rhs.x;
//	y += rhs.y;
//	z += rhs.z;
//	return *this;
//}
Vector3 Vector3::operator - (const Vector3 &rhs) const
{
	return Vector3(	this->x - rhs.x,
					this->y - rhs.y,
					this->z - rhs.z);
}

 /** Subtracts the given vector from this. */
void Vector3::operator-=(const Vector3& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
}

/** Adds the given vector to this. */
void Vector3::operator+=(const Vector3& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
}

/**
    * Returns the value of the given vector added to this.
    */
Vector3 Vector3::operator + (const Vector3& v) const
{
    return Vector3(x+v.x, y+v.y, z+v.z);
}
/** Multiplies this vector by the given scalar. */
void Vector3::operator *=(const float value)
{
    x *= value;
    y *= value;
    z *= value;
}
float Vector3::operator * (const Vector3 &rhs) const // dot product
{
	return D3DXVec3Dot(this, &rhs);
	
}

Vector3 Vector3::operator * (const float &rhs) const // scales the vector
{
	return (D3DXVECTOR3)*this * rhs;	

	/*Vector3 temp = *this;
	D3DXVec3Scale(&temp, &temp, rhs);
	return temp;*/
}

//void Vector3::operator *= (const float &rhs)
//{
//	*this *= rhs;
//}

Vector3 Vector3::operator = (const Vector3 &rhs) // assignment operator
{
	// Only do assignment if RHS is a different object from this.
    if (this != &rhs) 
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
	}

	return *this;
}

Vector3 Vector3::operator = (const D3DXVECTOR3 &rhs)
{
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;

	return *this;
}

float& Vector3::operator[](unsigned int i)
{
	if (i == 0) return x;
	if (i == 1) return y;
	return z;
}

//bools
bool Vector3::operator ==	(const Vector3 &rhs)
{
	return (this->x == rhs.x && this->y == rhs.y && this->z == rhs.z);
}
bool Vector3::operator !=	(const Vector3 &rhs)
{
	return !(*this == rhs);
}





/**********************/
/*******END************/
/*OVERLOADED OPERATORS*/
/**********************/