#include "Ball.h"
#include "../Graphics/ShapeManager.h"
#include "../Graphics/D3DSphere.h"
#include "CollisionDetector.h"
#include "Circle.h"
#include "AdvCollisions.h"


Ball::Ball()
	:RigidBody(-1, Vector3(0.0f,0.0f,0.0f)),m_fRadius(1.0f)
{
	//give it a graphical object
	m_iGraphicsID = SMI->CreateSphere(m_fRadius);

	m_qOrientation = Quaternion();

}

Ball::Ball(int iBodyID, Vector3 vPosition, float fRadius, int iBVid)
	:RigidBody(iBodyID, vPosition, iBVid), m_fRadius(fRadius)
{
	m_iGraphicsID = SMI->CreateSphere(m_fRadius);
	
	m_qOrientation = Quaternion();

	// inertial tensor for solid cube
	
	Matrix3 InertiaTensor;

	m_fInverseMass = 1.0f / 20.0f;

	// I = 2/5 MR^2
	float x = 0.4f * (1.0f/m_fInverseMass) * (fRadius * fRadius);
	float y = 0.4f * (1.0f/m_fInverseMass) * (fRadius * fRadius);
	float z = 0.4f * (1.0f/m_fInverseMass) * (fRadius * fRadius);

	InertiaTensor.data[0] = x;
	InertiaTensor.data[4] = y;
	InertiaTensor.data[8] = z;

	SetInertiaTensor(InertiaTensor);

	


	
}

Ball::~Ball()
{
	

}

void Ball::VUpdate(float dt)
{
	//update physics
	RigidBody::VUpdate(dt);

	// update bv
	//CMI->GetBV(m_iBoundingVolumeID)->VUpdate(dt);
	//ACMI->GetBV(m_iBoundingVolumeID)->VUpdate(dt);
		
	//update graphics
	SMI->GetSphere(m_iGraphicsID)->SetPosition(m_vPosition);
	// update graphics orientation
	SMI->GetSphere(m_iGraphicsID)->SetHeading(m_qOrientation);
}