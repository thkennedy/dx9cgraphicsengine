#pragma once


#include "functions.h"

class Vector3;


using namespace std;

class Quaternion;

class Matrix3
{

public:
	//float m_data[3][3];
	float data[9];
	
	Matrix3();
	//Matrix3(float data[3][3]);
	Matrix3(const Vector3 &compOne, const Vector3 &compTwo,
            const Vector3 &compThree);
	Matrix3(float c0, float c1, float c2, float c3, float c4, float c5,
            float c6, float c7, float c8);
	
	
	
	/*void scalar(float scale);*/
	void print();

	void SetTranspose(Matrix3 &m);  // returns new matrix containing transpose of this matrix
	Matrix3 Transpose();	
	/*Matrix3 translateMatrix (Vector3 deltaXY);
	Matrix3 scaleMatrix(Vector3 scaleXY);
	Matrix3 rotationMatrix(float theta);
	Matrix3 comboMatrix(float translateX, float translateY, float translateZ, float rotationVal, float scaleX, 
	float scaleY, float scaleZ);
	Matrix3 worldToLocal(Vector3 point);
	Matrix3 worldToLocal(Vector3 point, Vector3 origin);
	Matrix3 localToWorld(Vector3 point);
	Matrix3 localToWorld(Vector3 point, Vector3 origin);*/
	//void initializeIdentMatrix();

	void SetInverse(const Matrix3 &m);
	void SetComponents(const Vector3 &compOne, const Vector3 &compTwo,
            const Vector3 &compThree);
	
	Vector3 Transform(const Vector3 &vector);
	Vector3 transformTranspose(const Vector3 &vector);
	void SetBlockInertiaTensor(const Vector3 &halfsizes, float mass);
	void SetSkewSymmetric(const Vector3 vector);
	void SetDiagonal(float a, float b, float c);
	void SetInertiaTensorCoeffs(float ix, float iy, float iz,
    float ixy=0, float ixz=0, float iyz=0);
	Vector3 GetRowVector(int i) const;
	Vector3 GetAxisVector(int i) const;
	void SetOrientation(const Quaternion &q);
	Matrix3 Inverse() const;
	void Invert();
	/**
         * Interpolates a couple of matrices.
         */
        static Matrix3 linearInterpolate(const Matrix3& a, const Matrix3& b, float prop);

	
	
	
	void operator += (const Matrix3 &rhs); //overloaded operators
	//Matrix3 operator + (const Matrix3 &rhs);
	Matrix3 operator * (const Matrix3 &rhs);
	void operator *= (const Matrix3 &rhs);
	void operator *= (const float &rhs);

	//Matrix3 operator = (const Matrix3 &rhs);
	Vector3 operator * (const Vector3 &rhs);
	
	
	
};

