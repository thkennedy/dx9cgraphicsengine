#include "RigidBody.h"
#include "../Graphics/ShapeManager.h"
#include "AdvCollisions.h"
#include "OBB.h"
#include "Circle.h"


RigidBody::RigidBody()
{

	m_vPosition.Clear();
	m_vVelocity.Clear();
	m_vAcceleration.Clear();
	m_vForceAccum.Clear();
	m_vTorqueAccum.Clear();
	m_fInverseMass=0.0f;
	m_fLinearDamping=.89f;
	m_fAngularDamping=.60f;
	m_vRotation.Clear();
	m_fRestitution = ELASTICITY;

	m_bIsAwake = true; // all items start off as not asleep

	m_bCanSleep = true;	// all items can be set to sleep unless otherwise stated

	// add some m_fMotion to avoid it falling back asleep immediately
		m_fMotion = SLEEPEPSILON * 2.0F;


	
	

}

RigidBody::RigidBody(int id, Vector3 vPosition, int iBoundID,  Vector3 vVelocity)
	:BaseGameEntity(vPosition), m_iBodyID(id), m_iBoundingVolumeID(iBoundID), m_bIsAwake(true), m_bCanSleep (true)
{
	
	m_vVelocity.Clear();
	m_vAcceleration.Clear();
	m_vForceAccum.Clear();
	m_vTorqueAccum.Clear();
	m_fInverseMass=1.0f;
	m_fLinearDamping=.890f;
	m_fAngularDamping=.890f;
	m_vRotation.Clear();
	m_fRestitution = ELASTICITY;

	m_qOrientation = Quaternion(1.0f, 0.0f, 0.0f, 0.0f);

	// add some m_fMotion to avoid it falling back asleep immediately
		m_fMotion = SLEEPEPSILON * 2.0F;

	m_fInverseMass = 1.0f / 10.0f;

	m_vLastPosition = Vector3(10000,10000,10000);

	CalculateDerivedData();

}
RigidBody::~RigidBody()
{
	//need to delete graphics object and BV
	//graphics
	SMI->DeleteShape(m_iGraphicsID);
	//bv
	ACMI->DeleteBV(m_iBoundingVolumeID);
}


////////////////
// UTILITIES
////////////////

// all physics updates go here - virtual function
void RigidBody::VUpdate(float dt)
{
	// sleep logic
	if (!m_bIsAwake) return;

	// Calculate linear acceleration from force inputs.
    m_vLastFrameAccel = m_vAcceleration;
    m_vLastFrameAccel.AddScaledVector(m_vForceAccum, m_fInverseMass);

    // Calculate angular acceleration from torque inputs.
    Vector3 angularAcceleration = m_mInverseInertiaTensorW.Transform(m_vTorqueAccum);

    // Adjust velocities
    // Update linear velocity from both acceleration and impulse.
    m_vVelocity.AddScaledVector(m_vLastFrameAccel, dt);

    // Update angular velocity from both acceleration and impulse.
    m_vRotation.AddScaledVector(angularAcceleration, dt);

    // Impose drag.
    m_vVelocity *= powf(m_fLinearDamping, dt);
    m_vRotation *= powf(m_fAngularDamping, dt);

    // Adjust positions
    // Update linear position.
    m_vPosition.AddScaledVector(m_vVelocity, dt);

    // Update angular position.
    m_qOrientation.AddScaledVector(m_vRotation, dt);

    // Normalise the orientation, and update the matrices with the new
    // position and orientation
    CalculateDerivedData();

    // Clear accumulators.
    ClearAccumulators();

    // Update the kinetic energy store, and possibly put the body to
    // sleep.
    if (m_bCanSleep) 
	{
        float currentMotion = m_vVelocity * m_vVelocity + m_vRotation * m_vRotation;

        float bias = powf(0.2, dt);
        m_fMotion = bias * m_fMotion + (1-bias)*currentMotion;

        if (m_fMotion < SLEEPEPSILON) 
			SetAwake(false);
        else if (m_fMotion > 10 * SLEEPEPSILON) 
			m_fMotion = 10 * SLEEPEPSILON;
    }

	//store position and rotation for compare next time
	m_vLastPosition = m_vPosition;
	m_qLastOrientation = m_qOrientation;

	UpdateBV();
}	

// checks to see if body has a bounding volume. if it does, 
//	it updates its position, orientation, etc
void RigidBody::UpdateBV()
{
	//do I have a bv?
	if (m_iBoundingVolumeID == -1) return;
	
	//update position
	ACMI->GetBV(m_iBoundingVolumeID)->SetPosition(m_vPosition);

	//update orientation
	ACMI->GetBV(m_iBoundingVolumeID)->SetOrientationQ(m_qOrientation);

	//update transform
	ACMI->GetBV(m_iBoundingVolumeID)->SetTransform(m_mTransformMatrix);

}
// Calculates internal data from state data.
void RigidBody::CalculateDerivedData()
{
	
	//normalize the orientation
	m_qOrientation.Normalize();

    // Calculate the transform matrix for the body.
    CalculateTransformMatrix(m_mTransformMatrix, m_vPosition, m_qOrientation);

    // Calculate the inertiaTensor in world space.
    TransformInertiaTensor(m_mInverseInertiaTensorW,
        m_qOrientation,
        m_mInverseInertiaTensor,
        m_mTransformMatrix);

}					
// Converts a given point from world space to the body space.
Vector3 RigidBody::GetPointInLocalSpace(const Vector3 &point) 
{
	return m_mTransformMatrix.TransformInverse(point);
}
// Converts the given point from object's body space to world space
Vector3 RigidBody::GetPointInWorldSpace(const Vector3 &point) 
{
    return m_mTransformMatrix.Transform(point);
}
// Calculates the transform matrix for the object
void RigidBody::CalculateTransformMatrix(Matrix4 &transformMatrix, Vector3 &position,
			Quaternion &orientation)
{
    transformMatrix._11 = 1-2*m_qOrientation.y*m_qOrientation.y-
        2*m_qOrientation.z*m_qOrientation.z;
    transformMatrix._12 = 2*m_qOrientation.x*m_qOrientation.y -
        2*m_qOrientation.w*m_qOrientation.z;
    transformMatrix._13 = 2*m_qOrientation.x*m_qOrientation.z +
        2*m_qOrientation.w*m_qOrientation.y;
    transformMatrix._14 = position.x;

    transformMatrix._21 = 2*m_qOrientation.x*m_qOrientation.y +
        2*m_qOrientation.w*m_qOrientation.z;
    transformMatrix._22 = 1-2*m_qOrientation.x*m_qOrientation.x-
        2*m_qOrientation.z*m_qOrientation.z;
    transformMatrix._23 = 2*m_qOrientation.y*m_qOrientation.z -
        2*m_qOrientation.w*m_qOrientation.x;
    transformMatrix._24 = position.y;

    transformMatrix._31 = 2*m_qOrientation.x*m_qOrientation.z -
        2*m_qOrientation.w*m_qOrientation.y;
    transformMatrix._32 = 2*m_qOrientation.y*m_qOrientation.z +
        2*m_qOrientation.w*m_qOrientation.x;
    transformMatrix._33 = 1-2*m_qOrientation.x*m_qOrientation.x-
        2*m_qOrientation.y*m_qOrientation.y;
    transformMatrix._34 = position.z;
}
// Clears force and torque accumulators
void RigidBody::ClearAccumulators()
{
    m_vForceAccum.Clear();
    m_vTorqueAccum.Clear();
}

// sets whether or not an entity can sleep
void RigidBody::SetCanSleep(const bool bCanSleep)
{
    RigidBody::m_bCanSleep = bCanSleep;

    if (!m_bCanSleep && !m_bIsAwake) SetAwake(true);
}

////////////////
// ACCESSORS
////////////////
// Gets object's mass
float RigidBody::GetMass() const
{
    if (m_fInverseMass == 0) {
        return FLT_MAX;
    } else {
        return ((float)1.0)/m_fInverseMass;
    }
}
// Returns object's inverse mass (if 0, means object is immobile)
float RigidBody::GetInverseMass() const
{
    return m_fInverseMass;
}
// Returns a bool telling if an object can move or not
bool RigidBody::HasFiniteMass() const
{
    return m_fInverseMass > 0.0f;
}
// Gets an object's inertial tensor (in body space)via the passed in variable
void RigidBody::GetInertiaTensor(Matrix3 &inertiaTensor) const
{
    inertiaTensor.SetInverse(m_mInverseInertiaTensor);
}
// Returns the value of the object's inertial tensor in body space
Matrix3 RigidBody::GetInertiaTensor() const
{
    Matrix3 it;
    GetInertiaTensor(it);
    return it;
}
// Gets an object's inertial tensor in world space via the passed in variable
void RigidBody::GetInertiaTensorWorld(Matrix3 &inertiaTensor) const
{
    inertiaTensor.SetInverse(m_mInverseInertiaTensorW);
}
// Returns the value of the object's inertial tensor in world space 
Matrix3 RigidBody::GetInertiaTensorWorld() const
{
    Matrix3 it;
    GetInertiaTensorWorld(it);
    return it;
}
// Gets object's inverse inertial tensor in body space
void RigidBody::GetInverseInertiaTensor(Matrix3 &mInverseInertiaTensor) const
{
    mInverseInertiaTensor = m_mInverseInertiaTensor;
}
// Returns value of object's inverse inertial tensor in body space
Matrix3 RigidBody::GetInverseInertiaTensor() const
{
    return m_mInverseInertiaTensor;
}
//Passes value of inverse inertial tensor back via the passed var
void RigidBody::GetInverseInertiaTensorWorld(Matrix3 &mInverseInertiaTensor) const
{
    mInverseInertiaTensor = m_mInverseInertiaTensorW;
}
// Returns value of object's inverse inertial tensor
Matrix3 RigidBody::GetInverseInertiaTensorWorld() const
{
    return m_mInverseInertiaTensorW;
}
// Returns Linear Damping Value
float RigidBody::GetLinearDamping() const
{
    return m_fLinearDamping;
}
// REturns Angular Damping Value
float RigidBody::GetAngularDamping() const
{
    return m_fAngularDamping;
}
// Sets passed var with the value of the object's orientation
void RigidBody::GetOrientation(Quaternion &qOrientation) const
{
    qOrientation = m_qOrientation;
}
// Returns the values of the object's orientation
Quaternion RigidBody::GetOrientation() const
{
    return m_qOrientation;
}
// Sets the matrix 3 with the rotation matrix
void RigidBody::GetOrientation(Matrix3 &matrix) const
{
	GetOrientation(matrix.data);
}
//returns the rotation aspect of the transform matrix
void RigidBody::GetOrientation(float matrix[9]) const
{
    matrix[0] = m_mTransformMatrix._11;
    matrix[1] = m_mTransformMatrix._12;
    matrix[2] = m_mTransformMatrix._13;

    matrix[3] = m_mTransformMatrix._21;
    matrix[4] = m_mTransformMatrix._22;
    matrix[5] = m_mTransformMatrix._23;

    matrix[6] = m_mTransformMatrix._31;
    matrix[7] = m_mTransformMatrix._32;
    matrix[8] = m_mTransformMatrix._33;
}
// Sets passed variable with the value of the object's transform matrix
void RigidBody::GetTransform(Matrix4 &transform) const
{
    transform = m_mTransformMatrix;
}

void RigidBody::GetGLTransform(float matrix[16]) const
{
    matrix[0] = (float)m_mTransformMatrix._11;
    matrix[1] = (float)m_mTransformMatrix._21;
    matrix[2] = (float)m_mTransformMatrix._31;
    matrix[3] = 0;

    matrix[4] = (float)m_mTransformMatrix._12;
    matrix[5] = (float)m_mTransformMatrix._22;
    matrix[6] = (float)m_mTransformMatrix._32;
    matrix[7] = 0;

    matrix[8] = (float)m_mTransformMatrix._13;
    matrix[9] = (float)m_mTransformMatrix._23;
    matrix[10] = (float)m_mTransformMatrix._33;
    matrix[11] = 0;

    matrix[12] = (float)m_mTransformMatrix._14;
    matrix[13] = (float)m_mTransformMatrix._24;
    matrix[14] = (float)m_mTransformMatrix._34;
    matrix[15] = 1;
}
// Returns value of transform matrix
Matrix4 RigidBody::GetTransform() const
{
    return m_mTransformMatrix;
}

Vector3 RigidBody::GetDirectionInLocalSpace(const Vector3 &direction) const
{
    return m_mTransformMatrix.TransformInverseDirection(direction);
}

Vector3 RigidBody::GetDirectionInWorldSpace(const Vector3 &direction) const
{
    return m_mTransformMatrix.TransformDirection(direction);
}

// Sets passed variable to the object's velocity
void RigidBody::GetVelocity(Vector3 &vVelocity) const
{
    vVelocity = m_vVelocity;
}
// Returns value of object's velocity
Vector3 RigidBody::GetVelocity() const
{
    return m_vVelocity;
}
// sets passed var with object's rotation value
void RigidBody::GetRotation(Vector3 &vRotation) const
{
    vRotation = m_vRotation;
}
// returns rotation value item
Vector3 RigidBody::GetRotation() const
{
    return m_vRotation;
}
// sets passed var with last frame acceleration's value
void RigidBody::GetLastFrameAcceleration(Vector3 &vAcceleration) const
{
    vAcceleration = m_vLastFrameAccel;
}
// returns last fram accel
Vector3 RigidBody::GetLastFrameAcceleration() const
{
    return m_vLastFrameAccel;
}
// sets passed var with acceleration value
void RigidBody::GetAcceleration(Vector3 &vAcceleration) const
{
    vAcceleration = m_vAcceleration;
}
// returns acceleration value
Vector3 RigidBody::GetAcceleration() const
{
    return m_vAcceleration;
}




////////////////
// MUTATORS
////////////////
// Sets object's mass
void RigidBody::SetMass(const float mass)
{
	// make sure mass does not equal 0
    assert(mass != 0);

	//set inverse mass
    m_fInverseMass = ((float)1.0)/mass;
		
}
// Sets an object' inverse mass 
void RigidBody::SetInverseMass(const float fInverseMass)
{
    m_fInverseMass = fInverseMass;
}
// Sets an object's Torque (rotational acceleration)
void RigidBody::SetTorque(Vector3 itorque)
{
	m_vTorque = itorque;
}// Sets the object's inertial tensor
void RigidBody::SetInertiaTensor(const Matrix3 &inertiaTensor)
{
	m_mInverseInertiaTensor.SetInverse(inertiaTensor);
}
// Sets the object's Inverse Inertial tensor
void SetInverseInertiaTensor(const Matrix3 &inverseInertiaTensor)
{

}
// transforms Inertia Tensor from body to world space
void RigidBody::TransformInertiaTensor(Matrix3 &iitWorld, const Quaternion &q, 
	const Matrix3 &iitBody, const Matrix4 &rotmat)
{
    float t4 = rotmat._11*iitBody.data[0]+
        rotmat._12*iitBody.data[3]+
        rotmat._13*iitBody.data[6];
    float t9 = rotmat._11*iitBody.data[1]+
        rotmat._12*iitBody.data[4]+
        rotmat._13*iitBody.data[7];
    float t14 = rotmat._11*iitBody.data[2]+
        rotmat._12*iitBody.data[5]+
        rotmat._13*iitBody.data[8];
    float t28 = rotmat._21*iitBody.data[0]+
        rotmat._22*iitBody.data[3]+
        rotmat._23*iitBody.data[6];
    float t33 = rotmat._21*iitBody.data[1]+
        rotmat._22*iitBody.data[4]+
        rotmat._23*iitBody.data[7];
    float t38 = rotmat._21*iitBody.data[2]+
        rotmat._22*iitBody.data[5]+
        rotmat._23*iitBody.data[8];
    float t52 = rotmat._31*iitBody.data[0]+
        rotmat._32*iitBody.data[3]+
        rotmat._33*iitBody.data[6];
    float t57 = rotmat._31*iitBody.data[1]+
        rotmat._32*iitBody.data[4]+
        rotmat._33*iitBody.data[7];
    float t62 = rotmat._31*iitBody.data[2]+
        rotmat._32*iitBody.data[5]+
        rotmat._33*iitBody.data[8];

    iitWorld.data[0] = t4*rotmat._11+
        t9*rotmat._12+
        t14*rotmat._13;
    iitWorld.data[1] = t4*rotmat._21+
        t9*rotmat._22+
        t14*rotmat._23;
    iitWorld.data[2] = t4*rotmat._31+
        t9*rotmat._32+
        t14*rotmat._33;
    iitWorld.data[3] = t28*rotmat._11+
        t33*rotmat._12+
        t38*rotmat._13;
    iitWorld.data[4] = t28*rotmat._21+
        t33*rotmat._22+
        t38*rotmat._23;
    iitWorld.data[5] = t28*rotmat._31+
        t33*rotmat._32+
        t38*rotmat._33;
    iitWorld.data[6] = t52*rotmat._11+
        t57*rotmat._12+
        t62*rotmat._13;
    iitWorld.data[7] = t52*rotmat._21+
        t57*rotmat._22+
        t62*rotmat._23;
    iitWorld.data[8] = t52*rotmat._31+
        t57*rotmat._32+
        t62*rotmat._33;

	
}
//sets both linear and angular damping
void RigidBody::SetDamping(const float fLinearDamping,
               const float fAngularDamping)
{
    m_fLinearDamping = fLinearDamping;
    m_fAngularDamping = fAngularDamping;
}
//sets linear damping
void RigidBody::SetLinearDamping(const float fLinearDamping)
{
    m_fLinearDamping = fLinearDamping;
}
//sets angular damping    	
void RigidBody::SetAngularDamping(const float fAngularDamping)
{
    m_fAngularDamping = fAngularDamping;
}
// sets orientation    
void RigidBody::SetOrientation(const Quaternion &qOrientation)
{
    m_qOrientation = qOrientation;
    m_qOrientation.Normalize();
}
// sets orientation by passing individual values
void RigidBody::SetOrientation(const float w, const float x,
                   const float y, const float z)
{
    m_qOrientation.w = w;
	m_qOrientation.x = x;
    m_qOrientation.y = y;
    m_qOrientation.z = z;
    m_qOrientation.Normalize();
}
// sets velocity 
void RigidBody::SetVelocity(const Vector3 &vVelocity)
{
    m_vVelocity = vVelocity;
}
// sets velocity by passing individual values
void RigidBody::SetVelocity(const float x, const float y, const float z)
{
    m_vVelocity.x = x;
    m_vVelocity.y = y;
    m_vVelocity.z = z;
}
// affects a change in velocity using the current velocity in the calculation
void RigidBody::AddVelocity(const Vector3 &deltaVelocity)
{
    m_vVelocity += deltaVelocity;
}
// sets the rotation value   
void RigidBody::SetRotation(const Vector3 &vRotation)
{
    m_vRotation = vRotation;
}
// sets rotation values individually
void RigidBody::SetRotation(const float x, const float y, const float z)
{
    m_vRotation.x = x;
    m_vRotation.y = y;
    m_vRotation.z = z;
}
// adds force to center of mass
void RigidBody::AddForce(const Vector3 &force)
{
    m_vForceAccum += force;
    m_bIsAwake = true;
}
// add force to point on object in world space
void RigidBody::AddForceAtPoint(const Vector3 &force,
                                const Vector3 &point)
{
    // Convert to coordinates relative to center of mass.
    Vector3 pt = point;
    pt -= m_vPosition;

    m_vForceAccum += force;
    m_vTorqueAccum += pt % force;

	m_bIsAwake = true;
}
// add force to point on object (in body space)
void RigidBody::AddForceAtBodyPoint(const Vector3 &force, const Vector3 &point)
{
	// Convert to coordinates relative to center of mass.
    Vector3 pt = GetPointInWorldSpace(point);
    AddForceAtPoint(force, pt);

    m_bIsAwake = true;
}
// adds torque (angular accel) to object
void RigidBody::AddTorque(const Vector3 &m_torque)
{
    m_vTorqueAccum += m_torque;
    m_bIsAwake = true;
} 
// adds rotation (angular vel) to object
void RigidBody::AddRotation(const Vector3 &deltaRotation)
{
    m_vRotation += deltaRotation;
}
// sets acceleration for object
void RigidBody::SetAcceleration(const Vector3 &vAcceleration)
{
	m_vAcceleration = vAcceleration;
}
// sets acceleration by individual axis
void RigidBody::SetAcceleration(const float x, const float y, const float z)
{
    m_vAcceleration.x = x;
    m_vAcceleration.y = y;
    m_vAcceleration.z = z;
}
// manually rotates body by given quaternion
void RigidBody::RotateBody(Quaternion &qRotation)
{
	m_qOrientation += qRotation;

	CalculateDerivedData();
}

void RigidBody::SetAwake(bool bAwake)
{
	if (bAwake)
	{
		m_bIsAwake = true;
		
		// add some m_fMotion to avoid it falling back asleep immediately
		m_fMotion = SLEEPEPSILON * 2.0F;

		SMI->SetColor(m_iGraphicsID, RED);
		
	}
	else
	{
		m_bIsAwake = false;
		m_vVelocity.Clear();
		m_vRotation.Clear();

		SMI->SetColor(m_iGraphicsID, GRAY);

	}
}