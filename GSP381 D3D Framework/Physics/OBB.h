#pragma once
#include "AABB.h"


class OBB :
	public AABB
{
private:
	
	// local axes for the rigid body 0-x,1-y,2-z(velocity)
	Vector3	m_vLocalAxes[3];
	
public:
	OBB();
	OBB(int id, Vector3 vPosition, Vector3 vHalfExtents, int iBodyID = -1);
	OBB(AABB &box);
	~OBB(void);

	int m_iGraphicsID;
	LPCSTR m_sBoneName;

	/////////////////
	// UTILITIES
	////////////////
	virtual void VUpdate(float dt);
	void CalculateLocalAxes();

	////////////////
	// ACCESSORS
	////////////////
	
	Vector3		GetLocalAxis(int iAxis) const {return m_vLocalAxes[iAxis];}


	////////////////
	// MUTATORS
	////////////////
	
	
	void AdjustLocalAxis(int iAxis, float fAmount);
};

