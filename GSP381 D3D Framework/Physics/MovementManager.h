#pragma once
#include <vector>
#include "vector3.h"
#include "Quaternion.h"
#include <map>
#include "Matrix4.h"

/*
	Movement Manager - handles the movement of entities that need to do so. Gives needed entities
	direct access to update positions, add forces, rotation, etc.

	Singleton to allow large scale protected access

*/


//forward declarations
class RigidBody;
class Ball;
class Box;

#define MMI MovementManager::Instance()

class MovementManager
{
private:
	MovementManager();

	// map of all rigid bodies, used for random access
	std::map<int, RigidBody*> m_mRigidBodies;

	//list of all moving rigid bodies - used for iterating through 
	std::vector<RigidBody *> m_vRigidBodies;
	
	// rigid body id system
	int m_iBodyID;
	

public:

	static MovementManager* Instance();
	~MovementManager();
	//calls update method for all moving entities
	void Update(float dt);


	///////////////////
	// REGISTRATION
	///////////////////
	//registers new rigid body
	void AddBody(RigidBody* pNewBody);
	// creates and registers bodies
	// creates a ball rigid body with a sphere graphics object and circle bv
	int CreateBall(Vector3 vPosition, float fRadius);
	// creates a box rigid body with a box graphics object and AABB bv
	int CreateBoxA(Vector3 vPosition, Vector3 vHalfExtents);
	// creates a box rigid body with a box graphics object and OBB bv
	int CreateBoxO(Vector3 vPosition, Vector3 vHalfExtents);
	// creates a half-space with a normal and an offset
	int CreateHalfSpace(Vector3 vNormal, float fOffset, bool display);
	// Creates a box, but with a sphere equal to the smallest half-extent
	int CreateBoxS(Vector3 vPosition, Vector3 vHalfExtents);

	///////////////////
	// DELETION
	///////////////////
	void DeleteBody(int iBodyID);
	void DeleteAllBodies();
	
	//////////////////
	// ACCESS
	//////////////////
	// returns rigid body ptr for access
	RigidBody* GetBody(int iBodyID);
	//returns ptr to ball, given an id
	Ball*	GetBall(int iBallID);
	// return ptr to box, given an id
	Box*	GetBox(int iBoxID);
	
	
	////////////////
	// ACCESSORS
	////////////////
	// finds all neighbors within a radius of a point
	std::vector<RigidBody*> GetNeighbors(Vector3 vCenter, float radius);
	// gets object's position
	Vector3 GetPosition(int iBodyID);

	// Gets object's mass
	float GetMass(int iBodyID) ;	
	// Returns object's inverse mass (if 0, means object is immobile)
	float GetInverseMass(int iBodyID) ;
	// Returns a bool telling if an object can move or not
	bool HasFiniteMass(int iBodyID) ;
	
	// Returns the value of the object's inertial tensor in body space
	Matrix3 GetInertiaTensor(int iBodyID) ;
	
	// Returns the value of the object's inertial tensor in world space 
    Matrix3 GetInertiaTensorWorld(int iBodyID) ;
	
	// Returns value of object's inverse inertial tensor in body space
	Matrix3 GetInverseInertiaTensor(int iBodyID) ;
	
	// Returns value of object's inverse inertial tensor
	Matrix3 GetInverseInertiaTensorWorld(int iBodyID) ;
	// Returns Linear Damping Value
	float GetLinearDamping(int iBodyID) ;
	// REturns Angular Damping Value
	float GetAngularDamping(int iBodyID) ;
	
	// Returns the values of the object's orientation
    Quaternion GetOrientation(int iBodyID ) ;
	
	// Returns value of transform matrix
    Matrix4 GetTransform(int iBodyID) ;
	
    // Returns value of object's velocity
	Vector3 GetVelocity(int iBodyID) ;
	
	// returns rotation value item
    Vector3 GetRotation(int iBodyID) ;
	
	// returns last fram accel
    Vector3 GetLastFrameAcceleration(int iBodyID) ;
	
	// returns acceleration value
    Vector3 GetAcceleration(int iBodyID) ;
	
	

	////////////////////
	//  MUTATORS
	///////////////////
	// sets position of body
	void SetPosition(int iBodyID, Vector3 vPosition);
	//adds force to COM to the specified body id
	void AddForce(int iBodyID, const Vector3 &vForce);
	// sets orientation   
	void SetOrientation(int iBodyID, const Quaternion &orientation);
    // sets orientation by passing individual values
	void SetOrientation(int iBodyID, const float w, const float x,const float y, const float z);
    // sets velocity 
	void SetVelocity(int iBodyID, const Vector3 &velocity);
    // sets velocity by passing individual values
	void SetVelocity(int iBodyID, const float x, const float y, const float z);
    // affects a change in velocity using the current velocity in the calculation
    void AddVelocity(int iBodyID, const Vector3 &deltaVelocity);
    // sets the rotation value   
	void SetRotation(int iBodyID, const Vector3 &rotation);
    // sets rotation values individually
	void SetRotation(int iBodyID, const float x, const float y, const float z);
	// add force to point on object in world space
    void AddForceAtPoint(int iBodyID, const Vector3 &force, const Vector3 &point);
	// manuall rotates by the given quaternion
	void RotateBody(int iBodyID, Quaternion &qRotation);
	// add force to point on object (in body space)
    void AddForceAtBodyPoint(int iBodyID, const Vector3 &force, const Vector3 &point);
	// adds torque (angular accel) to object
    void AddTorque(int iBodyID, const Vector3 &torque);  
	// adds rotation (angular vel) to object
	void AddRotation(int iBodyID, const Vector3 &deltaRotation);
	// sets acceleration for object
    void SetAcceleration(int iBodyID, const Vector3 &acceleration);
    // sets acceleration by individual axis
	void SetAcceleration(int iBodyID, const float x, const float y, const float z);
	// sets mass of object
	void SetMass(int iBodyID, const float fMass);
	// sets mass of object
	void SetInverseMass(int iBodyID, const float invMass);
	

};

