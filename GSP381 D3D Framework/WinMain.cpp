// GSP381 D3D Framework.cpp : Defines the entry point for the application.
//

//basic header files for windows
#include "stdafx.h"
#include "Graphics/D3DApp.h"
#include "Timer.h"
#include "Input/DirectInput.h"

//definitions

// define the screen resolution
#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

//PROTOTYPES
// the WindowsProc function prototype
LRESULT CALLBACK WindowProc(	HWND hWnd,
								UINT message,
								WPARAM wParam,
								LPARAM lParam);

//entry point for any windows program
int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPSTR lpCmdLine,
					int nCmdShow)
{
	


	//the handle for the window, filled by a function
	HWND hWnd;
	//this struct holds info for the window  class
	WNDCLASSEX wc;



	//clear out the window class for use
	ZeroMemory(&wc, sizeof(WNDCLASSEX) );

	//fill in the struct with the needed information
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) COLOR_WINDOW;
	wc.lpszClassName = L"WindowClass1";

	//register the window class
	RegisterClassEx(&wc);

	// create the window and use the result as the handle
    hWnd = CreateWindowEx(NULL,
                          L"WindowClass1",    // name of the window class
                          L"GSP381 DirectX Framework",   // title of the window
                          WS_OVERLAPPEDWINDOW,    // window style
                          300,    // x-position of the window
                          300,    // y-position of the window
                          SCREEN_WIDTH,    // width of the window
                          SCREEN_HEIGHT,    // height of the window
                          NULL,    // we have no parent window, NULL
                          NULL,    // we aren't using menus, NULL
                          hInstance,    // application handle
                          NULL);    // used with multiple windows, NULL

    // display the window on the screen
    ShowWindow(hWnd, nCmdShow);

	

	//set up and initialize Direct3D
	D3DAPPI->InitD3D(hWnd,hInstance,true);

	DirectInput di(DISCL_NONEXCLUSIVE|DISCL_FOREGROUND, DISCL_NONEXCLUSIVE|DISCL_FOREGROUND);
	gDInput = &di;

	
	//start timer
	DXTIMER->Clear();
	DXTIMER->Start();

	//run program
	return D3DAPPI->Run();

}

// this is the main message handler for the program
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// Is the application in a minimized or maximized state?
	static bool minOrMaxed = false;

	RECT clientRect = {0, 0, 0, 0};
	switch( message )
	{

	// WM_ACTIVE is sent when the window is activated or deactivated.
	// We pause the game when the main window is deactivated and 
	// unpause it when it becomes active.
	//case WM_ACTIVATE:
	//	if( LOWORD(wParam) == WA_INACTIVE )
	//		mAppPaused = true;
	//	else
	//		mAppPaused = false;
	//	return 0;


	// WM_SIZE is sent when the user resizes the window.  
	case WM_SIZE:
		if( D3DAPPI->GetD3DDevice() )
		{
			D3DAPPI->m_D3Dpp.BackBufferWidth  = LOWORD(lParam);
			D3DAPPI->m_D3Dpp.BackBufferHeight = HIWORD(lParam);

			if( wParam == SIZE_MINIMIZED )
			{
				D3DAPPI->PauseApp();
				minOrMaxed = true;
			}
			else if( wParam == SIZE_MAXIMIZED )
			{
				D3DAPPI->UnpauseApp();
				minOrMaxed = true;
				D3DAPPI->OnLostDevice();
				D3DAPPI->GetD3DDevice()->Reset(&D3DAPPI->m_D3Dpp);
				D3DAPPI->OnResetDevice();
			}
			// Restored is any resize that is not a minimize or maximize.
			// For example, restoring the window to its default size
			// after a minimize or maximize, or from dragging the resize
			// bars.
			else if( wParam == SIZE_RESTORED )
			{
				D3DAPPI->UnpauseApp();

				// Are we restoring from a mimimized or maximized state, 
				// and are in windowed mode?  Do not execute this code if 
				// we are restoring to full screen mode.
				if( minOrMaxed && D3DAPPI->m_D3Dpp.Windowed )
				{
					D3DAPPI->OnLostDevice();
					D3DAPPI->GetD3DDevice()->Reset(&D3DAPPI->m_D3Dpp);
					D3DAPPI->OnResetDevice();
				}
				else
				{
					// No, which implies the user is resizing by dragging
					// the resize bars.  However, we do not reset the device
					// here because as the user continuously drags the resize
					// bars, a stream of WM_SIZE messages is sent to the window,
					// and it would be pointless (and slow) to reset for each
					// WM_SIZE message received from dragging the resize bars.
					// So instead, we reset after the user is done resizing the
					// window and releases the resize bars, which sends a
					// WM_EXITSIZEMOVE message.
				}
				minOrMaxed = false;
			}
		}
		return 0;


	// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
	// Here we reset everything based on the new window dimensions.
	case WM_EXITSIZEMOVE:
		GetClientRect(hWnd, &clientRect);
		D3DAPPI->m_D3Dpp.BackBufferWidth  = clientRect.right;
		D3DAPPI->m_D3Dpp.BackBufferHeight = clientRect.bottom;
		D3DAPPI->OnLostDevice();
		D3DAPPI->GetD3DDevice()->Reset(&D3DAPPI->m_D3Dpp);
		D3DAPPI->OnResetDevice();

		return 0;

	// WM_CLOSE is sent when the user presses the 'X' button in the
	// caption bar menu.
	case WM_CLOSE:
		DestroyWindow(hWnd);
		return 0;

	// WM_DESTROY is sent when the window is being destroyed.
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_KEYDOWN:
		if( wParam == VK_ESCAPE )
			D3DAPPI->EnableFullScreenMode(false);
		else if( wParam == 'F' )
			D3DAPPI->EnableFullScreenMode(true);
		return 0;

	//case WM_INPUT:
	//	{
	//		// this is where we read the device data
	//		// Determine how big the buffer should be
	//		UINT bufferSize;
	//		GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &bufferSize, sizeof (RAWINPUTHEADER));

	//		// Create a buffer of the correct size - but see note below
	//		BYTE *buffer=new BYTE[bufferSize];
	//		
	//		// Call the function again, this time with the buffer to get the data
	//		GetRawInputData((HRAWINPUT)lParam, RID_INPUT, (LPVOID)buffer, &bufferSize, sizeof (RAWINPUTHEADER));

	//		RAWINPUT *raw = (RAWINPUT*) buffer;
	//		if (raw->header.dwType== RIM_TYPEMOUSE)
	//		{
	//			// read the mouse data
	//			InputDevices->SetLeftMouseDown(raw->data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_DOWN);
	//			InputDevices->SetLeftMouseUp(raw->data.mouse.ulButtons & RI_MOUSE_LEFT_BUTTON_UP);
	//			InputDevices->SetRightMouseDown(raw->data.mouse.ulButtons & RI_MOUSE_RIGHT_BUTTON_DOWN);
	//			InputDevices->SetRightMouseUp(raw->data.mouse.ulButtons & RI_MOUSE_RIGHT_BUTTON_UP);
	//			
	//		
	//		}
	//		else if (raw->header.dwType== RIM_TYPEKEYBOARD)
	//		{

	//			InputDevices->SetKeyCode(raw->data.keyboard.VKey);
	//			InputDevices->SetKeyUp(raw->data.keyboard.Flags & RI_KEY_BREAK);

	//		}
	//	break;
	//	}
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}
	
        








