<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:Matrix4.TransformDirection(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix4.cpp" line="56">
 Transform the given direction vector by this matrix.

 @note When a direction is converted between frames of
 reference, there is no translation required.

 @param vector The vector to transform.

</member>
<member name="M:Matrix4.TransformInverseDirection(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix4.cpp" line="81">
 Transform the given direction vector by the
 transformational inverse of this matrix.

 @note This function relies on the fact that the inverse of
 a pure rotation matrix is its transpose. It separates the
 translational and rotation components, transposes the
 rotation, and multiplies out. If the matrix is not a
 scale and shear free transform matrix, then this function
 will not give correct results.

 @note When a direction is converted between frames of
 reference, there is no translation required.

 @param vector The vector to transform.

</member>
<member name="M:Matrix4.SetOrientationAndPos(Quaternion!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix4.cpp" line="128">
Sets this matrix to be the rotation matrix corresponding to
the given quaternion.

</member>
<member name="M:Matrix4.GetAxisVector(System.Int32)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix4.cpp" line="350">
 Gets a vector representing one axis (i.e. one column) in the matrix.

 @param i The row to return. Row 3 corresponds to the position
 of the transform matrix.

 @return The vector.

</member>
</members>
</doc>