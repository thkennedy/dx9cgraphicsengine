<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:BaseGameEntity.VUpdate(System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\objects\basegameentity.h" line="20">
<summary>
All Entity Updates Processed Here
</summary>
<param name="dt">Change in time from last frame</param>
<returns>No Return</returns>
</member>
<member name="M:Spring.#ctor(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,RigidBody*,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="138">
Creates a new spring with the given parameters. 
</member>
<member name="F:AnchoredSpring.m_anchor" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="160">
The location of the anchored end of the spring. 
</member>
<member name="F:AnchoredSpring.m_springConstant" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="163">
Holds the sprint constant. 
</member>
<member name="F:AnchoredSpring.m_restLength" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="166">
Holds the rest length of the spring. 
</member>
<member name="M:AnchoredSpring.#ctor(Vector3*,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="172">
Creates a new spring with the given parameters. 
</member>
<member name="M:AnchoredSpring.getAnchor" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="176">
Retrieve the anchor point. 
</member>
<member name="M:AnchoredSpring.init(Vector3*,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="179">
Set the spring's properties. 
</member>
<member name="M:AnchoredSpring.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="182">
Applies the spring force to the given RigidBody. 
</member>
<member name="F:Bungee.m_other" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="220">
The RigidBody at the other end of the spring. 
</member>
<member name="F:Bungee.m_springConstant" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="223">
Holds the spring constant. 
</member>
<member name="M:Bungee.#ctor(RigidBody*,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="231">
Creates a new bungee with the given parameters. 
</member>
<member name="M:Bungee.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="234">
Applies the spring force to the given RigidBody. 
</member>
<member name="M:AnchoredBungee.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="253">
Applies the spring force to the given RigidBody. 
</member>
<member name="F:Buoyancy.m_volume" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="270">
The volume of the object.

</member>
<member name="F:Buoyancy.m_waterHeight" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="275">
The height of the water plane above y=0. The plane will be
parrallel to the XZ plane.

</member>
<member name="F:Buoyancy.m_liquidDensity" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="281">
The density of the liquid. Pure water has a density of
1000kg per cubic meter.

</member>
<member name="M:Buoyancy.#ctor(System.Single,System.Single,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="289">
Creates a new buoyancy force with the given parameters. 
</member>
<member name="M:Buoyancy.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="292">
Applies the buoyancy force to the given RigidBody. 
</member>
<member name="M:ContactResolver.#ctor(System.UInt32,System.UInt32,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contacts.h" line="139">
Creates a new contact resolver with the given number of iterations
for each kind of resolution, and optional epsilon values.

</member>
<member name="M:ContactResolver.ResolveContacts(Contact*,System.UInt32,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contacts.h" line="168">
		book comments left because they're helpful

        * Resolves a set of contacts for both m_fPenetration and velocity.
        *
        * Contacts that cannot interact with
        * each other should be passed to separate calls to ResolveContacts,
        * as the resolution algorithm takes much longer for lots of
        * contacts than it does for the same number of contacts in small
        * sets.
        *
        * @param contactArray Pointer to an array of contact objects.
        *
        * @param numContacts The number of contacts in the array to resolve.
        *
        * @param numIterations The number of iterations through the
        * resolution algorithm. This should be at least the number of
        * contacts (otherwise some constraints will not be resolved -
        * although sometimes this is not noticable). If the iterations are
        * not needed they will not be used, so adding more iterations may
        * not make any difference. In some cases you would need millions
        * of iterations. Think about the number of iterations as a bound:
        * if you specify a large number, sometimes the algorithm WILL use
        * it, and you may drop lots of frames.
        *
        * @param dt The dt of the previous integration step.
        * This is used to compensate for forces applied.

</member>
<member name="M:ContactResolver.AdjustPositions(Contact*,System.UInt32,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contacts.h" line="205">
Resolves the positional issues with the given array of constraints,
using the given number of iterations.

</member>
<member name="D:int8_t" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\containers\tree-2.81\src\tree.hh" line="13">
\mainpage tree.hh
    \author   Kasper Peeters
    \version  2.81
    \date     23-Aug-2011
    \see      http://tree.phi-sci.com/
    \see      http://tree.phi-sci.com/ChangeLog

   The tree.hh library for C++ provides an STL-like container class
   for n-ary trees, templated over the data stored at the
   nodes. Various types of iterators are provided (post-order,
   pre-order, and others). Where possible the access methods are
   compatible with the STL or alternative algorithms are
   available. 

</member>
<member name="M:IntersectionTests.BoxAndHalfSpace(OBB!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Plane!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="37">
 Does an intersection test on an arbitrarily aligned box and a
 half-space.

 The box is given as a transform matrix, including
 position, and a vector of half-sizes for the extend of the
 box along each local axis.

 The half-space is given as a direction (i.e. unit) vector and the
 offset of the limiting plane from the origin, along the given
 direction.

</member>
<member name="T:CollisionData" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="69">
A helper structure that contains information for the detector to use
in building its contact data.

</member>
<member name="F:CollisionData.m_pContactArray" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="75">
Holds the base of the collision data: the first contact
in the array. This is used so that the contact pointer (below)
can be incremented each time a contact is detected, while
this pointer points to the first contact found.

</member>
<member name="F:CollisionData.m_pContacts" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="83">
Holds the contact array to write into. 
</member>
<member name="F:CollisionData.m_iContactsLeft" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="86">
Holds the maximum number of m_pContacts the array can take. 
</member>
<member name="F:CollisionData.m_uContactCount" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="89">
Holds the number of m_pContacts found so far. 
</member>
<member name="F:CollisionData.m_fFriction" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="92">
Holds the m_fFriction value to write into any collisions. 
</member>
<member name="F:CollisionData.m_fRestitution" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="95">
Holds the m_fRestitution value to write into any collisions. 
</member>
<member name="F:CollisionData.m_fTolerance" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="98">
Holds the collision m_fTolerance, even uncolliding objects this
close should have collisions generated.

</member>
<member name="M:CollisionData.HasMoreContacts" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="104">
Checks if there are more m_pContacts available in the contact
data.

</member>
<member name="M:CollisionData.Reset(System.UInt32)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="113">
Resets the data so that it has no used m_pContacts recorded.

</member>
<member name="M:CollisionData.AddContacts(System.UInt32)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="123">
Notifies the data that the given number of m_pContacts have
been added.

</member>
<member name="T:ACollisionDetector" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="138">
 A wrapper class that holds the fine grained collision detection
 routines.

 Each of the functions has the same format: it takes the details
 of two objects, and a pointer to a contact array to fill. It
 returns the number of m_pContacts it wrote into the array.

</member>
<member name="M:ACollisionDetector.AdvSphereAndBox(AdvancedBV!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Circle!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,tree&lt;BoundingVolume**&gt;.pre_order_iterator*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,CollisionData*,OBB*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Boolean*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="173">
Recursive Method for BVH Methods *
</member>
<member name="M:ACollisionDetector.BoxAndHalfSpace(OBB!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Plane!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,CollisionData*)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="185">
Does a collision test on a collision box and a plane representing
a half-space (i.e. the normal of the plane
points out of the half-space).

</member>
<member name="M:ACollisionDetector.AdvBoxAndBox(AdvancedBV!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,OBB!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,tree&lt;BoundingVolume**&gt;.pre_order_iterator*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,CollisionData*,OBB*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Boolean*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="221">
Recursive Method for BVH Methods *
</member>
<member name="F:AdvCollisions.maxContacts" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="247">
Holds the maximum number of contacts. 
</member>
<member name="F:AdvCollisions.m_Contacts" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="250">
Holds the array of contacts. 
</member>
<member name="F:AdvCollisions.m_cData" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="253">
Holds the collision data structure for collision detection. 
</member>
<member name="F:AdvCollisions.m_Resolver" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="256">
Holds the contact resolver. 
</member>
<member name="M:AdvCollisions.GenerateContacts" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="259">
Processes the contact generation code. 
</member>
<member name="M:AdvCollisions.Update(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advcollisions.h" line="284">
Update the objects. 
</member>
<member name="T:ContactGenerator" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="10">
This is the basic polymorphic interface for contact generators
applying to rigid bodies.

</member>
<member name="F:Joint.body" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="75">
Holds the two rigid bodies that are connected by this joint.

</member>
<member name="F:Joint.position" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="80">
Holds the relative location of the connection for each
body, given in local coordinates.

</member>
<member name="F:Joint.error" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="86">
Holds the maximum displacement at the joint before the
joint is considered to be violated. This is normally a
small, epsilon value.  It can be larger, however, in which
case the joint will behave as if an inelastic cable joined
the bodies at their joint locations.

</member>
<member name="M:Joint.Set(RigidBody*,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,RigidBody*,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="95">
Configures the joint in one go.

</member>
<member name="M:Joint.AddContact(Contact*,System.UInt32)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="104">
Generates the contacts required to restore the joint if it
has been violated.

</member>
<member name="F:Cable.body" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="114">
Holds the two rigid bodies that are connected by this joint.

</member>
<member name="F:Cable.position" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="119">
Holds the relative location of the connection for each
body, given in local coordinates.

</member>
<member name="F:Cable.length" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="125">
Holds the maximum displacement at the joint before the
joint is considered to be violated. This is normally a
small, epsilon value.  It can be larger, however, in which
case the joint will behave as if an inelastic cable joined
the bodies at their joint locations.

</member>
<member name="M:Cable.Set(RigidBody*,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,RigidBody*,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="134">
Configures the joint in one go.

</member>
<member name="M:Cable.AddContact(Contact*,System.UInt32)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contactgenerator.h" line="143">
Generates the contacts required to restore the joint if it
has been violated.

</member>
<member name="M:AdvancedBV.#ctor(System.Int32,System.Int32,Vector3,System.Single,System.Boolean,Vector3,Vector3)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advancedbv.h" line="57">
	Para const for Advanced BV - only thing required is the id - other params
	iBodyID - Rigid Body ID
	vCenter - center of mass for model. default is the center computed by D3DXComputeBoundingSphere
	**NOTE** this is m_vPosition of the entire advanced BV, but not necessarilly the center of 
	fRadius - radius of bounding sphere as computed by D3DXComputeBoundingSphere
	vMin - lower left corner of bounding box calculated by D3DXComputeBoundingBox 
	vMax - upper right corner of bounding box calculated by D3DXComputeBoundingBox 

	*
</member>
</members>
</doc>