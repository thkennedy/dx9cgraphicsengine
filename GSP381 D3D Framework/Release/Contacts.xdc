<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:ContactResolver.#ctor(System.UInt32,System.UInt32,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contacts.h" line="139">
Creates a new contact resolver with the given number of iterations
for each kind of resolution, and optional epsilon values.

</member>
<member name="M:ContactResolver.ResolveContacts(Contact*,System.UInt32,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contacts.h" line="168">
		book comments left because they're helpful

        * Resolves a set of contacts for both m_fPenetration and velocity.
        *
        * Contacts that cannot interact with
        * each other should be passed to separate calls to ResolveContacts,
        * as the resolution algorithm takes much longer for lots of
        * contacts than it does for the same number of contacts in small
        * sets.
        *
        * @param contactArray Pointer to an array of contact objects.
        *
        * @param numContacts The number of contacts in the array to resolve.
        *
        * @param numIterations The number of iterations through the
        * resolution algorithm. This should be at least the number of
        * contacts (otherwise some constraints will not be resolved -
        * although sometimes this is not noticable). If the iterations are
        * not needed they will not be used, so adding more iterations may
        * not make any difference. In some cases you would need millions
        * of iterations. Think about the number of iterations as a bound:
        * if you specify a large number, sometimes the algorithm WILL use
        * it, and you may drop lots of frames.
        *
        * @param dt The dt of the previous integration step.
        * This is used to compensate for forces applied.

</member>
<member name="M:ContactResolver.AdjustPositions(Contact*,System.UInt32,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\contacts.h" line="205">
Resolves the positional issues with the given array of constraints,
using the given number of iterations.

</member>
<member name="M:BaseGameEntity.VUpdate(System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\objects\basegameentity.h" line="20">
<summary>
All Entity Updates Processed Here
</summary>
<param name="dt">Change in time from last frame</param>
<returns>No Return</returns>
</member>
</members>
</doc>