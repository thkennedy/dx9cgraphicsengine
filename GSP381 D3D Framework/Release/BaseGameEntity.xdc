<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:BaseGameEntity.VUpdate(System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\objects\basegameentity.h" line="20">
<summary>
All Entity Updates Processed Here
</summary>
<param name="dt">Change in time from last frame</param>
<returns>No Return</returns>
</member>
</members>
</doc>