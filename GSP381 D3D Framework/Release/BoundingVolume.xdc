<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:BoundingVolume.GetAxis(System.UInt32)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\boundingvolume.cpp" line="61">
This is a convenience function to allow access to the
axis vectors in the transform for this primitive.

</member>
<member name="M:BoundingVolume.GetTransform" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\boundingvolume.cpp" line="70">
Returns the resultant transform of the primitive, calculated from
the combined offset of the primitive and the transform
(orientation + position) of the rigid body to which it is
attached.

</member>
</members>
</doc>