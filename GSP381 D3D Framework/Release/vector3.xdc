<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:Vector3.ComponentProductUpdate(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.cpp" line="106">
Performs a component-wise product with the given vector and
sets this vector to its result.

</member>
<member name="M:Vector3.AddScaledVector(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.cpp" line="139">
Adds the given vector to this, scaled by the given amount.

</member>
<member name="M:Vector3.op_SubtractionAssignment(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.cpp" line="199">
Subtracts the given vector from this. 
</member>
<member name="M:Vector3.op_AdditionAssignment(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.cpp" line="207">
Adds the given vector to this. 
</member>
<member name="M:Vector3.op_Addition(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.cpp" line="215">
Returns the value of the given vector added to this.

</member>
<member name="M:Vector3.op_MultiplicationAssignment(System.Single!System.Runtime.CompilerServices.IsConst)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.cpp" line="222">
Multiplies this vector by the given scalar. 
</member>
</members>
</doc>