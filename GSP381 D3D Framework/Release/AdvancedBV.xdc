<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="D:int8_t" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\containers\tree-2.81\src\tree.hh" line="13">
\mainpage tree.hh
    \author   Kasper Peeters
    \version  2.81
    \date     23-Aug-2011
    \see      http://tree.phi-sci.com/
    \see      http://tree.phi-sci.com/ChangeLog

   The tree.hh library for C++ provides an STL-like container class
   for n-ary trees, templated over the data stored at the
   nodes. Various types of iterators are provided (post-order,
   pre-order, and others). Where possible the access methods are
   compatible with the STL or alternative algorithms are
   available. 

</member>
<member name="M:AdvancedBV.#ctor(System.Int32,System.Int32,Vector3,System.Single,System.Boolean,Vector3,Vector3)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advancedbv.h" line="57">
	Para const for Advanced BV - only thing required is the id - other params
	iBodyID - Rigid Body ID
	vCenter - center of mass for model. default is the center computed by D3DXComputeBoundingSphere
	**NOTE** this is m_vPosition of the entire advanced BV, but not necessarilly the center of 
	fRadius - radius of bounding sphere as computed by D3DXComputeBoundingSphere
	vMin - lower left corner of bounding box calculated by D3DXComputeBoundingBox 
	vMax - upper right corner of bounding box calculated by D3DXComputeBoundingBox 

	*
</member>
<member name="M:AdvancedBV.#ctor(System.Int32,System.Int32,Vector3,System.Single,System.Boolean,Vector3,Vector3)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\advancedbv.cpp" line="15">
Full const for Advanced BV - only thing required is the id - other params
iBodyID - Rigid Body ID
vCenter - center of mass for model. default is the center computed by D3DXComputeBoundingSphere
**NOTE** this is m_vPosition of the entire advanced BV, but not necessarilly the center of 
fRadius - radius of bounding sphere as computed by D3DXComputeBoundingSphere
vMin - lower left corner of bounding box calculated by D3DXComputeBoundingBox 
vMax - upper right corner of bounding box calculated by D3DXComputeBoundingBox 

*
</member>
</members>
</doc>