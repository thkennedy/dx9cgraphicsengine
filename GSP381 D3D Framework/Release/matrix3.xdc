<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:Matrix3.op_Multiply(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="144">
 Transform the given vector by this matrix.

 @param vector The vector to transform.

</member>
<member name="M:Matrix3.SetOrientation(Quaternion!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="235">
Sets this matrix to be the rotation matrix corresponding to
the given quaternion.

</member>
<member name="M:Matrix3.SetComponents(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="480">
Sets the matrix values from the given three vector components.
These are arranged as the three columns of the vector.

</member>
<member name="M:Matrix3.Transform(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="510">
 Transform the given vector by this matrix.

 @param vector The vector to transform.

</member>
<member name="M:Matrix3.transformTranspose(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="520">
 Transform the given vector by the transpose of this matrix.

 @param vector The vector to transform.

</member>
<member name="M:Matrix3.SetDiagonal(System.Single,System.Single,System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="540">
Sets the matrix to be a diagonal matrix with the given
values along the leading diagonal.

</member>
<member name="M:Matrix3.SetInertiaTensorCoeffs(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="549">
Sets the value of the matrix from inertia tensor values.

</member>
<member name="M:Matrix3.SetBlockInertiaTensor(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="571">
Sets the value of the matrix as an inertia tensor of
a rectangular block aligned with the body's coordinate
system with the given axis half-sizes and mass.

</member>
<member name="M:Matrix3.SetSkewSymmetric(Vector3!System.Runtime.CompilerServices.IsConst)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="584">
Sets the matrix to be a skew symmetric matrix based on
the given vector. The skew symmetric matrix is the equivalent
of the vector product. So if a,b are vectors. a x b = A_s b
where A_s is the skew symmetric form of a.

</member>
<member name="M:Matrix3.Inverse" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="611">
Returns a new matrix containing the inverse of this matrix. 
</member>
<member name="M:Matrix3.Invert" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="619">
Inverts the matrix.

</member>
<member name="M:Matrix3.GetRowVector(System.Int32)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="627">
 Gets a vector representing one row in the matrix.

 @param i The row to return.

</member>
<member name="M:Matrix3.GetAxisVector(System.Int32)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.cpp" line="637">
 Gets a vector representing one axis (i.e. one column) in the matrix.

 @param i The row to return.

 @return The vector.

</member>
</members>
</doc>