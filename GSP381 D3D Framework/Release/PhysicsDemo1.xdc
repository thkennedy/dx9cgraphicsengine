<?xml version="1.0"?><doc>
<members>
<member name="M:Matrix3.linearInterpolate(Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,Matrix3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\matrix3.h" line="61">
Interpolates a couple of matrices.

</member>
<member name="M:Vector3.Scale(System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\vector3.h" line="38">
MUTATORS - directly affects the stored value
</member>
<member name="M:BaseGameEntity.VUpdate(System.Single)" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\objects\basegameentity.h" line="20">
<summary>
All Entity Updates Processed Here
</summary>
<param name="dt">Change in time from last frame</param>
<returns>No Return</returns>
</member>
<member name="M:Spring.#ctor(Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,RigidBody*,Vector3!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="138">
Creates a new spring with the given parameters. 
</member>
<member name="F:AnchoredSpring.m_anchor" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="160">
The location of the anchored end of the spring. 
</member>
<member name="F:AnchoredSpring.m_springConstant" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="163">
Holds the sprint constant. 
</member>
<member name="F:AnchoredSpring.m_restLength" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="166">
Holds the rest length of the spring. 
</member>
<member name="M:AnchoredSpring.#ctor(Vector3*,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="172">
Creates a new spring with the given parameters. 
</member>
<member name="M:AnchoredSpring.getAnchor" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="176">
Retrieve the anchor point. 
</member>
<member name="M:AnchoredSpring.init(Vector3*,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="179">
Set the spring's properties. 
</member>
<member name="M:AnchoredSpring.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="182">
Applies the spring force to the given RigidBody. 
</member>
<member name="F:Bungee.m_other" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="220">
The RigidBody at the other end of the spring. 
</member>
<member name="F:Bungee.m_springConstant" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="223">
Holds the spring constant. 
</member>
<member name="M:Bungee.#ctor(RigidBody*,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="231">
Creates a new bungee with the given parameters. 
</member>
<member name="M:Bungee.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="234">
Applies the spring force to the given RigidBody. 
</member>
<member name="M:AnchoredBungee.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="253">
Applies the spring force to the given RigidBody. 
</member>
<member name="F:Buoyancy.m_volume" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="270">
The volume of the object.

</member>
<member name="F:Buoyancy.m_waterHeight" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="275">
The height of the water plane above y=0. The plane will be
parrallel to the XZ plane.

</member>
<member name="F:Buoyancy.m_liquidDensity" decl="false" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="281">
The density of the liquid. Pure water has a density of
1000kg per cubic meter.

</member>
<member name="M:Buoyancy.#ctor(System.Single,System.Single,System.Single,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="289">
Creates a new buoyancy force with the given parameters. 
</member>
<member name="M:Buoyancy.UpdateForce(RigidBody*,System.Single)" decl="true" source="d:\code\graphics\dx9cgraphicsengine\gsp381 d3d framework\physics\forcegenerator.h" line="292">
Applies the buoyancy force to the given RigidBody. 
</member>
</members>
</doc>