#pragma once

#include "state.h"
#include "Physics\functions.h"
#include "Physics\vector3.h"
#include <list>




//foward declaration to avoid circular include issues
class Carrier;
class Sphere;
class Dijkstra;

#define CFlock	CarrierStateFlock::Instance()
#define CEscape	CarrierStateEscape::Instance()
#define CAggresive	CarrierStateWander::Instance()
#define CPath	CarrierStateFollowPath::Instance()
#define CWinLevel CarrierStateWinLevel::Instance()


class CarrierStateFlock:public State<Carrier>
{
private:
	CarrierStateFlock(){}


public:
	static CarrierStateFlock* Instance();
	void Enter(Carrier* pCarrier);
	void Execute(Carrier* pCarrier);
	void Exit(Carrier* pCarrier);
	bool OnMessage(Carrier* pCarrier, const Mail& message);

	
	
};

class CarrierStateEscape: public State<Carrier>
{
private:
	CarrierStateEscape() {}

public:
	static CarrierStateEscape* Instance();
	void Enter(Carrier* pCarrier);
	void Execute(Carrier* pCarrier);
	void Exit(Carrier* pCarrier);
	bool OnMessage(Carrier* pCarrier, const Mail& message);
	
};

class CarrierStateWander: public State<Carrier>
{
private:
	CarrierStateWander();

	real m_rWanderRadius;
	real m_rWanderDistance;
	real m_rWanderJitter;
	vector3 m_vWanderTarget, m_vWanderCircle;
	Sphere * m_gWander, *m_gWanderTarget;
	int m_gWanderSphereID, m_gWanderTargetID;


public:

	static CarrierStateWander* Instance();
	void Enter(Carrier* pCarrier);
	void Execute(Carrier* pCarrier);
	void Exit(Carrier* pCarrier);
	bool OnMessage(Carrier* pCarrier, const Mail& message);



	
};


//  ****************************
// ******   PATH     **********
//****************************

class CarrierStateFollowPath: public State<Carrier>
{
private:
	CarrierStateFollowPath(){}
	
	int m_iStartingNode, m_iTargetNode/*, m_iCurrentNode, m_iNextNode*/;
	Dijkstra* m_Seeker;
	//std::list<int>::iterator m_CurrentNode;
	std::list<int> m_lCurrentPath;

	
public:

	static CarrierStateFollowPath* Instance();
	void Enter(Carrier* pCarrier);
	void Execute(Carrier* pCarrier);
	void Exit(Carrier* pCarrier);
	bool OnMessage(Carrier* pCarrier, const Mail& message);



	
};

//  ****************************
// ******    WIN     **********
//****************************

class CarrierStateWinLevel: public State<Carrier>
{
private:
	CarrierStateWinLevel() {}

public:

	static CarrierStateWinLevel* Instance();
	void Enter(Carrier* pCarrier);
	void Execute(Carrier* pCarrier);
	void Exit(Carrier* pCarrier);
	bool OnMessage(Carrier* pCarrier, const Mail& message);

};




class CarrierStates
{
protected:

public:
	CarrierStates();
	~CarrierStates();

	State<Carrier> *Flock;
	State<Carrier> *Aggresive;
	State<Carrier> *Escape;
};