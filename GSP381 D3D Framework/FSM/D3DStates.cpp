#include "D3DStates.h"
#include "../Graphics/D3DApp.h"
#include "../Graphics/D3DSprite.h"
#include "../Graphics/Sprites/D3DSpriteManager.h"
#include "../Objects/MenuButton.h"
#include "../Objects/D3DMenu.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/GfxStats.h"
#include "../Sound/SoundSys.h"
#include "../Sound/SoundEffect.h"
#include "../Input/DirectInput.h"
#include <iostream>
#include <string>
#include "../Graphics/D3DVertex.h"
#include "../Graphics/D3DPyramid.h"
#include "../Graphics/D3DCamera.h"
#include "../Graphics/Cube Map/CubeMap.h"
#include "GameState.h"
#include "../FSM/MenuState.h"




//////////////////////////////////////////////////////////
// INTRO MOVIE
/////////////////////////////////////////////////////////
	
//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void D3DStateIntroMovie::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void D3DStateIntroMovie::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void D3DStateIntroMovie::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//D3D specific methods
void D3DStateIntroMovie::InitializeState(D3DApp* pD3DApp)
{
	//load movie
	//render video
	HRESULT hr;
	hr = pD3DApp->GetGraph()->RenderFile(L"Video/HAWX 2Opening.wmv",NULL);

	//create video window
	IVideoWindow* tempWindow = pD3DApp->GetVidWindow();

	pD3DApp->GetGraph()->QueryInterface(IID_IVideoWindow, (void**) &tempWindow);

	pD3DApp->SetVidWindow(tempWindow);

	//give that video window the main window handle
	pD3DApp->GetVidWindow()->put_Owner((OAHWND)pD3DApp->GetHWND());

	//set the style of the window
	pD3DApp->GetVidWindow()->put_WindowStyle(WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);

	//get the window's size
	RECT windowSize;
	
	GetClientRect(pD3DApp->GetHWND(),&windowSize);

	//set the video size to the size of the window
	pD3DApp->GetVidWindow()->SetWindowPosition(windowSize.left,windowSize.top,windowSize.right,windowSize.bottom);

	//video is set up - now we can just run it.
	pD3DApp->GetVidControl()->Run();
	m_bVideoPaused = false;
	m_bVideoPlaying = true;

}

void D3DStateIntroMovie::UpdateScene(D3DApp* pD3DApp, float dt)
{
		long evCode, param1, param2;

		if(SUCCEEDED(pD3DApp->GetVidEvent()->GetEvent(&evCode, &param1, &param2, 0)))
		{
			pD3DApp->GetVidEvent()->FreeEventParams(evCode, param1, param2);

			//If video is complete (EC_COMPLETE)
			if(evCode == EC_COMPLETE)
			{
				//act on complete call here. (continue with next game state, ect)
				pD3DApp->GetVidControl()->Stop();
				m_bVideoPlaying = false;

				pD3DApp->GetVidWindow()->put_Visible(OAFALSE);
				pD3DApp->GetVidWindow()->put_Owner((OAHWND)pD3DApp->GetHWND());

				//change states to menu state
				pD3DApp->GetFSM()->ChangeState(GameMenu);

			}
		}

		//space pauses movie
		if (gDInput->keyDown(VK_SPACE))
		{
			if (m_bVideoPaused == false)
			{
   				pD3DApp->GetVidControl()->Pause();
				m_bVideoPaused = true;
			}
			else
			{
				pD3DApp->GetVidControl()->Run();
				m_bVideoPaused = false;
			}
		}

		//escape ends movie
		if (gDInput->keyDown(VK_ESCAPE))
		{
			pD3DApp->GetVidControl()->Stop();
			m_bVideoPlaying = false;

			pD3DApp->GetVidWindow()->put_Visible(OAFALSE);
			pD3DApp->GetVidWindow()->put_Owner((OAHWND)pD3DApp->GetHWND());

			//change states to menu state
			pD3DApp->GetFSM()->ChangeState(GameMenu);
		}
	
	
}

void D3DStateIntroMovie::RenderScene(D3DApp* pD3DApp)
{



}

void D3DStateIntroMovie::OnResetDevice(D3DApp* pD3DApp)
{
	
}

void D3DStateIntroMovie::OnLostDevice(D3DApp* pD3DApp)
{

}

void D3DStateIntroMovie::LeaveState(D3DApp* pD3DApp)
{
	//delete objects
	pD3DApp->CleanUpDirectShow();
}

bool D3DStateIntroMovie::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}




D3DStateIntroMovie* D3DStateIntroMovie::Instance()
{
	static D3DStateIntroMovie instance;

	return &instance;
}


//////////////////////////////////////////////////////////
// CREDITS
/////////////////////////////////////////////////////////
	
//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void D3DStateCredits::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void D3DStateCredits::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void D3DStateCredits::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//utility methods
void D3DStateCredits::InitCredits()
{
	//open file
	inFile.open("Objects/credits.txt");
	
	
	std::string sLine;
	std::vector<LPCSTR> charLines;
	char *line;
	//read line from file
	do
	{

		line = new char[80];
	
		inFile.getline(line,80,'\n');
		
		

		charLines.push_back(line);

		m_pCredits->CreateText(line,JMIDDLE);

		
	}while (!inFile.eof());

	m_pCredits->SetPosition(Vector3(0,D3DAPPI->m_D3Dpp.BackBufferHeight,0));
	m_pCredits->SetSeekTarget(Vector3(0,-1866,0));
}

//D3D specific methods
void D3DStateCredits::InitializeState(D3DApp* pD3DApp)
{
	//SET UP CREDITS SOUNDS
	//initialize sounds
	m_sAmbient = new SoundEffect();
	

	m_sAmbient->initialise();
	
	//load sounds
	m_sAmbient->load("Sound/creditstheme.mp3");
	
	m_pCredits = new D3DMenu();

	//set crawl speed
	m_pCredits->SetMaxSpeed(10);

	//set justify
	m_pCredits->SetJustification(JMIDDLE);

	InitCredits();

	m_sAmbient->play();


}

void D3DStateCredits::UpdateScene(D3DApp* pD3DApp, float dt)
{
	m_pCredits->Update(dt);
	
	
}

void D3DStateCredits::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

    // clear the window to a deep blue
    pD3DApp->GetD3DDevice()->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);

	pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene

    //////////////////////////////////////////////////////////////////////////
	// Draw FPS
	//////////////////////////////////////////////////////////////////////////
	//display stats
#if 1
	GStats->display();
#endif

	
	pD3DApp->GetD3DSpriteObject()->Begin(D3DXSPRITE_ALPHABLEND);
	
	
		
	SMI->RenderSprites();

	
		// End drawing 2D sprites
	pD3DApp->GetD3DSpriteObject()->End();

	m_pCredits->Render();
	

	

    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame

}

void D3DStateCredits::OnResetDevice(D3DApp* pD3DApp)
{
	GStats->onResetDevice();
	pD3DApp->GetD3DSpriteObject()->OnResetDevice();
	m_pCredits->OnResetDeviceT();
}

void D3DStateCredits::OnLostDevice(D3DApp* pD3DApp)
{
	GStats->onLostDevice();
	pD3DApp->GetD3DSpriteObject()->OnLostDevice();
	m_pCredits->OnLostDevice();
}

void D3DStateCredits::LeaveState(D3DApp* pD3DApp)
{
	
}

bool D3DStateCredits::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}




D3DStateCredits* D3DStateCredits::Instance()
{
	static D3DStateCredits instance;

	return &instance;
}

//////////////////////////////////////////////////////////
// DRAW A PYRAMID
/////////////////////////////////////////////////////////
	
//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void D3DStatePyramid::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void D3DStatePyramid::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void D3DStatePyramid::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//utility methods
void D3DStatePyramid::BuildFX()
{
	HRESULT hr;

	//////////////////////////////////
	//		Pyramid
	/////////////////////////////////
	
	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/pyramid.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("PyramidTech");
	m_hWVP  = m_pEffect->GetParameterByName(0, "gWVP");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");

	
	m_pPyramid->SetFX(m_pEffect);
	m_pPyramid->SetTexture(m_hTexture, m_Texture);


	//////////////////////////////////
	//		CubeMap
	/////////////////////////////////


}

void D3DStatePyramid::InitVerts()
{

	m_pPyramid = new D3DPyramid();

	BuildFX();
	OnResetDevice(D3DAPPI);

	InitAllVertexDeclarations();

}


//D3D specific methods
void D3DStatePyramid::InitializeState(D3DApp* pD3DApp)
{
	//set up camera first
	m_pCamera = new D3DCamera();

	//initialize sounds
	m_sAmbient = new SoundSys();

	//set up skybox
	m_pSky = new CubeMap("Graphics/Cube Map/hills.dds", 10000.0f);
	
	m_sAmbient->initialise();
	
	//load sounds
	m_sAmbient->load("Sound/creditstheme.mp3");
	

	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);
	InitVerts();

	//m_sAmbient->play();


}

void D3DStatePyramid::UpdateScene(D3DApp* pD3DApp, float dt)
{
	static float index = 0.0f; /*index+=0.001f;    // an ever-increasing float value*/

	// Check input.
	if( gDInput->keyDown(DIK_W) )
	{
		//rotation.x   -= 10 * dt;
		m_pPyramid->SetRotationX(m_pPyramid->GetOrientationV()->x - 1 * dt);
	}
	if( gDInput->keyDown(DIK_S) )	 
	{
		//rotation.x += 10* dt;
		m_pPyramid->SetRotationX(m_pPyramid->GetOrientationV()->x + 1 * dt);
	}
	if(gDInput->keyDown(DIK_A) )
	{
		//rotation.y -= 10 * dt;
		m_pPyramid->SetRotationY(m_pPyramid->GetOrientationV()->y - 1 * dt);
	}
	if(gDInput->keyDown(DIK_D) )
	{
		//rotation.y += 10 * dt;
		m_pPyramid->SetRotationY(m_pPyramid->GetOrientationV()->y + 1 * dt);
	}

	

	// The camera position/orientation relative to world space can 
	// change every frame based on input, so we need to rebuild the
	// view matrix every frame with the latest changes.
	//BuildViewMatrix();
	
	//m_pPyramid->Update();

	m_pCamera->Update(dt);
}


void D3DStatePyramid::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

	HRESULT hr;

    // clear the window to a deep blue
	hr =   pD3DApp->GetD3DDevice()->Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);

	hr = pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene
#if 1
	// Setup the rendering FX
	hr = m_pEffect->SetTechnique(m_hTech);
	hr = m_pEffect->SetMatrix(m_hWVP, &(m_pPyramid->GetTransform() * m_pCamera->GetViewProj()));
#endif

	//draw objects
#if 1
	m_pSky->Render();
	m_pPyramid->Render();
	

#endif

	 //////////////////////////////////////////////////////////////////////////
	// Draw FPS
	//////////////////////////////////////////////////////////////////////////
	//display stats
#if 1
	GStats->display();
#endif
	
    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame

}

void D3DStatePyramid::OnResetDevice(D3DApp* pD3DApp)
{
	//D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);

	/*D3DAPPI->GetD3DDevice()->SetRenderState(D3DRS_LIGHTING, false);
	D3DAPPI->GetD3DDevice()->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);*/

	m_pPyramid->OnResetDevice();
	m_pEffect->OnResetDevice();
	GStats->onResetDevice();
	m_pSky->OnResetDevice();
	m_pSky->SetCamera(m_pCamera);
	//BuildProjectionMatrix();
	m_pCamera->CalculateProjectionMatrix();
	
}

void D3DStatePyramid::OnLostDevice(D3DApp* pD3DApp)
{
	m_pSky->OnLostDevice();
	GStats->onLostDevice();
	m_pPyramid->OnLostDevice();
	m_pEffect->OnLostDevice();
}

void D3DStatePyramid::LeaveState(D3DApp* pD3DApp)
{
	
}

bool D3DStatePyramid::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}




D3DStatePyramid* D3DStatePyramid::Instance()
{
	static D3DStatePyramid instance;

	return &instance;
}



