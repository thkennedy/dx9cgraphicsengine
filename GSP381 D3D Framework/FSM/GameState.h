#pragma once
#include "D3DStates.h"
#include <vector>
#include <map>

/////////////////////////////
//	Main Game State - Final
////////////////////////////

class D3DShip;
class D3DTorus;

#define MainGame GameState::Instance()

class GameState :
	public State<D3DApp>
{
private:
	//private const for singleton
	GameState(void);

	//sounds - put in map for ability to easily switch. 
	SoundEffect* m_sAmbient;
	std::map<int, SoundEffect*> m_vSoundEffects;

	////////////////////
	// SHADER INFO
	///////////////////
	ID3DXEffect*	m_pEffect;			//effects interface
	D3DXHANDLE      m_hTech;			//technique handle
	D3DXHANDLE      m_hWVP;				//World*View*Projection matrix handle
	D3DXHANDLE		m_hTexture;			//object texture
	D3DXHANDLE		m_hWorldInvTrans;	//worldinvtransform matrix
	
	////////////////////
	//	LIGHTING VARS
	////////////////////
	D3DXHANDLE		m_hLightVecW    ;	
	D3DXHANDLE		m_hDiffuseMtrl  ;
	D3DXHANDLE		m_hDiffuseLight ;
	D3DXHANDLE		m_hAmbientMtrl  ;
	D3DXHANDLE		m_hAmbientLight ;
	D3DXHANDLE		m_hSpecularMtrl ;
	D3DXHANDLE		m_hSpecularLight;
	D3DXHANDLE		m_hSpecularPower;
	D3DXHANDLE		m_hEyePos       ;
	D3DXHANDLE		m_hWorld        ;

	D3DXVECTOR3 mLightVecW;		// light in world coords
	D3DXCOLOR   mAmbientMtrl;	// ambient mat color
	D3DXCOLOR   mAmbientLight;	// ambient light color
	D3DXCOLOR   mDiffuseMtrl;	// diffse mat color
	D3DXCOLOR   mDiffuseLight;	// diffuse light color
	D3DXCOLOR   mSpecularMtrl;	// specular mat color
	D3DXCOLOR   mSpecularLight;	// specular light color
	float       mSpecularPower;	// specular power



	//////////////////
	//	TEXTURES
	/////////////////
	IDirect3DTexture9* m_Texture;

	////////////////////
	// GAME OBJECTS
	///////////////////
	D3DPyramid *m_pPyramid;
	D3DShip *m_pShip;
	std::vector<D3DTorus*> m_vRings;


	/////////////////////////
	//	BACKGROUND OBJECTS
	/////////////////////////
	CubeMap* m_pSky;



	///////////////////////
	//	CAMERAS
	///////////////////////
	D3DCamera *m_pCamera;
	//side camera
	D3DCamera *m_pSideCamera;
	//front camera
	D3DCamera *m_pFrontCamera;


public:
	//instance return to prevent copying or duplication
	static GameState* Instance();
	~GameState(void);

	//global state methods
	virtual void Enter(D3DApp* pD3DApp);
	virtual void Execute(D3DApp* pD3DApp, float dt);
	virtual void Exit(D3DApp* pD3DApp);
	
	//utility methods
	void BuildFX();
	void InitVerts();
	void InitSound();
	void InitGameObjects();
	void EndGame();
	void CleanUp();
	void InitGame();


	//D3D specific methods
	virtual void InitializeState(D3DApp* pD3DApp);
	virtual void UpdateScene(D3DApp* pD3DApp, float dt);
	virtual void RenderScene(D3DApp* pD3DApp);
	virtual void OnResetDevice(D3DApp* pD3DApp);
	virtual void OnLostDevice(D3DApp* pD3DApp);
	virtual void LeaveState(D3DApp* pD3DApp);

	virtual bool OnMessage(D3DApp* pD3DApp, const Mail& message);
};