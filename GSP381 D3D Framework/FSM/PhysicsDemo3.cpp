#include "PhysicsDemo3.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/D3DCamera.h"
#include "../Graphics/D3DBox.h"
#include "../Graphics/D3DApp.h"
#include "../Input/DirectInput.h"
#include "../Graphics/GfxStats.h"
#include "../Graphics/ShapeManager.h"
#include "../Graphics/D3DSphere.h"
#include "../Physics/MovementManager.h"
#include "../Physics/CollisionDetector.h"
#include "../Physics/ForceGenerator.h"
#include "../Physics/AdvCollisions.h"
#include "../Physics/ContactGenerator.h"
#include "../Physics/AdvancedBV.h"




//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void PhysicsDemo3::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void PhysicsDemo3::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void PhysicsDemo3::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}




//D3D specific methods
void PhysicsDemo3::InitializeState(D3DApp* pD3DApp)
{
	//set up camera first
	m_pCamera = new D3DCamera();
	m_pCamera->SetPos(0,50,-150);
	m_pCamera->SetSpeed(50.0f);
	m_pCamera->SetLookAt(Vector3(0,0,0),Vector3(0,1,0));

	//give shape mgr the camera
	SMI->SetCameraView(m_pCamera);
	
	//set up force registry and generators
	m_pForces = new ForceRegistry();
	

	int id, id1, id2;
	
	//create initial objects here
	
	/*MMI->CreateHalfSpace(Vector3 (1,0,0), 100,1);
	MMI->CreateHalfSpace(Vector3 (-1,0,0), 100,1);*/
	//MMI->CreateHalfSpace(Vector3 (0,1,0), -30,1);
	//MMI->CreateHalfSpace(Vector3 (0,-1,0), -100,1);
	//MMI->CreateHalfSpace(Vector3 (0,0,-1), 100,0);
	//MMI->CreateHalfSpace(Vector3 (0,0,1), -100,0);

	
	id = MMI->CreateBoxO(Vector3(0,0,0),Vector3(15,5,15) );
	MMI->SetInverseMass(id, 0);
	
	
	//Blocks();
	//RagDoll2();

	//ReferenceDots();

	/*int x = -500;
	for (auto i =0; i < 32; ++i)
		{
			BVHExample(x);

			x += 80;
		}*/
	
	//BVHExample(10);


	//end testing

	OnResetDevice(D3DAPPI);


}


void PhysicsDemo3::BallBallExample()
{
	int id; 

	id = MMI->CreateBall(Vector3(-0,60,0),5);
	MMI->SetVelocity(id,0,-30,0);
	
	/*id = MMI->CreateBall(Vector3(-50,0,0),5);
	MMI->SetVelocity(id,20,0,0);

	id = MMI->CreateBall(Vector3(50,0,0),5);
	MMI->SetVelocity(id,-20,0,0);
	
	id = MMI->CreateBall(Vector3(50,30,0),5);
	MMI->SetVelocity(id,-20,0,0);*/

	//id = MMI->CreateBall(Vector3(-50,0,0),5);
	//MMI->SetVelocity(id,30,-25,40);
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	//id = MMI->CreateBall(Vector3(50,0,0),5);
	//MMI->SetVelocity(id,-30,15,-50);
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	//id = MMI->CreateBall(Vector3(5,-50,0),5);
	//MMI->SetVelocity(id,10,30.2,-10);
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	//id = MMI->CreateBall(Vector3(30,0,0),5);
	//MMI->SetVelocity(id,-20,0,0);
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	
	
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));
}

void PhysicsDemo3::BoxBoxAABBExample()
{
	int id; 
	//x
	id = MMI->CreateBoxA(Vector3(30,0,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, -50,0,0);

	id = MMI->CreateBoxA(Vector3(-30,0,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, 50,0,0);
	
	// y
	id = MMI->CreateBoxA(Vector3(0,40,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,-50,0);

	id = MMI->CreateBoxA(Vector3(0,-40,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,50,0);

	// z
	id = MMI->CreateBoxA(Vector3(0,0,50), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,0,-50);

	id = MMI->CreateBoxA(Vector3(0,0,-50), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,0,50);
}

void PhysicsDemo3::BallBoxAABBExample()
{
	int	id = MMI->CreateBall(Vector3(-50,0,0),5);
	MMI->SetVelocity(id,30,0,0);

	id = MMI->CreateBoxA(Vector3(50,0,0),Vector3(5,5,5));
	MMI->SetVelocity(id, -30,0,0);
}

void PhysicsDemo3::BoxBoxOBBExample()
{
	int id;

	id = MMI->CreateBoxO(Vector3(30,0,0),Vector3(5,2.5,2.5));
	//MMI->SetVelocity(id, -30,0,0);
	//MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,0.7f),2));
	//MMI->SetMass(id, 50);
	m_pForces->AddGravityForce(id, Vector3(0,-9.8,0));

	id = MMI->CreateBoxO(Vector3(30,10,0),Vector3(2.5,2.5,5));
	//MMI->SetVelocity(id, -30,0,0);
	//MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,0.7f),2));
	//MMI->SetMass(id, 50);
	m_pForces->AddGravityForce(id, Vector3(0,-9.8,0));

	id = MMI->CreateBoxO(Vector3(30,20,0),Vector3(5,2.5,2.5));
	//MMI->SetVelocity(id, -30,0,0);
	//MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,0.7f),2));
	//MMI->SetMass(id, 50);
	m_pForces->AddGravityForce(id, Vector3(0,-9.8,0));

	id = MMI->CreateBoxO(Vector3(30,25,0),Vector3(2.5,2.5,5));
	//MMI->SetVelocity(id, -30,0,0);
	//MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,0.7f),2));
	//MMI->SetMass(id, 50);
	m_pForces->AddGravityForce(id, Vector3(0,-9.8,0));

	//id = MMI->CreateBoxO(Vector3(-30,-12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, 30,0,0);

	//id = MMI->CreateBoxO(Vector3(50,12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,5);
	//MMI->SetVelocity(id, -30,0,-20);


	//id = MMI->CreateBoxO(Vector3(-25,-10,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, -30,0,20);
	//MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,1.0f),2));

	//id = MMI->CreateBoxO(Vector3(-30,-12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, 30,-10,5);

	//id = MMI->CreateBoxO(Vector3(30,12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, -5,0,0);
	//MMI->SetVelocity(id, -30,0,5);
}

void PhysicsDemo3::CollisionRotationExample()
{
	int id; 
	id = MMI->CreateBoxO(Vector3(-30,7,0),Vector3(5,5,5));
	MMI->SetVelocity(id, 50,0,0);
	

	id = MMI->CreateBoxO(Vector3(30,0,0),Vector3(5,5,5));
	MMI->SetVelocity(id, -50,0,0);
}

void PhysicsDemo3::ForcesExample()
{
	int id, id1, id2;

	//force test
	//spring
	id1 = MMI->CreateBall(Vector3(20,0,0),5);
	id2 = MMI->CreateBall(Vector3(-20,0,0),5);
	m_pForces->AddSpringForce(id1,Vector3(0,0,0),id2,Vector3(0,0,0), 1, 70);

	//bungee
	id = MMI->CreateBoxO(Vector3(30,20,0),Vector3(5,5,5));
	m_pForces->AddAnchoredBungeeForce(id, 0,30,0,1,30);
	m_pForces->AddGravityForce(id,Vector3(0,-20,0));


	////buoyancy + gravity
	//id = MMI->CreateBall(Vector3(200,-10,-200),5);
	//m_pForces->AddBuoyancyForce(id, 50, .0001, 0, 700);
	//m_pForces->AddGravityForce(id, Vector3(0,-75,0));
	//id = SMI->CreateBox(10,50,50);
	//SMI->GetBox(id)->SetPosition(220,-25,-200);
	//SMI->SetColor(id, YELLOW);


}

void PhysicsDemo3::BlastForce()
{
	int id;

	id = MMI->CreateBall(Vector3(0,0,-20),5);
	MMI->SetVelocity(id, 0,0,0);
	m_pForces->AddBlastForce(id,7);
}
void PhysicsDemo3::BVHExample(float xposition)
{

	int id[13];
	Vector3 gravity(0,-10,0);
	float error = .15f;

	bool bOutterVisible = 0;

	Vector3 center(xposition,0,200);

	
	/*///////////////
	Advanced BV's
	////////////////*/
	AdvancedBV* bv = ACMI->CreateAdvancedBV(center, 40.0f,0);

	//first level children
	// adding top half
	bv->AddOBB("Circle",Vector3(0,14,0),Vector3(26,14,6),"Top Half", bOutterVisible);
	//bottom half
	bv->AddOBB("Circle",Vector3(0,-12.5,0),Vector3(26,12.5,6),"Bottom Half", bOutterVisible);
		
	
	// second level children
	// adding torso
	bv->AddOBB("Top Half", Vector3(0,14,0),Vector3(8,13,5),"Torso", bOutterVisible);
	// left arm to upper
	bv->AddOBB("Top Half", Vector3(-16,13,0),Vector3(9,3,3),"Left Arm");
	// right arm to upper
	bv->AddOBB("Top Half", Vector3(16,13,0),Vector3(9,3,3),"Right Arm");
	// left arm to lower
	//bv->AddOBB("Bottom Half", Vector3(-16,13,0),Vector3(9,3,3),"Left Arm", bOutterVisible);
	// right arm to lower
	//bv->AddOBB("Bottom Half", Vector3(16,13,0),Vector3(9,3,3),"Right Arm", bOutterVisible);
	// legs
	bv->AddOBB("Bottom Half",Vector3(0,-11.5,0),Vector3(8,11.5,5),"Legs", bOutterVisible);
	
	// third level children
	// chest
	bv->AddOBB("Torso", Vector3(0,17,0),Vector3(7.5,9,5),"Upper Torso", bOutterVisible);
	// stomach
	bv->AddOBB("Torso", Vector3(0,4,0),Vector3(7.5,4,4),"Stomach");
	// groin
	bv->AddOBB("Legs", Vector3(0,-2.5,0),Vector3(1.5,2.5,4),"Groin");
	// left leg
	bv->AddOBB("Legs", Vector3(-4.5,-11.5,0),Vector3(3,11,3),"Left Leg");
	// right leg
	bv->AddOBB("Legs", Vector3(4.5,-11.5,0),Vector3(3,11,3),"Right Leg");

	//fourth level children
	//head
	bv->AddOBB("Upper Torso", Vector3(0,20,0),Vector3(4,4,4),"Head");
	//chest
	bv->AddOBB("Upper Torso", Vector3(0,12,0),Vector3(7.5,4,4),"Chest");

	
}
void PhysicsDemo3::RagDoll()
{
	int id[13];
	Vector3 gravity(0,-10,0);
	float error = .15f;

	Vector3 center(0,15,0);
	
	

	//MMI->CreateHalfSpace(Vector3 (1,0,0), -50,1);
	//MMI->CreateHalfSpace(Vector3 (-1,0,0), -50,1);
	MMI->CreateHalfSpace(Vector3 (0,1,0), -30,1);
	//MMI->CreateHalfSpace(Vector3 (0,-1,0), -50,1);
	//MMI->CreateHalfSpace(Vector3 (0,0,-1), -100,0);
	//MMI->CreateHalfSpace(Vector3 (0,0,1), -30,0);

	//left foot 0
	id[0] = MMI->CreateBoxS(center + Vector3(-5,-25.5,-4.5),Vector3(2.5,2.5,5));
	m_pForces->AddGravityForce(id[0],gravity);
	//left calf 1
	id[1] = MMI->CreateBoxS(center + Vector3(-5,-18,0),Vector3(2.5,5,2.5));
	//m_pForces->AddGravityForce(id[1],gravity);
	
	//left ankle
	ACMI->AddJointCGen(id[0],Vector3(0,2.5,4.5),id[1],Vector3(0,-5,0), error);

	//left thigh 2
	id[2] = MMI->CreateBoxS(center + Vector3(-5,-9,0),Vector3(3,4,3));
	//m_pForces->AddGravityForce(id[2],gravity);
	//left knee
	ACMI->AddJointCGen(id[1],Vector3(0,5,0),id[2],Vector3(0,-4,0), error);

	
	//right foot 3
	id[3] = MMI->CreateBoxS(center + Vector3(5,-25.5,-4.5),Vector3(2.5,2.5,5));
	m_pForces->AddGravityForce(id[3],gravity);
	//right calf 4
	id[4] = MMI->CreateBoxS(center + Vector3(5,-18,0),Vector3(2.5,5,2.5));
	//m_pForces->AddGravityForce(id[4],gravity);
	//right ankle
	ACMI->AddJointCGen(id[3],Vector3(0,2.5,4.5),id[4],Vector3(0,-5,0), error);
	//right thigh 5
	id[5] = MMI->CreateBoxS(center + Vector3(5,-9,0),Vector3(3,4,3));
	//m_pForces->AddGravityForce(id[5],gravity);
	//right knee
	ACMI->AddJointCGen(id[4],Vector3(0,5,0),id[5],Vector3(0,-4,0), error);
	
	

	//pelvis 6
	id[6] = MMI->CreateBoxS(center + Vector3(0,-2.5,0),Vector3(8,2.5,4));
	m_pForces->AddGravityForce(id[6],gravity);
	//left hip
	ACMI->AddJointCGen(id[2],Vector3(0,4,0),id[6],Vector3(-5,-2.5,0), error);
	//right hip
	ACMI->AddJointCGen(id[5],Vector3(0,4,0),id[6],Vector3(5,-2.5,0), error);


	//chest 7
	id[7] = MMI->CreateBoxS(center + Vector3(0,8,0),Vector3(8,8,4));
	m_pForces->AddGravityForce(id[7],gravity);
	//waist
	ACMI->AddJointCGen(id[6],Vector3(0,2.5,0),id[7],Vector3(0,-8.0,0), error);

	//left forearm 8
	id[8] = MMI->CreateBoxS(center + Vector3(-21,13,0),Vector3(5,2.5,2.5));
	m_pForces->AddGravityForce(id[8],gravity);
	//left bicep 9
	id[9] = MMI->CreateBoxS(center + Vector3(-12,13,0),Vector3(4,3,3));
	//m_pForces->AddGravityForce(id[9],gravity);
	//left elbow
	ACMI->AddJointCGen(id[8],Vector3(5,0,0),id[9],Vector3(-4,0,0), error);
	//left shoulder
	ACMI->AddJointCGen(id[9],Vector3(4,0,0),id[7],Vector3(-8,5,0), error);
	
	//right forearm 10
	id[10] = MMI->CreateBoxS(center + Vector3(21,13,0),Vector3(5,2.5,2.5));
	m_pForces->AddGravityForce(id[10],gravity);
	//right bicep 11
	id[11] = MMI->CreateBoxS(center + Vector3(12,13,0),Vector3(4,3,3));
	//m_pForces->AddGravityForce(id[11],gravity);
	//right elbow 
	ACMI->AddJointCGen(id[10],Vector3(-5,0,0),id[11],Vector3(4,0,0), error);
	//right shoulder
	ACMI->AddJointCGen(id[11],Vector3(-4,0,0),id[7],Vector3(8,5,0), error);

	//head 12
	id[12] = MMI->CreateBoxS(center + Vector3(0,20,0),Vector3(4,4,4));
	//m_pForces->AddGravityForce(id[12],gravity);
	//neck
	ACMI->AddJointCGen(id[12],Vector3(0,-4,0),id[7],Vector3(0,8,0), error);

}

void PhysicsDemo3::RagDoll2()
{
	int id[13];
	Vector3 gravity(0,-39.8,0);
	float error = 10;

	Vector3 center(0,40,0);
	
	

	/*MMI->CreateHalfSpace(Vector3 (1,0,0), 100,1);
	MMI->CreateHalfSpace(Vector3 (-1,0,0), 100,1);*/
	MMI->CreateHalfSpace(Vector3 (0,1,0), -30,1);
	/*MMI->CreateHalfSpace(Vector3 (0,-1,0), -100,1);
	MMI->CreateHalfSpace(Vector3 (0,0,-1), -100,0);
	MMI->CreateHalfSpace(Vector3 (0,0,1), -100,0);*/

	//left foot 0
	id[0] = MMI->CreateBoxO(center + Vector3(-5,-25.5,-4.5),Vector3(2.5,2.5,5));
	m_pForces->AddGravityForce(id[0],gravity);
	//left calf 1
	id[1] = MMI->CreateBoxO(center + Vector3(-5,-18,0),Vector3(2.5,5,2.5));
	m_pForces->AddGravityForce(id[1],gravity);
	
	//left ankle
	ACMI->AddCableCGen(id[0],Vector3(0,2.5,4.5),id[1],Vector3(0,-5,0), error);
	

	//left thigh 2
	id[2] = MMI->CreateBoxO(center + Vector3(-5,-9,0),Vector3(3,4,3));
	m_pForces->AddGravityForce(id[2],gravity);
	//left knee
	ACMI->AddCableCGen(id[1],Vector3(0,5,0),id[2],Vector3(0,-4,0), error);

	
	//right foot 3
	id[3] = MMI->CreateBoxO(center + Vector3(5,-25.5,-4.5),Vector3(2.5,2.5,5));
	m_pForces->AddGravityForce(id[3],gravity);
	//right calf 4
	id[4] = MMI->CreateBoxO(center + Vector3(5,-18,0),Vector3(2.5,5,2.5));
	m_pForces->AddGravityForce(id[4],gravity);
	//right ankle
	ACMI->AddCableCGen(id[3],Vector3(0,2.5,4.5),id[4],Vector3(0,-5,0), error);
	//right thigh 5
	id[5] = MMI->CreateBoxO(center + Vector3(5,-9,0),Vector3(3,4,3));
	m_pForces->AddGravityForce(id[5],gravity);
	//right knee
	ACMI->AddCableCGen(id[4],Vector3(0,5,0),id[5],Vector3(0,-4,0), error);
	
	

	//pelvis 6
	id[6] = MMI->CreateBoxO(center + Vector3(0,-2.5,0),Vector3(8,2.5,4));
	m_pForces->AddGravityForce(id[6],gravity);
	//left hip
	ACMI->AddCableCGen(id[2],Vector3(0,4,0),id[6],Vector3(-5,-2.5,0), error);
	//right hip
	ACMI->AddCableCGen(id[5],Vector3(0,4,0),id[6],Vector3(5,-2.5,0), error);


	//chest 7
	id[7] = MMI->CreateBoxO(center + Vector3(0,8,0),Vector3(8,8,4));
	m_pForces->AddGravityForce(id[7],gravity);
	//waist
	ACMI->AddCableCGen(id[6],Vector3(0,2.5,0),id[7],Vector3(0,-8.0,0), error);

	//left forearm 8
	id[8] = MMI->CreateBoxO(center + Vector3(-21,13,0),Vector3(5,2.5,2.5));
	m_pForces->AddGravityForce(id[8],gravity);
	//left bicep 9
	id[9] = MMI->CreateBoxO(center + Vector3(-12,13,0),Vector3(4,3,3));
	m_pForces->AddGravityForce(id[9],gravity);
	//left elbow
	ACMI->AddCableCGen(id[8],Vector3(5,0,0),id[9],Vector3(-4,0,0), error);
	//left shoulder
	ACMI->AddCableCGen(id[9],Vector3(4,0,0),id[7],Vector3(-8,5,0), error);
	
	//right forearm 10
	id[10] = MMI->CreateBoxO(center + Vector3(21,13,0),Vector3(5,2.5,2.5));
	m_pForces->AddGravityForce(id[10],gravity);
	//right bicep 11
	id[11] = MMI->CreateBoxO(center + Vector3(12,13,0),Vector3(4,3,3));
	m_pForces->AddGravityForce(id[11],gravity);
	//right elbow 
	ACMI->AddCableCGen(id[10],Vector3(-5,0,0),id[11],Vector3(4,0,0), error);
	//right shoulder
	ACMI->AddCableCGen(id[11],Vector3(-4,0,0),id[7],Vector3(8,5,0), error);

	//head 12
	id[12] = MMI->CreateBoxO(center + Vector3(0,20,0),Vector3(4,4,4));
	m_pForces->AddGravityForce(id[12],gravity);
	//neck
	ACMI->AddCableCGen(id[12],Vector3(0,-4,0),id[7],Vector3(0,8,0), error);
}

void PhysicsDemo3::Blocks()
{
	int id, id1, id2;

	Vector3 gravity(0,-25,0);
	
	//create initial objects here
	
	/*MMI->CreateHalfSpace(Vector3 (1,0,0), 100,1);
	MMI->CreateHalfSpace(Vector3 (-1,0,0), 100,1);*/
	MMI->CreateHalfSpace(Vector3 (0,1,0), 0,1);
	//MMI->CreateHalfSpace(Vector3 (0,-1,0), -100,1);
	//MMI->CreateHalfSpace(Vector3 (0,0,-1), 100,0);
	//MMI->CreateHalfSpace(Vector3 (0,0,1), -100,0);

	//id = MMI->CreateBoxO(Vector3(0,0,0),Vector3(300,1,300));
	//MMI->SetInverseMass(id,0);

	// box demo
	//bottom row
	id = MMI->CreateBoxO(Vector3(0,4,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);
	
	id = MMI->CreateBoxO(Vector3(14,4,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);
	
	id = MMI->CreateBoxO(Vector3(-14,4,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(28,4,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);
	
	id = MMI->CreateBoxO(Vector3(-28,4,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	// lower row

	id = MMI->CreateBoxO(Vector3(7,12,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(-7,12,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(21,12,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(-21,12,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);
		
	// middle row

	id = MMI->CreateBoxO(Vector3(0,20,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(14,20,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(-14,20,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	// upper row
	id = MMI->CreateBoxO(Vector3(7,28,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);

	id = MMI->CreateBoxO(Vector3(-7,28,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);


	// top block

	id = MMI->CreateBoxO(Vector3(0,36,0),Vector3(7,4,4));
	m_pForces->AddGravityForce(id, gravity);


	//ramp
	id = MMI->CreateBoxO(Vector3(0,50,-100), Vector3(10,1,60));
	MMI->RotateBody(id, Quaternion(Vector3(1,0,0),.8f));
	MMI->SetInverseMass(id, 0);
	
	id = MMI->CreateBall(Vector3(0,70,-125),5);
	MMI->SetMass(id, 100);
	m_pForces->AddGravityForce(id, Vector3(0,-59.8,0));
	
	
}

void PhysicsDemo3::UpdateScene(D3DApp* pD3DApp, float dt)
{
	static float counter = 999.0f;

	// Check input.
	if( gDInput->keyDown(DIK_1) && counter > 0.3f )
	{
		//BallBallExample();
		m_iSelectedAmmo = 1;
		counter = 0.0f;
	}

	if( gDInput->keyDown(DIK_2) && counter > 0.3f)
	{
		//BoxBoxAABBExample();
		m_iSelectedAmmo = 2;
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_3) && counter > 0.3f)
	{
		//BallBoxAABBExample();
		m_iSelectedAmmo = 3;
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_4) && counter > 0.3f)
	{
		//BoxBoxOBBExample();
		m_iSelectedAmmo = 4;
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_5) && counter > 0.3f)
	{
		//CollisionRotationExample();
		m_iSelectedAmmo = 5;
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_8) && counter > 0.3f)
	{
		//BlastForce();
		BoxBoxOBBExample();
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_7) && counter > 0.3f)
	{
		//ForcesExample();
		//m_iSelectedAmmo = 7;
		Blocks();
		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_0) && counter > 0.3f )
	{
		MMI->DeleteAllBodies();
		m_pForces->clear();

		counter = 0.0f;
	}
	if(gDInput->mouseButtonDown(0) && counter > 0.3f )
	{
		FireSelected();
		counter = 0.0f;
	}

	//update forces
	m_pForces->updateForces(dt);
	
	//update physics objects
	MMI->Update(dt);
	//update graphics objects
	SMI->Update(dt);

	// update camera
	m_pCamera->Update(dt);

	//check for collisions, out of bounds, etc
	//CMI->CheckOutOfBounds(125,125,125,Vector3(0,0,0));
	//CMI->CheckCollisions();
	
	ACMI->Update(dt);



	

	//increment dbounce counter
	counter += dt;
}




void PhysicsDemo3::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

	HRESULT hr;

    // clear the window to a deep blue
	hr = pD3DApp->GetD3DDevice()->Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);
	hr = pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene


	//draw objects
	SMI->Render();

#if 1
	 //////////////////////////////////////////////////////////////////////////
	// Draw FPS
	//////////////////////////////////////////////////////////////////////////
	//display stats

	GStats->display();
#endif
	
    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame

}

void PhysicsDemo3::OnResetDevice(D3DApp* pD3DApp)
{
	SMI->OnResetDevice();
	
	GStats->onResetDevice();
	
	m_pCamera->CalculateProjectionMatrix();
	
}

void PhysicsDemo3::OnLostDevice(D3DApp* pD3DApp)
{
	SMI->OnLostDevice();
	
	GStats->onLostDevice();


}

void PhysicsDemo3::LeaveState(D3DApp* pD3DApp)
{
	
}

bool PhysicsDemo3::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}

void PhysicsDemo3::FireSelected()
{
	switch(m_iSelectedAmmo)
	{
	case 1:
		ShootBullet(100);
		break;
	case 2:
		ShootBall(100);
		break;
	case 3:
		ShootBallGravity(100);
		break;
	case 4:
		ShootBox(100);
		break;
	case 5:
		ShootBoxGravity(100);
		break;
	case 6:
		ShootGrenade(100);
		break;
	default:
		ShootBullet(100);
	}
}


PhysicsDemo3* PhysicsDemo3::Instance()
{
	static PhysicsDemo3 instance;

	return &instance;
}


void PhysicsDemo3::ReferenceDots()
{
	int id;

	//reference dots
	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,0,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));
	SMI->SetColor(id,YELLOW);

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,10,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,20,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,30,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,40,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,50,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	//////////////////
		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,-10,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,-20,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,-30,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,-40,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(0,-50,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	//////////////////////////////////////////

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,0,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,10,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,20,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,30,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,40,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,50,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	//////////////////
		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,-10,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,-20,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,-30,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,-40,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(-10,-50,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));


	//////////////////////////////

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,0,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,10,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,20,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,30,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,40,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,50,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	//////////////////
		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,-10,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

	id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,-20,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,-30,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,-40,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));

		id = SMI->CreateSphere(1);
	SMI->GetShape(id)->SetPosition(Vector3(10,-50,0));
	SMI->GetShape(id)->SetScale(Vector3(1,1,10));
}

void PhysicsDemo3::ShootBullet( float speed)
{
	MMI->SetVelocity(MMI->CreateBall(m_pCamera->GetPosition(), 1),
		m_pCamera->GetLookAt().x * speed, 
		m_pCamera->GetLookAt().y * speed, 
		m_pCamera->GetLookAt().z * speed
		);

}
void PhysicsDemo3::ShootBall( float speed)
{
	MMI->SetVelocity(MMI->CreateBall(m_pCamera->GetPosition(), 3),
		m_pCamera->GetLookAt().x * speed, 
		m_pCamera->GetLookAt().y * speed, 
		m_pCamera->GetLookAt().z * speed
		);
}
void PhysicsDemo3::ShootBallGravity(float speed)
{
	MMI->SetVelocity(MMI->CreateBall(m_pCamera->GetPosition(), 1),
		m_pCamera->GetLookAt().x * speed, 
		m_pCamera->GetLookAt().y * speed, 
		m_pCamera->GetLookAt().z * speed
		);

}
void PhysicsDemo3::ShootBox(float speed)
{
	int id;
	id = MMI->CreateBoxO(m_pCamera->GetPosition(), Vector3(4,4,4));
	MMI->SetVelocity(id, 
		m_pCamera->GetLookAt().x * speed, 
		m_pCamera->GetLookAt().y * speed, 
		m_pCamera->GetLookAt().z * speed
		);
	MMI->SetMass(id, 50);

	
}
void PhysicsDemo3::ShootBoxGravity(float speed)
{

}
void PhysicsDemo3::ShootGrenade(float speed)
{

}