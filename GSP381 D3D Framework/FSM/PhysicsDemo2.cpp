#include "PhysicsDemo2.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/D3DCamera.h"
#include "../Graphics/D3DBox.h"
#include "../Graphics/D3DApp.h"
#include "../Input/DirectInput.h"
#include "../Graphics/GfxStats.h"
#include "../Graphics/ShapeManager.h"
#include "../Graphics/D3DSphere.h"
#include "../Physics/MovementManager.h"
#include "../Physics/CollisionDetector.h"
#include "../Physics/ForceGenerator.h"
#include "../Physics/AdvCollisions.h"



//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void PhysicsDemo2::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void PhysicsDemo2::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void PhysicsDemo2::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}




//D3D specific methods
void PhysicsDemo2::InitializeState(D3DApp* pD3DApp)
{
	//set up camera first
	m_pCamera = new D3DCamera();
	m_pCamera->SetPos(0,0,-200);
	m_pCamera->SetSpeed(50.0f);
	m_pCamera->SetLookAt(Vector3(0,0,0),Vector3(0,1,0));

	//give shape mgr the camera
	SMI->SetCameraView(m_pCamera);
	
	//set up force registry and generators
	m_pForces = new ForceRegistry();

	int id, id1, id2;
	
	//create initial objects here
	
	/*MMI->CreateHalfSpace(Vector3 (1,0,0), 100,1);
	MMI->CreateHalfSpace(Vector3 (-1,0,0), 100,1);*/
	MMI->CreateHalfSpace(Vector3 (0,1,0), -100,1);
	//MMI->CreateHalfSpace(Vector3 (0,-1,0), -100,1);
	//MMI->CreateHalfSpace(Vector3 (0,0,-1), 100,0);
	//MMI->CreateHalfSpace(Vector3 (0,0,1), -100,0);

	



	//end testing

	OnResetDevice(D3DAPPI);


}

void PhysicsDemo2::BallBallExample()
{
	int id; 
	
	id = MMI->CreateBall(Vector3(-50,0,0),5);
	MMI->SetVelocity(id,30,-25,40);
	m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	id = MMI->CreateBall(Vector3(50,0,0),5);
	MMI->SetVelocity(id,-30,15,-50);
	m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	id = MMI->CreateBall(Vector3(5,-50,0),5);
	MMI->SetVelocity(id,10,30.2,-10);
	m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	id = MMI->CreateBall(Vector3(30,10,0),5);
	MMI->SetVelocity(id,-20,0,0);
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));

	id = MMI->CreateBall(Vector3(-30,10,0),5);
	MMI->SetVelocity(id,20,0,0);
	//m_pForces->AddGravityForce(id,Vector3(0,-40,0));
}

void PhysicsDemo2::BoxBoxAABBExample()
{
	int id; 
	//x
	id = MMI->CreateBoxA(Vector3(30,0,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, -50,0,0);

	id = MMI->CreateBoxA(Vector3(-30,0,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, 50,0,0);
	
	// y
	id = MMI->CreateBoxA(Vector3(0,40,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,-50,0);

	id = MMI->CreateBoxA(Vector3(0,-40,0), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,50,0);

	// z
	id = MMI->CreateBoxA(Vector3(0,0,50), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,0,-50);

	id = MMI->CreateBoxA(Vector3(0,0,-50), Vector3 (5,5,5));
	MMI->SetVelocity(id, 0,0,50);
}

void PhysicsDemo2::BallBoxAABBExample()
{
	int	id = MMI->CreateBall(Vector3(-50,0,0),5);
	MMI->SetVelocity(id,30,0,0);

	id = MMI->CreateBoxA(Vector3(50,0,0),Vector3(5,5,5));
	MMI->SetVelocity(id, -30,0,0);
}

void PhysicsDemo2::BoxBoxOBBExample()
{
	int id;

	id = MMI->CreateBoxO(Vector3(30,0,0),Vector3(8,5,5));
	//MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, -30,0,0);
	MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,1.0f),2));
	MMI->SetMass(id, 10);
	m_pForces->AddGravityForce(id, Vector3(0,-20,0));

	//id = MMI->CreateBoxO(Vector3(-30,-12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, 30,0,0);

	//id = MMI->CreateBoxO(Vector3(50,12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,5);
	//MMI->SetVelocity(id, -30,0,-20);


	//id = MMI->CreateBoxO(Vector3(-25,-10,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, -30,0,20);
	//MMI->RotateBody(id, Quaternion(Vector3(0.0f,0.0f,1.0f),2));

	//id = MMI->CreateBoxO(Vector3(-30,-12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, 0,0,10);
	//MMI->SetVelocity(id, 30,-10,5);

	//id = MMI->CreateBoxO(Vector3(30,12,0),Vector3(5,5,5));
	////MMI->SetRotation(id, -5,0,0);
	//MMI->SetVelocity(id, -30,0,5);
}

void PhysicsDemo2::CollisionRotationExample()
{
	int id; 
	id = MMI->CreateBoxO(Vector3(-30,7,0),Vector3(5,5,5));
	MMI->SetVelocity(id, 50,0,0);
	

	id = MMI->CreateBoxO(Vector3(30,0,0),Vector3(5,5,5));
	MMI->SetVelocity(id, -50,0,0);
}

void PhysicsDemo2::ForcesExample()
{
	int id, id1, id2;

	//force test
	//spring
	id1 = MMI->CreateBall(Vector3(20,0,0),5);
	id2 = MMI->CreateBall(Vector3(-20,0,0),5);
	m_pForces->AddSpringForce(id1,Vector3(0,0,0),id2,Vector3(0,0,0), 1, 70);

	//bungee
	id = MMI->CreateBoxO(Vector3(30,20,0),Vector3(5,5,5));
	m_pForces->AddAnchoredBungeeForce(id, 0,30,0,1,30);
	m_pForces->AddGravityForce(id,Vector3(0,-20,0));


	////buoyancy + gravity
	//id = MMI->CreateBall(Vector3(200,-10,-200),5);
	//m_pForces->AddBuoyancyForce(id, 50, .0001, 0, 700);
	//m_pForces->AddGravityForce(id, Vector3(0,-75,0));
	//id = SMI->CreateBox(10,50,50);
	//SMI->GetBox(id)->SetPosition(220,-25,-200);
	//SMI->SetColor(id, YELLOW);


}

void PhysicsDemo2::BlastForce()
{
	int id;

	id = MMI->CreateBall(Vector3(-50,0,0),5);
	MMI->SetVelocity(id, 0,0,20);
	m_pForces->AddBlastForce(id,300);
}

void PhysicsDemo2::UpdateScene(D3DApp* pD3DApp, float dt)
{
	static float counter = 999.0f;

	// Check input.
	if( gDInput->keyDown(DIK_1) && counter > 0.3f )
	{
		BallBallExample();

		counter = 0.0f;
	}

	if( gDInput->keyDown(DIK_2) && counter > 0.3f)
	{
		//BoxBoxAABBExample();

		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_3) && counter > 0.3f)
	{
		//BallBoxAABBExample();

		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_4) && counter > 0.3f)
	{
		BoxBoxOBBExample();

		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_5) && counter > 0.3f)
	{
		CollisionRotationExample();

		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_7) && counter > 0.3f)
	{
		ForcesExample();

		counter = 0.0f;
	}
	if( gDInput->keyDown(DIK_8) && counter > 0.3f)
	{
		BlastForce();

		counter = 0.0f;
	}

	if( gDInput->keyDown(DIK_0) && counter > 0.3f )
	{
		MMI->DeleteAllBodies();
		m_pForces->clear();

		counter = 0.0f;
	}

	//update forces
	m_pForces->updateForces(dt);

	//update physics objects
	MMI->Update(dt);
	//update graphics objects
	SMI->Update(dt);

	// update camera
	m_pCamera->Update(dt);

	//check for collisions, out of bounds, etc
	//CMI->CheckOutOfBounds(125,125,125,Vector3(0,0,0));
	//CMI->CheckCollisions();
	ACMI->Update(dt);



	

	//increment dbounce counter
	counter += dt;
}




void PhysicsDemo2::RenderScene(D3DApp* pD3DApp)
{
	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

	HRESULT hr;

    // clear the window to a deep blue
	hr = pD3DApp->GetD3DDevice()->Clear(0, 0, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 10, 100), 1.0f, 0);
	hr = pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene


	//draw objects
	SMI->Render();

#if 1
	 //////////////////////////////////////////////////////////////////////////
	// Draw FPS
	//////////////////////////////////////////////////////////////////////////
	//display stats

	GStats->display();
#endif
	
    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame

}

void PhysicsDemo2::OnResetDevice(D3DApp* pD3DApp)
{
	SMI->OnResetDevice();
	
	GStats->onResetDevice();
	
	m_pCamera->CalculateProjectionMatrix();
	
}

void PhysicsDemo2::OnLostDevice(D3DApp* pD3DApp)
{
	SMI->OnLostDevice();
	
	GStats->onLostDevice();


}

void PhysicsDemo2::LeaveState(D3DApp* pD3DApp)
{
	
}

bool PhysicsDemo2::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}




PhysicsDemo2* PhysicsDemo2::Instance()
{
	static PhysicsDemo2 instance;

	return &instance;
}


