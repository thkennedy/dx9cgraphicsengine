#include "MenuState.h"
#include "D3DStates.h"
#include "../Graphics/D3DApp.h"
#include "../Graphics/D3DSprite.h"
#include "../Graphics/Sprites/D3DSpriteManager.h"
#include "../Objects/MenuButton.h"
#include "../Objects/D3DMenu.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/GfxStats.h"
#include "../Sound/SoundSys.h"
#include "../Sound/SoundEffect.h"
#include "../Input/DirectInput.h"
#include <iostream>
#include <string>
#include "../Graphics/D3DVertex.h"
#include "../Graphics/D3DPyramid.h"
#include "../Graphics/D3DCamera.h"
#include "../Graphics/Cube Map/CubeMap.h"
#include "GameState.h"


////////////////////////////////////////////////////
//D3DWk2Menu
////////////////////////////////////////////////////

MenuState* MenuState::Instance()
{
	static MenuState instance;

	return &instance;
}
	
//global state methods
//calls the D3D specific methods so that FSM stays modular(usable) for game entities
void MenuState::Enter(D3DApp* pD3DApp)
{
	InitializeState(pD3DApp);
}
void MenuState::Execute(D3DApp* pD3DApp, float dt)
{
	UpdateScene(pD3DApp, dt);
	
}
void MenuState::Exit(D3DApp* pD3DApp)
{
	LeaveState(pD3DApp);
}

//state methods
void MenuState::LoadMenuButtons(D3DApp* pD3DApp)
{


	//init menus
	menu1 = new D3DMenu();
	menu2 = new D3DMenu();
	menu3 = new D3DMenu();
	menu4 = new D3DMenu();
	menu5 = new D3DMenu();
	menu6 = new D3DMenu();
	menu7 = new D3DMenu();

	////////////////////////////////////////////////////////////////
	// MENU 1 - top level
	////////////////////////////////////////////////////////////////


	//create 1st button
	menu1->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Campaign", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu1->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Demos", m_iNextButtonID++,Vector3 (400,428,0));

	menu1->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Options", m_iNextButtonID++,Vector3 (400,428,0));

	menu1->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Quit", m_iNextButtonID++,Vector3 (400,428,0));

	m_vMenus.push_back(menu1);
	//menu1->SetPosition(Vector3(2000,0,0));


	//////////////////////////////////////////////////////////////////
	//// MENU 2 - MULTIPLAYER
	//////////////////////////////////////////////////////////////////

	//create 1st button
	menu2->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Shader Demo", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu2->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Future Demo", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu2->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Future Demo", m_iNextButtonID++,Vector3 (400,428,0));


	menu2->SetPosition(Vector3(2000,0,0));

	m_vMenus.push_back(menu2);
	//////////////////////////////////////////////////////////////////
	//// MENU 3 - Options
	//////////////////////////////////////////////////////////////////

	//create 1st button
	menu3->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Volume", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu3->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Graphics", m_iNextButtonID++,Vector3 (400,428,0));

	menu3->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Back", m_iNextButtonID++,Vector3 (400,428,0));

	menu3->SetPosition(Vector3(2000,0,0));

	m_vMenus.push_back(menu3);
	//////////////////////////////////////////////////////////////////
	//// MENU 4 - Quit
	//////////////////////////////////////////////////////////////////
	//create 1st button
	menu4->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Yes", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu4->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "No", m_iNextButtonID++,Vector3 (400,428,0));

	menu4->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Credits", m_iNextButtonID++,Vector3 (400,428,0));
	menu4->SetPosition(Vector3(2000,0,0));

	m_vMenus.push_back(menu4);
	//////////////////////////////////////////////////////////////////
	//// MENU 5 - LAN
	//////////////////////////////////////////////////////////////////
	//create 1st button
	menu5->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Host Game", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu5->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Connect To Game", m_iNextButtonID++,Vector3 (400,428,0));
	
	menu5->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Back", m_iNextButtonID++,Vector3 (400,428,0));
	menu5->SetPosition(Vector3(2000,0,0));

	m_vMenus.push_back(menu5);


	//////////////////////////////////////////////////////////////////
	//// MENU 6 - Volume
	//////////////////////////////////////////////////////////////////
	//create 1st button
	menu6->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Master", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu6->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Sound FX", m_iNextButtonID++,Vector3 (400,428,0));
						
	menu6->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Music", m_iNextButtonID++,Vector3 (400,428,0));

	menu6->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Back", m_iNextButtonID++,Vector3 (400,428,0));

	menu6->SetPosition(Vector3(2000,0,0));

	m_vMenus.push_back(menu6);
	//////////////////////////////////////////////////////////////////
	//// MENU 7 - Graphics
	//////////////////////////////////////////////////////////////////
	//create 1st button
	menu7->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Hardware", m_iNextButtonID++,Vector3 (400,428,0));

	//create 2nd button
	menu7->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Software", m_iNextButtonID++,Vector3 (400,428,0));

	menu7->CreateButton(pD3DApp->GetHWND(),pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject()
						,L"Graphics/Sprites/f22-256x94.png",D3DSprite64, "Back", m_iNextButtonID++,Vector3 (400,428,0));

	menu7->SetPosition(Vector3(2000,0,0));

	m_vMenus.push_back(menu7);

	
}

void MenuState::ClickedButtons(D3DApp* pD3DApp, int buttonClicked)
{
	switch(buttonClicked)
	{
	case -1:
		return;
		break;
	///////////////////////////////
	// MENU 1
	///////////////////////////////
	
	case 0:
		//DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{		
			m_sOnClick->play();
			pD3DApp->GetFSM()->ChangeState(MainGame);
			m_bLeaveState = true;
			return;
			
		}
		break;
	case 1:
		//menu 1 goes to menu 2
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu1->OffscreenLeft();
			menu2->OnscreenRight();
			
			return;
		}
		break;
	case 2:
		//menu 1 goes to menu 3
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu1->OffscreenLeft();
			menu3->OnscreenRight();
			return;
		}
		break;
	case 3:
		//menu 1 goes to menu 4
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu1->OffscreenLeft();
			menu4->OnscreenRight();
			return;
		}
		break;
	///////////////////////////////
	// MENU 2
	///////////////////////////////

	case 4:
		//changes states into the pyramid demo
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			pD3DApp->GetFSM()->ChangeState(MainGame);
			return;
		}
		break;
	case 5:
		//DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			return;
		}
		break;
	case 6:
		//goes from menu 2 to menu 1
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu2->OffscreenLeft();
			menu1->OnscreenRight();
			return;
		}
		break;

	///////////////////////////////
	// MENU 3
	///////////////////////////////
	case 7://from menu 3 to 6
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu3->OffscreenLeft();
			menu6->OnscreenRight();
			return;
		}
		break;
	case 8: //from menu 3 to menu 7
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu3->OffscreenLeft();
			menu7->OnscreenRight();
			return;
		}
		break;
	case 9: //from menu 3 to menu 1
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu3->OffscreenLeft();
			menu1->OnscreenRight();
			return;
		}
		break;

	///////////////////////////////
	// MENU 4
	///////////////////////////////
	case 10://QUITS app
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			D3DAPPI->Quit();
			return;
		}
		break;
	case 11://menu 4 to menu 1
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu4->OffscreenLeft();
			menu1->OnscreenRight();
			return;
		}

	case 12: //CREDITS
		if (gDInput->mouseButtonDown(0))
		{
			pD3DApp->GetFSM()->ChangeState(Credits);
			m_bLeaveState = true;
			return;
		}
		break;
		
	///////////////////////////////
	// MENU 5
	///////////////////////////////

	case 13://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 14://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 15://goes from menu 5 to menu 2
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu5->OffscreenLeft();
			menu2->OnscreenRight();
			return;
		}
		break;

	///////////////////////////////
	// MENU 6
	///////////////////////////////
	case 16://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 17://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 18://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 19: //goes from menu 6 to menu 3
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu6->OffscreenLeft();
			menu3->OnscreenRight();
			
			return;
		}
		break;

	///////////////////////////////
	// MENU 7
	///////////////////////////////
	case 20://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 21://DOES NOTHING
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			
			
			return;
		}
		break;
	case 22:
		if (gDInput->mouseButtonDown(0))
		{
			m_sOnClick->play();
			menu7->OffscreenLeft();
			menu3->OnscreenRight();
			return;
		}
		break;
	
	default:
		return;
	};
}



//D3D specific methods
void MenuState::InitializeState(D3DApp* pD3DApp)
{
	m_bLeaveState = false;

	//set up background
	m_pBackground = new D3DSprite(pD3DApp->GetD3DObject(),pD3DApp->GetD3DDevice(),pD3DApp->GetD3DSpriteObject());
	m_pBackground->SetTexture(L"Graphics/Sprites/f22background1024x1024.png");
	m_pBackground->SetPosition(D3DXVECTOR3(0.0f,0.0f,0.0f));
	m_pBackground->SetCenter(D3DXVECTOR3(0,0,0));
	m_pBackground->SetScale(D3DXVECTOR3(.39,.6,1));
	SMI->RegisterSprite(m_pBackground);
	
	//initialize sounds
	m_sAmbient = new SoundEffect();
	m_sOnClick = new SoundEffect();
	m_sOnMouseOver = new SoundEffect();

	m_sAmbient->initialise();
	m_sOnClick->initialise();
	m_sOnMouseOver->initialise();

	//load sounds
	m_sAmbient->load("Sound/topgunanthem.mp3");
	m_sOnClick->load("Sound/jetflyby.wav");
		
	//set up button ID system
	m_iNextButtonID = 0;
	
	//load the menu buttons
	LoadMenuButtons(pD3DApp);

	//begin ambient
	m_sAmbient->playLooped();

}

void MenuState::UpdateScene(D3DApp* pD3DApp, float dt)
{

	int iButtonOver = -1, iButtonOver2 = -1, iButtonOver3 = -1;
	
	for (auto it = m_vMenus.begin(); it != m_vMenus.end(); it++)
	{
		iButtonOver = (*it)->Update(dt);

		ClickedButtons(pD3DApp,iButtonOver);

		if (m_bLeaveState == true)
			return;
	}

	SMI->UpdateSprites(dt);
	
}

void MenuState::RenderScene(D3DApp* pD3DApp)
{

	

	// If the device was not created successfully, return
	if(!pD3DApp->GetD3DDevice())
		return;

    // clear the window to a deep blue
    pD3DApp->GetD3DDevice()->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);

	pD3DApp->GetD3DDevice()->BeginScene();    // begins the 3D scene

    //////////////////////////////////////////////////////////////////////////
	// Draw FPS
	//////////////////////////////////////////////////////////////////////////
	//display stats


	//////////////////////////////////////////////////////////////////////////
	// Draw 2D sprites
	//////////////////////////////////////////////////////////////////////////
	// Note: You should only be calling the sprite object's begin and end once, 
	// with all draw calls of sprites between them

	// Call Sprite's Begin to start rendering 2D sprite objects
	pD3DApp->GetD3DSpriteObject()->Begin(D3DXSPRITE_ALPHABLEND);
	//	//////////////////////////////////////////////////////////////////////////
	//	// Matrix Transformations to control sprite position, scale, and rotate
	//	// Set these matrices for each object you want to render to the screen
	//	//////////////////////////////////////////////////////////////////////////
	//			

	//	// Rotation on Z axis, value in radians, converting from degrees
	//	// Translation
	//	// Multiply scale and rotation, store in scale
	//	// Multiply scale and translation, store in world

	//	// Set Transform

	//			
	//	// Draw the texture with the sprite object
	
		
	SMI->RenderSprites();

	
		// End drawing 2D sprites
	pD3DApp->GetD3DSpriteObject()->End();

#if 1
	GStats->display();
#endif

	//render menus w/ text
	for (auto it = m_vMenus.begin(); it != m_vMenus.end(); it++)
	{
		(*it)->Render();

	}
	

	

    pD3DApp->GetD3DDevice()->EndScene();    // ends the 3D scene

    pD3DApp->GetD3DDevice()->Present(NULL, NULL, NULL, NULL);    // displays the created frame
	

}

void MenuState::OnResetDevice(D3DApp* pD3DApp)
{
	GStats->onResetDevice();
	pD3DApp->GetD3DSpriteObject()->OnResetDevice();
	for (auto it = m_vMenus.begin(); it != m_vMenus.end(); it++)
	{
		(*it)->OnResetDevice();
	}

	
		float x = .0004875,
				y = .001;
		m_pBackground->SetScale(D3DXVECTOR3(D3DAPPI->m_D3Dpp.BackBufferWidth * x,  D3DAPPI->m_D3Dpp.BackBufferHeight * y, 1));
	
}

void MenuState::OnLostDevice(D3DApp* pD3DApp)
{
	GStats->onLostDevice();
	pD3DApp->GetD3DSpriteObject()->OnLostDevice();
	
	for (auto it = m_vMenus.begin(); it != m_vMenus.end(); it++)
	{
		(*it)->OnLostDevice();
	}
	

	
}

void MenuState::LeaveState(D3DApp* pD3DApp)
{
	//delete objects
	GStats->onLostDevice();
	pD3DApp->GetD3DSpriteObject()->OnLostDevice();
	
	for (auto it = m_vMenus.begin(); it != m_vMenus.end(); it++)
	{
		(*it)->OnLostDevice();
	}

	for (int i = 0; i < m_vMenus.size(); i++)
	{
		delete m_vMenus[i];
	}

	m_vMenus.clear();

	SMI->ClearRegistry();

	delete m_sAmbient;
	delete m_sOnClick;

}

bool MenuState::OnMessage(D3DApp* pD3DApp, const Mail& message)
{
	return 1;
}
