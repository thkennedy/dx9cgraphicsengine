#include "Carrier.h"
#include "CarrierStates.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "MyGameWorld.h"
#include <time.h>
#include "WorldManager.h"
#include "Physics\Matrix4.h"
#include "AMMS.h"
#include "Graphics\gspOut.h"


Carrier::Carrier(vector3 position, vector3 velocity)
	:MovingEntity(CARRIER,position,velocity)
{
	//create the graphics object, and get the next ID from it.
	m_entityID = gMyGameWorld->CreateTriangle(WMI->GetNextValidID());

	//setting default heading
	m_qOrientation = Quaternion();

	//carrier str range is 10-20
	m_rStrength = RandInRange(10,20);

	//carrier radius is large, as the carrier is slow and has to react early
	m_rVisionRadius = 30.00f;

	//this thing is huge
	SetMass(50);

	//collision radius
	m_rCollisionRadius = 6.00f;

	//carrier speed is slooooow because its so big and powerful
	m_rMaxSpeed = 15.0f;

	//carrier max force
	m_rMaxForce = 20.00f;

	//set up the state machine
    m_pStateMachine = new FiniteStateMachine<Carrier>(this);

	////set current state
	m_pStateMachine->SetCurrentState(CEscape);
	m_pStateMachine->ChangeState(CPath);

	////for logic and output purposes, storing state name
	//m_eStateName = WANDER;

	//setting the neighborhood size
	m_rNeighborhoodRadius = 30.00f;

	
	
	

	//team entity is on

	//register entity
	WMI->RegisterEntity(this);

	//graphics stuff
	Triangle *graphicsTriangle = gMyGameWorld->GetTriangle(m_entityID);

	//give the carrier the triangle's ptr
	m_gObject = graphicsTriangle;

	//create a color var
	D3DCOLOR tempColor = D3DCOLOR_XRGB(0,255,0);
	
	//set the triangle's color
	graphicsTriangle->SetColor(tempColor);

	//set carriers size
	graphicsTriangle->SetScale(2,2,2);

	//set update message
	Dispatch->DispatchMsg(1,this->GetID(),this->GetID(),MSG_UPDATE,NULL);
}


Carrier::~Carrier()
{
	//deleting graphics object first
	gMyGameWorld->RemoveTriangle(GetID());
	
	//delete all messages referring to pEntity
	Dispatch->EraseAllMessagesByID(GetID());
	
	//removing from all lists
	WMI->RemoveFromEntLists(this);

	//change state
	GetFSM()->CurrentState()->Exit(this);

	//remove all AI forces
	AMI->ClearAllEntForces(this);

	wout<< "Carrier: " << this->GetID() << " was destroyed!" << endl;
}


void Carrier::Update(real time_elapsed)
{
#if 0
	m_vRotation = vector3(0,0,0);
	

#endif
	
	m_vVelocity.z = 0;

	//get internal data
	CalculateDerivedData();
	
	//clamp velocity to max speed
	m_vVelocity.normalize();
	m_vVelocity.scale(m_rMaxSpeed);

	if (m_rInverseMass <= 0.0f) return;
		assert(time_elapsed > 0.0f);

	//linear updates
	m_vPosition += m_vVelocity.scaleReturn(time_elapsed);

	vector3 currentV = m_vVelocity;
	
	vector3 resultingAcc = m_vAcceleration;

	m_vForceAccum.normalize();

	m_vForceAccum.scale(m_rMaxForce);

	resultingAcc += m_vForceAccum;
	
	m_vVelocity += resultingAcc.scaleReturn(time_elapsed);

	m_vVelocity *= pow(DAMPING, time_elapsed);

	//diff of velocities as a rotation
	//if ((m_vVelocity - currentV).magnitudeSquared() >= .01)
	m_vRotation = (m_vVelocity - currentV).scaleReturn(m_rMaxForce);


	//HEADING UPDATE
	//this is the rotation axis of your vector (rotating around z axis)
	vector3 orientation = vector3(0,0,1);
	
	//find the rotation angle from the right axis
	vector3	a = vector3(1,0,0),
			b = m_vVelocity.normalizeRet();
	real theta = acos(a * b); // dot product

	//The previous step will always return a positive number, so we check to see if the angle is negative.
	//  If so, we flip it.
	if (a.y * b.x > a.x * b.y)
		theta *= -1;
	
	Quaternion qOrientation(orientation, theta);

	m_qOrientation = qOrientation;

	//END HEADI

	ClearAccumulator();


	//update pos and heading of graphics object
	m_gObject->SetHeading(D3DXQUATERNION(m_qOrientation.x, m_qOrientation.y, m_qOrientation.z, m_qOrientation.w));
	//m_gObject->SetHeading(orientation.x,orientation.y,orientation.z);

	//gMyGameWorld->SetTriangleHeading(this->GetID(),0.0f, 0.0f, theta);
	//m_gObject->SetHeading(m_vVelocity.x,m_vVelocity.z,m_vVelocity.y);
	//wout << "Heading X: " << m_gObject->Heading().x << ", Y: "<<m_gObject->Heading().y<<", Z: "<<m_gObject->Heading().z << endl;
	m_gObject->SetPosition(m_vPosition.x,m_vPosition.y,m_vPosition.z);

#if 1 // lock to x/y movement

	m_vPosition.z = 0;
	m_vVelocity.z = 0;



#endif

}


void Carrier::ChangeState(State<Carrier>* pNewState)
{
	//make sure both states are valid before attempting to
	//call their methods
	assert (m_pCurrentState && pNewState);

	//call the exit method of the existing state
	m_pCurrentState->Exit(this);

	//change state to the new state
	m_pCurrentState = pNewState;

	//call the entry method of the new state
	m_pCurrentState->Enter(this);

}

bool Carrier::HandleMessage(const Mail& msg)
{
	return m_pStateMachine->HandleMessage(msg);


}

StateName Carrier::GetStateName()
{
	return m_eStateName;
}

void Carrier::SetStateName(StateName newStateName)
{
	m_eStateName = newStateName;
}


void Carrier::SetGraphicsObject(Triangle* graphicObj)
{
	m_gObject = graphicObj;
}

bool Carrier::StateLogic()
{
	//logic for changing states
	//carriers value preservation over domination. if there is someone stronger than them, they 
	//will always run away. However, if there are none stronger than them, they will seek the weakest and 
	//destroy them first

	BaseGameEntity* highestStr;
	BaseGameEntity* lowestStr;

	//if there's any targets in range, loop through them
	if (m_vTargetsInRange.size() > 0)
	{
		//checking loop
		std::vector<BaseGameEntity*>::iterator it;

		it = m_vTargetsInRange.begin();

		highestStr = (*it);
		lowestStr = (*it);

		while(it != m_vTargetsInRange.end())
		{
			if ((*it)->GetStrength() >= highestStr->GetStrength())
				highestStr = (*it);
			
			if ((*it)->GetStrength() <= lowestStr->GetStrength())
				lowestStr = (*it);

			it++;
		}
	

		////preservation first - if the highest strength target is greater than the weakest, and 
		////that target is also stronger or strong as me, time to run away
		//if (highestStr->GetStrength() >= lowestStr->GetStrength() &&
		//	highestStr->GetStrength() >= m_rStrength)
		//{
		//	//send a message to myself to change state to FLEE passing what to run away from if I'm not already there
		//	if(m_eStateName != FLEE)
		//	{
		//		this->SetCurrentTarget(highestStr);
		//		//AMI->AddFleeForce(this,highestStr);
		//		//Dispatch->DispatchMsg(0,m_entityID,m_entityID,MSG_FLEE,highestStr);
		//		return 1;
		//		
		//	}
		//	
		//}
	
		//// if I'm stronger than everything else, seek the weakest target
		//else if (highestStr->GetStrength() < m_rStrength && lowestStr->GetStrength() < m_rStrength)
		//{
		//	//send a message to myself to change state to SEEK, passing the target to seek if I'm not already there
		//	if(m_eStateName != SEEK)
		//	{
		//		this->SetCurrentTarget(lowestStr);
		//		//AMI->AddSeekForce(this,lowestStr);
		//		//Dispatch->DispatchMsg(0,m_entityID,m_entityID,MSG_SEEK,lowestStr);
		//		return 1;
		//		
		//	}
		//}

	}
	else 
	{
		////no targets in range, change to wander if that's not our current state
		//if (m_eStateName != WANDER)
		//{
		//	//send a message to myself to change state to wander
		//	//AMI->AddWanderForce(this,50,100,50);
		//	//Dispatch->DispatchMsg(0,m_entityID,m_entityID,MSG_WANDER,NULL);
		//	return 1;
		//}
	}

	//no change in situation - maintain same course
	return 0;
}

