#include "D3DShip.h"
#include "D3DApp.h"
#include "D3DVertex.h"
#include "D3DCamera.h"


D3DShip::D3DShip(int id)
	:D3DShape(id)
{
	/////////////////////////
	// INTERNAL VARS
	/////////////////////////

	m_vOrientation = D3DXVECTOR3(0,0,0);

	m_vPosition = m_vCenter = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(1.0f,1.0f,1.0f);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	LPD3DXBUFFER bufShipMaterial;

	D3DXLoadMeshFromX(L"Graphics/Meshes/SpaceshipDirectX/Spaceship.x",    // load this file
                      D3DXMESH_SYSTEMMEM,    // load the mesh into system memory
					  D3DAPPI->GetD3DDevice(),    // the Direct3D Device
                      NULL,    // we aren't using adjacency
                      &bufShipMaterial,    // put the materials here
                      NULL,    // we aren't using effect instances
                      &m_wNumMaterials,    // the number of materials in this model
                      &m_pMeshShip);    // put the mesh here

	// retrieve the pointer to the buffer containing the material information
    D3DXMATERIAL* tempMaterials = (D3DXMATERIAL*)bufShipMaterial->GetBufferPointer();


	// create a new material buffer for each material in the mesh
    m_pMaterial = new D3DMATERIAL9[m_wNumMaterials];
	

    for(DWORD i = 0; i < m_wNumMaterials; i++)    // for each material...
    {
        m_pMaterial[i] = tempMaterials[i].MatD3D;    // get the material info
        m_pMaterial[i].Ambient = m_pMaterial[i].Diffuse;    // make ambient the same as diffuse

    }


	//setting up light
	mLightVecW     = D3DXVECTOR3(-350.0f, 350.0f, 300.0f);
	mDiffuseMtrl   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mDiffuseLight  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mAmbientMtrl   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mAmbientLight  = D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.0f);
	mSpecularMtrl  = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	mSpecularLight = D3DXCOLOR(255.0f, 1.0f, 1.0f, 1.0f);
	mSpecularPower = 999.0001f;

	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Meshes/SpaceShipDirectX/SpaceShipUV.jpg", &m_Texture);


	BuildFX();
	
	


	OnResetDevice();


	

	


}


D3DShip::~D3DShip(void)
{
}

void D3DShip::CalculateInternals()
{

	//////////////
	//	ROTATION
	//////////////

	// a matrix to store the rotation information
	D3DXMATRIX matRotateY, matRotateX, matRotateZ;  

	//clamp rotation values if they are out of range
	if (m_vOrientation.x > MAXROTATION_X)
	{
		m_vOrientation.x = MAXROTATION_X;
	}
	if (m_vOrientation.x < -MAXROTATION_X)
	{
		m_vOrientation.x = -MAXROTATION_X;
	}

	//clamp rotation values if they are out of range
	if (m_vOrientation.y > MAXROTATION_Y)
	{
		m_vOrientation.y = MAXROTATION_Y;
	}
	if (m_vOrientation.y < -MAXROTATION_Y)
	{
		m_vOrientation.y = -MAXROTATION_Y;
	}
	 

	
	// build a matrix to rotate the model based on the orientation values
    D3DXMatrixRotationX(&matRotateX, m_vOrientation.x);
	D3DXMatrixRotationY(&matRotateY, m_vOrientation.y);
	D3DXMatrixRotationZ(&matRotateZ, m_vOrientation.z);
	

	D3DXMATRIX rotationMatrix = matRotateX * matRotateY * matRotateZ;

	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = rotationMatrix * matScale *  matTranslate;


}

void D3DShip::BuildFX()
{
	HRESULT hr;

	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/texturemapwolight.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("ShipTech");
	m_hWVP  = m_pEffect->GetParameterByName(0, "gWVP");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");
	m_hWorldInvTrans = m_pEffect->GetParameterByName(0, "gWorldInvTrans");
	m_hLightVecW     = m_pEffect->GetParameterByName(0, "gLightVecW");
	m_hDiffuseMtrl   = m_pEffect->GetParameterByName(0, "gDiffuseMtrl");
	m_hDiffuseLight  = m_pEffect->GetParameterByName(0, "gDiffuseLight");
	m_hAmbientMtrl   = m_pEffect->GetParameterByName(0, "gAmbientMtrl");
	m_hAmbientLight  = m_pEffect->GetParameterByName(0, "gAmbientLight");
	m_hSpecularMtrl  = m_pEffect->GetParameterByName(0, "gSpecularMtrl");
	m_hSpecularLight = m_pEffect->GetParameterByName(0, "gSpecularLight");
	m_hSpecularPower = m_pEffect->GetParameterByName(0, "gSpecularPower");
	m_hEyePos        = m_pEffect->GetParameterByName(0, "gEyePosW");
	m_hWorld         = m_pEffect->GetParameterByName(0, "gWorld");
		
	m_pEffect->SetTexture(m_hTexture,m_Texture);

	


}

void D3DShip::Update(float dt)
{
	////Normalize velocity
	//D3DXVec3Normalize(&m_vVelocity,&m_vVelocity);

	////scale the direction to speed
	//D3DXVec3Scale(&m_vVelocity,&m_vVelocity,m_fSpeed);
	//
	////movement update
	//m_vPosition += m_vVelocity * dt;

	////let my camera know where I am
	//D3DXVECTOR3 lookat = m_vPosition - m_vOrientation;
	//lookat * 2;
	//
	//m_pCamView->SetLookAt(lookat,D3DXVECTOR3(0,1,0));

	


	CalculateInternals();
}
void D3DShip::Render()
{
	HRESULT hr;

#if 1
	// Setup the rendering FX
	hr = m_pEffect->SetTechnique(m_hTech);
	hr = m_pEffect->SetMatrix(m_hWVP, &(m_mTransform * m_pCamView->GetViewProj()));

	D3DXMATRIX worldInvTrans;
	D3DXMatrixInverse(&worldInvTrans, 0, &m_mTransform);
	D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
	m_pEffect->SetMatrix(m_hWorldInvTrans, &worldInvTrans);
	m_pEffect->SetValue(m_hLightVecW, &mLightVecW, sizeof(D3DXVECTOR3));
	m_pEffect->SetValue(m_hDiffuseMtrl, &mDiffuseMtrl, sizeof(D3DXCOLOR));
	m_pEffect->SetValue(m_hDiffuseLight, &mDiffuseLight, sizeof(D3DXCOLOR));
	m_pEffect->SetValue(m_hAmbientMtrl, &mAmbientMtrl, sizeof(D3DXCOLOR));
	m_pEffect->SetValue(m_hAmbientLight, &mAmbientLight, sizeof(D3DXCOLOR));
	m_pEffect->SetValue(m_hSpecularLight, &mSpecularLight, sizeof(D3DXCOLOR));
	m_pEffect->SetValue(m_hSpecularMtrl, &mSpecularMtrl, sizeof(D3DXCOLOR));
	m_pEffect->SetFloat(m_hSpecularPower, mSpecularPower);
	m_pEffect->SetMatrix(m_hWorld, &m_mTransform);
#endif

	// Begin passes.
	UINT numPasses = 0;
	m_pEffect->Begin(&numPasses, 0);
	for(UINT i = 0; i < numPasses; ++i)
	{
		m_pEffect->BeginPass(i);
		// draw the spaceship
		for(DWORD i = 0; i < m_wNumMaterials; i++)    // loop through each subset
		{
			D3DAPPI->GetD3DDevice()->SetMaterial(&m_pMaterial[i]);    // set the material for the subset
			
			hr = m_pMeshShip->DrawSubset(i);    // draw the subset
		}
		m_pEffect->EndPass();
	}
	m_pEffect->End();
	
	
	
}

void D3DShip::OnResetDevice()
{
	m_pEffect->OnResetDevice();
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Meshes/SpaceShipDirectX/SpaceShipUV.jpg", &m_Texture);
}
void D3DShip::OnLostDevice()
{
	m_pEffect->OnLostDevice();
}

