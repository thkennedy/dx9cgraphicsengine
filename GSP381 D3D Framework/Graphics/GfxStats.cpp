#include "d3dApp.h"
#include "GfxStats.h"
#include <tchar.h>

GfxStats* GfxStats::Instance()
{
	static GfxStats instance;

	return &instance;
}

GfxStats::GfxStats()
: mFont(0), mFPS(0.0f), mMilliSecPerFrame(0.0f), mNumAABB(0), mNumSphere(0), mNumOBB(0)
{
	D3DXFONT_DESC fontDesc;
	fontDesc.Height          = 18;
    fontDesc.Width           = 8;
    fontDesc.Weight          = 1;
    fontDesc.MipLevels       = 1;
    fontDesc.Italic          = false;
    fontDesc.CharSet         = DEFAULT_CHARSET;
    fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
    fontDesc.Quality         = DEFAULT_QUALITY;
    fontDesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
    _tcscpy(fontDesc.FaceName, _T("Times New Roman"));

	D3DXCreateFontIndirect(D3DAPPI->GetD3DDevice(), &fontDesc, &mFont);
}

GfxStats::~GfxStats()
{
	ReleaseCOM(mFont);
}

void GfxStats::onLostDevice()
{
	mFont->OnLostDevice();
}

void GfxStats::onResetDevice()
{
	mFont->OnResetDevice();
}

void GfxStats::addSphere(DWORD n)
{
	mNumSphere += n;
}

void GfxStats::subVertices(DWORD n)
{
	mNumSphere -= n;
}

void GfxStats::addAABB(DWORD n)
{
	mNumAABB += n;
}

void GfxStats::addOBB(DWORD n)
{
	mNumOBB += n;
}



void GfxStats::update(float dt)
{
	// Make static so that their values persist accross function calls.
	static float numFrames   = 0.0f;
	static float timeElapsed = 0.0f;

	// Increment the frame count.
	numFrames += 1.0f;

	// Accumulate how much time has passed.
	timeElapsed += dt;

	// Has one second passed?--we compute the frame statistics once 
	// per second.  Note that the time between frames can vary so 
	// these stats are averages over a second.
	if( timeElapsed >= 1.0f )
	{
		// Frames Per Second = numFrames / timeElapsed,
		// but timeElapsed approx. equals 1.0, so 
		// frames per second = numFrames.

		mFPS = numFrames;

		// Average time, in miliseconds, it took to render a single frame.
		mMilliSecPerFrame = 1000.0f / mFPS;

		// Reset time counter and frame count to prepare for computing
		// the average stats over the next second.
		timeElapsed = 0.0f;
		numFrames   = 0.0f;
	}
}

void GfxStats::display()
{
	// Make static so memory is not allocated every frame.
	static char buffer[256];

	sprintf_s(buffer, "Frames Per Second = %.2f\n"
		"Milliseconds Per Frame = %.4f\n"
		"Sphere Count = %d\n"
		"AABB Count = %d\n"
		"OBB Count = %d", mFPS, mMilliSecPerFrame, mNumSphere, mNumAABB, mNumOBB);

	RECT R = {5, 5, 0, 0};
	mFont->DrawTextA(0, buffer, -1, &R, DT_NOCLIP, D3DCOLOR_XRGB(255,255,255));
}