#pragma once
#include <vector>
#include <map>
#include "D3DShape.h"
#include "../Physics/vector3.h"




enum EShapeColor
{
	RED,
	GREEN,
	BLUE,
	GRAY,
	YELLOW

};


/*
	Singleton Shape Manager - Allows for centralized creation, access, update, 
	rendering, onlost, onreset, and deleting of graphics objects
*/

#define SMI ShapeManager::Instance()

typedef std::vector<D3DShape*> ShapeVec;


//forward declarations

class D3DSphere;
class D3DBox;
class D3DLine;
class D3DCamera;


class ShapeManager
{
private:
	ShapeManager(void);

	// map of shapes for random access

	
	//vector of shapes for iterating through and updating
	ShapeVec m_vAllShapes;

	int m_iShapeID;

	D3DCamera *m_pCurrentCamView;

	// sphere used for all draw calls
	LPD3DXMESH	m_pSphere;
	// box used for all draw calls
	LPD3DXMESH	m_pBox;
	// line used for all draw calls
	LPD3DXLINE m_pLine;

	//////////////////////////
	// TEXTURES
	//////////////////////////
	IDirect3DTexture9* m_RedTexture;
	IDirect3DTexture9* m_GreenTexture;
	IDirect3DTexture9* m_BlueTexture;
	IDirect3DTexture9* m_GrayTexture;
	IDirect3DTexture9* m_YellowTexture;



public:
	~ShapeManager(void);

	static ShapeManager* Instance();


	///////////////////////////
	// CREATE SHAPES
	//////////////////////////
	// create a ship mesh with a scale of 1 and the position entered.
	int CreateShip(float xPos = 0.0f, float yPos = 0.0f, float zPos = 0.0f);
	int CreateSphere(float radius = 1.0f);
	int CreateBox(float x = 1.0f,float y = 1.0f, float z = 1.0f);
	int CreateLine(D3DXVECTOR3 vStartPos, D3DXVECTOR3 vEndPos);

	///////////////////////
	// ACCESS SHAPES
	//////////////////////
	D3DShape*	GetShape(int id);
	D3DSphere*	GetSphere(int id);
	D3DBox*		GetBox(int id);
	D3DLine*	GetLine(int id);

	////////////////////
	// DELETE SHAPES
	///////////////////
	void DeleteShape(int id);

	/////////////////
	// CAMERA (VIEWS)
	/////////////////
	void SetCameraView(D3DCamera *camView);

	////////////////
	// COLORS
	///////////////
	void SetColor(int id, EShapeColor newColor);
	void SetLineColor(int id, D3DCOLOR newColor);
	void SetRandomColor(int id);
	
	////////////////////
	// CHANGES
	///////////////////
	void SetLine(int id, Vector3 vStartPos, Vector3 vEndPos);
	void SetPosition(int id, Vector3 vPos);


	void Update(float dt);
	void Render();
	
	void OnResetDevice();
	void OnLostDevice();
};

