#pragma once
#include "d3dUtility.h"

enum CameraType 
{
	FREECAM, FPSCAM, THIRRDPERSON, CHASECAM
};

enum CameraMode
{
	FREE,
	LOCKED
};


class D3DCamera
{
private:
	
	

	D3DXVECTOR3 m_vPosition;	// CAM'S POSITION
	D3DXVECTOR3 m_vOrigin;		// Where Cam rotates around
	float		m_fSpeed;		// Cam's speed
	CameraMode	m_eMode;		// camera mode - free or locked onto a target
	D3DXVECTOR3 m_vLockTarget;	// camera's target while in locked mode
	CameraType	m_eCamType;		// type of camera - chase, free, etc

		
	/////////////////
	// LOCAL NORMALS
	////////////////

	D3DXVECTOR3 m_vLookAt;
	D3DXVECTOR3 m_vRight;
	D3DXVECTOR3 m_vUp;


	D3DXQUATERNION m_qOrientation;

	/////////////
	//	MATRICES
	/////////////
	D3DXMATRIX m_mViewProj;		//view and projection matrix already multiplied
	D3DXMATRIX m_mViewMatrix;
	D3DXMATRIX m_mProjMatrix;


	///////////
	//	VIEWS
	//////////
	
public:
	
	
	D3DCamera();
	D3DCamera(D3DXVECTOR3 pos, D3DXVECTOR3 look, D3DXVECTOR3 up);
	D3DCamera(float xPos, float yPos, float zPos, float xLook, float yLook, float zLook, 
		float xUp = 0.0f, float yUp = 1.0f, float zUp = 0.0f);
	~D3DCamera();

	void Update(float dt);
	void CalculateViewMatrix();
	void CalculateProjectionMatrix();
	void OnLostDevice();	
	void OnResetDevice();
	void ResetView();
	

	///////////////////
	// ACCESSORS
	//////////////////
	const D3DXMATRIX& GetView() const;
	const D3DXMATRIX& GetProj() const;
	const D3DXMATRIX& GetViewProj() const;
	const D3DXVECTOR3& GetRight() const;
	const D3DXVECTOR3& D3DCamera::GetUp() const;
	const D3DXVECTOR3& GetLookAt() const;
	D3DXVECTOR3& GetPosition();

	
	/////////////////
	//	MUTATORS
	////////////////
	void SetLookAt(D3DXVECTOR3& pos, D3DXVECTOR3& target, D3DXVECTOR3& up);
	void SetLookAt(D3DXVECTOR3& target, D3DXVECTOR3& up);
	void SetSpeed(float s);
	void LockCamera(D3DXVECTOR3& target);
	void SetPos(D3DXVECTOR3& pos);
	void SetPos(float x, float y, float z);
	void SetCamType(CameraType type){m_eCamType = type;}
	void SetTarget(D3DXVECTOR3 target){m_vLockTarget = target;}
	void SetCamMode(CameraMode mode){m_eMode = mode;}



};

