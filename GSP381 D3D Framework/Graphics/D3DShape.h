#pragma once
#include "d3dUtility.h"



class D3DCamera;

class D3DShape
{
protected:
	
	//The object's position
	D3DXVECTOR3 m_vPosition;

	//the object's center - rotates around
	D3DXVECTOR3 m_vCenter;

	// the object's orientation
	D3DXVECTOR3 m_vOrientation;
	D3DXQUATERNION m_qOrientation;

	//object's scale
	D3DXVECTOR3 m_vScale;

	// transformation matrix
	D3DXMATRIX m_mTransform;

	// rotation matrix (for phys calcs)

	// current camera view
	D3DCamera *m_pCamView;

	///////////////////////////////
	// VERTEX AND INDEX BUFFERS
	///////////////////////////////
	IDirect3DVertexBuffer9* m_VertexBuffer;
	IDirect3DIndexBuffer9*	m_IndexBuffer;

	

	//calculate internals- World->View->Proj space, orientation, etc
	virtual void CalculateInternals() = 0;

	
	//the shape's texture(s)
	// shape's active texture
	IDirect3DTexture9* m_ActiveTexture;
	// shape's inactive texture
	IDirect3DTexture9* m_InactiveTexture;
	// shape's rendered texture
	IDirect3DTexture9* m_Texture;

	// shape's color
	D3DCOLOR m_cColor;

	//shape's ID
	int m_iID;
	

public:
	////////////////////
	// SHADER INFO
	///////////////////
	ID3DXEffect*			m_pEffect;	//	effects interface
	D3DXHANDLE				m_hTexture; //	texture handle
	D3DXHANDLE m_hTech;
	D3DXHANDLE m_hWVP ;
	D3DXHANDLE m_hWorldInvTrans;
	D3DXHANDLE m_hLightVecW    ;
	D3DXHANDLE m_hDiffuseMtrl  ;
	D3DXHANDLE m_hDiffuseLight ;
	D3DXHANDLE m_hAmbientMtrl  ;
	D3DXHANDLE m_hAmbientLight ;
	D3DXHANDLE m_hSpecularMtrl ;
	D3DXHANDLE m_hSpecularLight;
	D3DXHANDLE m_hSpecularPower;
	D3DXHANDLE m_hEyePos       ;
	D3DXHANDLE m_hWorld        ;
	

	D3DShape();
	D3DShape(int id);
	virtual ~D3DShape();


	/////////////////
	//	UTILITIES
	////////////////
	virtual void Update(float dt) = 0;
	virtual void Render() = 0;
	virtual void OnResetDevice() = 0;
	virtual void OnLostDevice() = 0;



	/////////////////
	//	ACCESSORS
	////////////////
	D3DXVECTOR3*		GetPosition(){return &m_vPosition;}
	D3DXVECTOR3*		GetOrientationV(){return &m_vOrientation;}
	D3DXQUATERNION*		GetOrientationQ(){return &m_qOrientation;}
	D3DXMATRIX			GetTransform(){return m_mTransform;}
	int					GetID(){return m_iID;}
	



	/////////////////
	//	MUTATORS
	////////////////
	void	SetPosition(D3DXVECTOR3 pos);
	void	SetPosition(float x, float y, float z);
	void	SetPositionX(float x);
	void	SetPositionY(float y);
	void	SetPositionZ(float z);

	
	void	SetRotation(D3DXVECTOR3 pos);
	void	SetRotation(float x, float y, float z);
	void	SetRotationX(float x);
	void	SetRotationY(float y);
	void	SetRotationZ(float z);
	void	SetHeading(float yaw, float pitch, float roll);
	void	SetHeading(D3DXQUATERNION qHeading);

	void	SetFX(ID3DXEffect* fx){m_pEffect = fx;}
	void	SetTexture (D3DXHANDLE hTex, IDirect3DTexture9* texture);
	void	SetTexture (IDirect3DTexture9* texture);
	void	SetActiveTexture (IDirect3DTexture9* texture);
	void	SetInActiveTexture (IDirect3DTexture9* texture);

	void	SetCamera(D3DCamera* cam){m_pCamView = cam;}

	void	SetColor(D3DCOLOR cNewColor) {m_cColor = cNewColor;}

	void	SetScale(D3DXVECTOR3 vNewScale) {m_vScale = vNewScale;}
	

};

