#include "D3DPyramid.h"
#include "D3DApp.h"
#include "D3DVertex.h"


D3DPyramid::D3DPyramid(void)
{
	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	m_vPosition = m_vCenter = m_vOrientation = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(0.75f,1.5f,1.0f);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	///////////////////////////////////////
	//		VERTEX BUFFER
	//////////////////////////////////////
	HRESULT hr;
	// Obtain a pointer to a new vertex buffer.
	hr = D3DAPPI->GetD3DDevice()->CreateVertexBuffer(	16 * sizeof(PCTN), 
														D3DUSAGE_WRITEONLY,
														0, 
														D3DPOOL_MANAGED, 
														&m_VertexBuffer, 
														0);
	
	PCTN* v = 0;
	m_VertexBuffer->Lock(0, 0, (void**)&v, 0);
	
	D3DXVECTOR3 normal1, normal2, normal3, normal4, normal5;

	//side 1
	//CALC side 1 NORMAL
	D3DXVec3Cross(&normal1,&D3DXVECTOR3(-1.0f,-1.0f,-1.0f),&D3DXVECTOR3(0.0f,1.0f,0.0f));
	D3DXVec3Normalize(&normal1, &normal1);

	v[0] = PCTN(	-1.0f,	-1.0f,	-1.0f,	0.0f,1.0f, normal1);
	v[1] = PCTN(	0.0f,	1.0f,	0.0f,	0.5f,0.0f, normal1);
	v[2] = PCTN(	1.0f,	-1.0f,	-1.0f,	1.0f,1.0f, normal1);
	
	//side 2
	//CALC side 2 NORMAL
	D3DXVec3Cross(&normal2,&D3DXVECTOR3(1.0f,-1.0f,-1.0f),&D3DXVECTOR3(0.0f,1.0f,0.0f));
	D3DXVec3Normalize(&normal2, &normal2);
	v[3] = PCTN(	1.0f,	-1.0f,	-1.0f,	0.0f,1.0f, normal2);
	v[4] = PCTN(	0.0f,	1.0f,	0.0f,	0.5f,0.0f, normal2);
	v[5] = PCTN(	1.0f,	-1.0f,	1.0f,	1.0f,1.0f, normal2);

	////side 3
	//CALC side 3 NORMAL
	D3DXVec3Cross(&normal3,&D3DXVECTOR3(1.0f,1.0f,1.0f),&D3DXVECTOR3(0.0f,1.0f,0.0f));
	D3DXVec3Normalize(&normal3, &normal3);
	v[6] = PCTN(	1.0f,	-1.0f,	1.0f,	0.0f,1.0f, normal3);
	v[7] = PCTN(	0.0f,	1.0f,	0.0f,	0.5f,0.0f, normal3);
	v[8] = PCTN(	-1.0f,	-1.0f,	1.0f,	1.0f,1.0f, normal3);

	//side 4
	//CALC side 4 NORMAL
	D3DXVec3Cross(&normal4,&D3DXVECTOR3(-1.0f,-1.0f,1.0f),&D3DXVECTOR3(0.0f,1.0f,0.0f));
	D3DXVec3Normalize(&normal4, &normal4);
	v[9] = PCTN(	-1.0f,	-1.0f,	1.0f,	0.0f,1.0f, normal4);
	v[10] = PCTN(	0.0f,	1.0f,	0.0f,	0.5f,0.0f, normal4);
	v[11] = PCTN(	-1.0f,	-1.0f,	-1.0f,	1.0f,1.0f, normal4);


	////side 5
	////CALC side 5 NORMAL
	/*D3DXVec3Cross(&normal5,&D3DXVECTOR3(-1.0f,-1.0f,-1.0f),&D3DXVECTOR3(-1.0f,-1.0f,1.0f));
	D3DXVec3Normalize(&normal5, &normal5);*/
	normal5 = D3DXVECTOR3(0.0f,-1.0f,0.0f);
	v[12] = PCTN(	-1.0f,	-1.0f,	-1.0f,	0.0f,1.0f, normal5);
	v[13] = PCTN(	-1.0f,	-1.0f,	1.0f,	0.0f,0.0f, normal5);
	v[14] = PCTN(	1.0f,	-1.0f,	1.0f,	1.0f,0.0f, normal5);
	v[15] = PCTN(	1.0f,	-1.0f,	-1.0f,	1.0f,1.0f, normal5);

	m_VertexBuffer->Unlock();


	///////////////////////////
	// INDEX BUFFER
	//////////////////////////

		
	// Obtain a pointer to a new index buffer.
	hr = D3DAPPI->GetD3DDevice()->CreateIndexBuffer(18 * sizeof(WORD), D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_IndexBuffer, 0);

	// Now lock it to obtain a pointer to its internal data, and write the data

	WORD* k = 0;

	m_IndexBuffer->Lock(0, 0, (void**)&k, 0);

	//front
	k[0] = 0; k[1] = 1; k[2] = 2;
	
	//right
	k[3] = 3; k[4] = 4; k[5] = 5;

	//back
	k[6] = 6; k[7] = 7; k[8] = 8;

	//left
	k[9] = 9; k[10] = 10; k[11] = 11;

	//bottom
	k[12] = 14; k[13] = 13; k[14] = 12;
	k[15] = 15; k[16] = 14; k[17] = 12;


	m_IndexBuffer->Unlock();

	OnResetDevice();


}


D3DPyramid::~D3DPyramid(void)
{
}

void D3DPyramid::CalculateInternals()
{

	//////////////
	//	ROTATION
	//////////////

	// a matrix to store the rotation information
	D3DXMATRIX matRotateY, matRotateX, matRotateZ;    
	
	// build a matrix to rotate the model based on the increasing float value
    D3DXMatrixRotationX(&matRotateX, m_vOrientation.x);
	D3DXMatrixRotationY(&matRotateY, m_vOrientation.y);
	D3DXMatrixRotationZ(&matRotateZ, m_vOrientation.z);
	

	D3DXMATRIX rotationMatrix = matRotateX * matRotateY * matRotateZ;

	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = rotationMatrix * matTranslate * matScale;


}

void D3DPyramid::Update(float dt)
{
	CalculateInternals();
}
void D3DPyramid::Render()
{
	//set the transform matrix
	 // tell Direct3D about our matrix
	//D3DAPPI->GetD3DDevice()->SetTransform(D3DTS_WORLD, &m_mTransform);

	HRESULT hr;

	hr = D3DAPPI->GetD3DDevice()->SetStreamSource(0,m_VertexBuffer,0,sizeof(PCTN));
	hr = D3DAPPI->GetD3DDevice()->SetIndices(m_IndexBuffer);
	hr = D3DAPPI->GetD3DDevice()->SetVertexDeclaration(PCTN::Decl);

	
	//D3DAPPI->GetD3DDevice()->SetTexture(0,m_Texture);

	// Begin passes.
	UINT numPasses = 0;
	m_pEffect->Begin(&numPasses, 0);
	for(UINT i = 0; i < numPasses; ++i)
	{
		m_pEffect->BeginPass(i);
		D3DAPPI->GetD3DDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 16, 0, 6);
		m_pEffect->EndPass();
	}
	m_pEffect->End();
	
	
	
}

void D3DPyramid::OnResetDevice()
{
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);
}
void D3DPyramid::OnLostDevice()
{

}

