#pragma once
#include "../Graphics/d3dUtility.h"


void InitAllVertexDeclarations();
void DestroyAllVertexDeclarations();


//////////////////////
// Vertex Buffer Element Structs
//////////////////////
struct ELEMENT_Position
{
	ELEMENT_Position():pos(0.0f, 0.0f, 0.0f){}
	ELEMENT_Position(float x, float y, float z):pos(x,y,z){}
	ELEMENT_Position(const D3DXVECTOR3& v):pos(v){}

	D3DXVECTOR3 pos;
	
};

struct ELEMENT_Color
{
	ELEMENT_Color():color(D3DCOLOR_ARGB(255,255,255,255)){}
	ELEMENT_Color(int r, int g, int b):color(D3DCOLOR_ARGB(255,r,g,b)){}
	ELEMENT_Color(const DWORD &col):color(col){}

	DWORD color;
	
};

struct ELEMENT_Texture
{
	ELEMENT_Texture():texture(0.0F,0.0F){}
	ELEMENT_Texture(float u, float v): texture(u,v){}
	ELEMENT_Texture(const D3DXVECTOR2 &uv):texture(uv){}

	D3DXVECTOR2 texture;
	
};

struct ELEMENT_Normal
{
	ELEMENT_Normal():normal(0.0f,0.0f,1.0f){}
	ELEMENT_Normal(float x, float y, float z):normal(x,y,x){}
	ELEMENT_Normal(const D3DXVECTOR3 &v):normal(v){}

	D3DXVECTOR3 normal;
	
};

///////////////////
//	Vertex Type Declarations
//////////////////

/////////
// P
//	P - Position
////////

struct P
{
	P():p(){}
	P(float posx, float posy, float posz): p(posx,posy,posz){}
	P(D3DXVECTOR3 pos):	p(pos){}
	P(const P &newp):p(newp.p){}


	ELEMENT_Position	p;	//vertex position

	static IDirect3DVertexDeclaration9* Decl;

};

/////////
// PCTN 
//	P - Position
//	C - Color
////////

struct PC
{
	PC():p(),c(){}
	PC(float posx, float posy, float posz, int r, int g, int b): 
		p(posx,posy,posz),c(r,g,b){}
	PC(D3DXVECTOR3 pos, DWORD col):
		p(pos), c(col){}

	PC(const PC &newPC):p(newPC.p),c(newPC.c){}


	ELEMENT_Position	p;	//vertex position
	ELEMENT_Color		c;	//vertex color

	static IDirect3DVertexDeclaration9* Decl;

};



/////////
// PCTN 
//	P - Position
//	C - Color
//	N - Normal
//	T - Texture
////////

struct PCTN
{
	PCTN():p(),c(),t(),n(){}
	
	PCTN(float posx, float posy, float posz, int r, int g, int b, float u, float v, D3DXVECTOR3 normal): 
		p(posx,posy,posz),c(r,g,b),t(u,v), n(normal){}
	
	PCTN(float posx, float posy, float posz, float u, float v, D3DXVECTOR3 normal): 
		p(posx,posy,posz),c(),t(u,v), n(normal){}

	PCTN(D3DXVECTOR3 pos, DWORD col, D3DXVECTOR2 texture, D3DXVECTOR3 norm):
		p(pos), c(col), t(texture), n(norm){}
	
	PCTN(float posx, float posy, float posz, float normx, float normy, float normz, float u, float v):
		p(posx,posy,posz),c(),t(u,v),n(normx,normy,normz){}

	ELEMENT_Position	p;	//vertex position
	ELEMENT_Color		c;	//vertex color
	ELEMENT_Texture		t;	//vertex texture
	ELEMENT_Normal		n;	//vertex normal

	static IDirect3DVertexDeclaration9* Decl;

};

struct VertexPos
{
	VertexPos():pos(0.0f, 0.0f, 0.0f){}
	VertexPos(float x, float y, float z):pos(x,y,z){}
	VertexPos(const D3DXVECTOR3& v):pos(v){}

	D3DXVECTOR3 pos;
	static IDirect3DVertexDeclaration9* Decl;
};









