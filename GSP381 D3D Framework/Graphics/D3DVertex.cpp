
#include "D3DVertex.h"
#include "D3DApp.h"

// Initialize static variables.
IDirect3DVertexDeclaration9* PCTN::Decl = 0;
IDirect3DVertexDeclaration9* P::Decl = 0;
IDirect3DVertexDeclaration9* PC::Decl = 0;
IDirect3DVertexDeclaration9* VertexPos::Decl = 0;



void InitAllVertexDeclarations()
{
	HRESULT hr;

	//===============================================================
	// P
	
	D3DVERTEXELEMENT9 PElements[] = 
	{
		{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		D3DDECL_END()
	};	
	hr = D3DAPPI->GetD3DDevice()->CreateVertexDeclaration(PElements, &P::Decl);


	//===============================================================
	// PC
	
	float i = sizeof(D3DXVECTOR3);
	D3DVERTEXELEMENT9 PCElements[] = 
	{
		{0,	0,	D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_POSITION,	0},
		{0,	sizeof(D3DXVECTOR3),	D3DDECLTYPE_D3DCOLOR,	D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_COLOR,	0},
		D3DDECL_END()
	};	
	hr = D3DAPPI->GetD3DDevice()->CreateVertexDeclaration(PCElements, &PC::Decl);


	//===============================================================
	// VertexPos

	D3DVERTEXELEMENT9 VertexPosElements[] = 
	{
		{0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
		D3DDECL_END()
	};	
	D3DAPPI->GetD3DDevice()->CreateVertexDeclaration(VertexPosElements, &VertexPos::Decl);

	//===============================================================
	// PCTN
	
	D3DVERTEXELEMENT9 PCNTElements[] = 
	{
		{0,	0,																D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_POSITION,	0},
		{0,	sizeof(D3DXVECTOR3),											D3DDECLTYPE_D3DCOLOR,	D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_COLOR,		0},
		{0, sizeof(D3DXVECTOR3) + sizeof(D3DCOLOR),							D3DDECLTYPE_FLOAT2,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_TEXCOORD,	0},
		{0, sizeof(D3DXVECTOR3) + sizeof(D3DCOLOR) + sizeof(D3DXVECTOR2),	D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_NORMAL,	0},
		D3DDECL_END()
	};	
	hr = D3DAPPI->GetD3DDevice()->CreateVertexDeclaration(PCNTElements, &PCTN::Decl);
	
}



void DestroyAllVertexDeclarations()
{
	ReleaseCOM(PCTN::Decl);
}
