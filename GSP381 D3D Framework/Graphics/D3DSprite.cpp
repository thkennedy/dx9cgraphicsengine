#include "D3DSprite.h"
#include "d3dUtility.h"


D3DSprite::D3DSprite(LPDIRECT3D9 d3dObj, LPDIRECT3DDEVICE9 d3dDevice, ID3DXSprite* spriteObj)
{
	m_pD3DObject = d3dObj;
	m_pD3DDevice = d3dDevice;
	m_pD3DSprite = spriteObj;

	//default center is top left corner
	m_vCenter = D3DXVECTOR3(0,0,0);
	
	//getting focus window specs
	m_pD3DDevice->GetCreationParameters(&m_d3dcpWindowParams);
	GetWindowRect(m_d3dcpWindowParams.hFocusWindow, &m_rDisplay);

	//default position is the center of the screen
	m_vPosition = D3DXVECTOR3((m_rDisplay.right-m_rDisplay.left)/2,(m_rDisplay.bottom-m_rDisplay.top)/2,0);
	m_bOnScreen = true;

	//sprite given invalid texture so it'll throw error if trying to render
	m_pTexture = NULL;

	//sprite given invalid ID to show it has not been registered
	m_iSpriteID = -1;

	//default dimensions is 128x128
	m_vSpriteDimensions = Vector3(128,128,0);

	m_bMouseOver = false;

	//set default scale,translate,etc
	m_vTranslate = D3DXVECTOR3(0,0,0);
	m_vScale = D3DXVECTOR3(1,1,1);
	m_vRotation = D3DXVECTOR3(0,0,0);

}


D3DSprite::~D3DSprite(void)
{

}


//////////////////////////////////////////////////////////////////////////
// Utilities
//////////////////////////////////////////////////////////////////////////
void D3DSprite::Update(float time_elapsed)
{
	//Update rotation based on angular velocity here later


  

   
}

void D3DSprite::Render()
{
	// Position and size the background sprite--remember that 
	// we always draw the ship in the center of the client area 
	// rectangle. To give the illusion that the ship is moving,
	// we translate the background in the opposite direction.
	D3DXMATRIX T, S;
	D3DXMatrixScaling(&S, m_vScale.x, m_vScale.y, 1.0f);
	m_pD3DSprite->SetTransform(&(S));


	//m_pD3DSprite->SetTransform(&m_mTransform);
	
	if (!m_bMouseOver)
		m_pD3DSprite->Draw(m_pTexture,NULL,&m_vCenter,&m_vPosition,D3DWhite);
	else
		m_pD3DSprite->Draw(m_pTexture,NULL,&m_vCenter,&m_vPosition,D3DBlack);

	m_pD3DSprite->Flush();

}

void D3DSprite::ReleaseSprite()
{
	if (m_pTexture)
	{
		m_pTexture->Release();
	}

	
}

void D3DSprite::OnLostDevice()
{
	if (m_pTexture)
	{
		m_pTexture->Release();
	}
}
void D3DSprite::OnReset()
{
	
}
	

//////////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////////

	


//////////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////////
void D3DSprite::SetTexture(LPCTSTR NewTexture)
{
	HRESULT hr;
	hr = D3DXCreateTextureFromFile(m_pD3DDevice,NewTexture,&m_pTexture);

	//get files info
	D3DXGetImageInfoFromFile(NewTexture, &m_imageInfo);

	//set the file particulars
	m_vCenter = D3DXVECTOR3(m_imageInfo.Width/2, m_imageInfo.Height/2, 0);
	m_vSpriteDimensions = D3DXVECTOR3(m_imageInfo.Width, m_imageInfo.Height,0);

}