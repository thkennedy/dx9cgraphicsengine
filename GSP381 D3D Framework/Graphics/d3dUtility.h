#include <d3d9.h>
#include <d3dx9.h>
#pragma warning(disable : 4995)
#pragma warning(disable : 4244)

#define D3DWhite	D3DCOLOR_ARGB(255,	255,	255,	255)
#define D3DBlack	D3DCOLOR_ARGB(255,	0,		0,		0)
#define D3DRed		D3DCOLOR_ARGB(255,	255,	0,		0)
#define D3DGreen	D3DCOLOR_ARGB(255,	0,		255,	0)
#define D3DBlue		D3DCOLOR_ARGB(255,	0,		0,		255)
#define D3DYellow	D3DCOLOR_ARGB(255,	255,	255,	0)
#define D3DCyan		D3DCOLOR_ARGB(255,	0,		255,	255)
#define D3DMagenta	D3DCOLOR_ARGB(255,	255,	0,		255)

#define D3DSprite64	D3DXVECTOR3(64.0,64.0,0.0)

// Macro to release COM objects fast and safely
#define SAFE_RELEASE(x) if(x){x->Release(); x = 0;}



