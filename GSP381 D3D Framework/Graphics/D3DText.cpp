#include "d3dUtility.h"
#include "D3DText.h"



D3DText::D3DText(HWND hWnd, LPDIRECT3D9 pD3DObject, LPDIRECT3DDEVICE9 pD3DDevice)
{

	//passing d3d pointers in
	m_pD3DObject = pD3DObject;
	m_pD3DDevice = pD3DDevice;

	// Create a D3DX font object
	D3DXCreateFont( m_pD3DDevice, 20, 0, FW_BOLD, 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Arial"), &m_pD3DFont );

	//default text color is white
	m_FontColor = D3DWhite;

	//getting focus window specs
	m_pD3DDevice->GetCreationParameters(&m_d3dcpWindowParams);
	GetWindowRect(m_d3dcpWindowParams.hFocusWindow, &m_rDisplay);

	//default position is the center of the screen
	m_vPosition = Vector3((m_rDisplay.right-m_rDisplay.left)/2,(m_rDisplay.bottom-m_rDisplay.top)/2,0);

	//display rect
	m_pDisplayRect = new RECT();
	
	m_pDisplayRect->left = m_vPosition.x - 60;
	m_pDisplayRect->right = m_vPosition.x + 60;
	m_pDisplayRect->top = m_vPosition.y - 20;
	m_pDisplayRect->bottom = m_vPosition.y + 20;

	
	
}


D3DText::~D3DText(void)
{
	SAFE_RELEASE(m_pD3DFont);
}

void D3DText::Update(float time_elapsed)
{
			
	
}
void D3DText::SetPosition(Vector3 newPos)
{
	m_vPosition = newPos;
	SyncDisplayWithPosition();
}

void D3DText::SyncDisplayWithPosition()
{
	m_pDisplayRect->left = m_vPosition.x - 60;
	m_pDisplayRect->right = m_vPosition.x + 60;
	m_pDisplayRect->top = m_vPosition.y - 20;
	m_pDisplayRect->bottom = m_vPosition.y + 20;
}

void D3DText::Render()
{
	
	//getting display rect
	m_pD3DFont->DrawTextA(NULL, m_sText,-1,m_pDisplayRect,DT_CALCRECT,m_FontColor);

	//actual draw call
	m_pD3DFont->DrawTextA(NULL, m_sText,-1,m_pDisplayRect,DT_CENTER,m_FontColor);
}

void D3DText::OnLostDevice()
{
	m_pD3DFont->OnLostDevice();
}

void D3DText::OnResetDevice()
{
	m_pD3DFont->OnResetDevice();
}

