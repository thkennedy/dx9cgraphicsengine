#include "ShapeManager.h"
#include "D3DBox.h"
#include "D3DSphere.h"
#include "D3DLine.h"
#include "D3DShip.h"
#include "D3DCamera.h"
#include "D3DApp.h"
#include <random>


ShapeManager::ShapeManager(void)
{
	m_iShapeID = 0;

	D3DXCreateSphere(D3DAPPI->GetD3DDevice(), 1.0f, 40, 20, &m_pSphere, NULL);
	D3DXCreateBox(D3DAPPI->GetD3DDevice(), 1.0f, 1.0f, 1.0f, &m_pBox, NULL);
	D3DXCreateLine(D3DAPPI->GetD3DDevice(),&m_pLine);

	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Red.png", &m_RedTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Green.png", &m_GreenTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Blue.png", &m_BlueTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Gray.png", &m_GrayTexture);
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/Colors/Yellow.png", &m_YellowTexture);

	srand(time(NULL));

}


ShapeManager::~ShapeManager(void)
{
	
}

ShapeManager* ShapeManager::Instance()
{
	static ShapeManager instance;

	return &instance;
}

///////////////////////////
// CREATE SHAPES
//////////////////////////

int ShapeManager::CreateShip(float xPos, float yPos, float zPos)
{
	// create ship object, id
	D3DShip *tempShip = new D3DShip(m_iShapeID);

	// give it the current cam view
	tempShip->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempShip);

	return m_iShapeID++;


}
int ShapeManager::CreateSphere(float radius)
{
	//create sphere object, id
	D3DSphere *tempSphere = new D3DSphere(m_pSphere, radius, m_BlueTexture, m_iShapeID);

	//give it the current cam view
	tempSphere->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempSphere);

	//return the id so it can be stored, then increment
	return m_iShapeID++;
}

int ShapeManager::CreateBox(float x,float y, float z)
{
	//create sphere object, id
	D3DBox *tempBox = new D3DBox(m_pBox, x,y,z, m_RedTexture, m_iShapeID);

	//give it the current cam view
	tempBox->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempBox);

	//return the id so it can be stored, then increment
	return m_iShapeID++;
}




int ShapeManager::CreateLine(D3DXVECTOR3 vStartPos, D3DXVECTOR3 vEndPos)
{
	// create line object, id - DEFAULT COLOR IS WHITE
	D3DLine *tempLine = new D3DLine(m_pLine, vStartPos, vEndPos, D3DCOLOR_XRGB(255,255,255), m_iShapeID);

	// give it the current cam view
	tempLine->SetCamera(m_pCurrentCamView);

	//add it to the shape vec
	m_vAllShapes.push_back(tempLine);

	// return the id so it can be store, then increment
	return m_iShapeID++;
}




///////////////////////
// ACCESS SHAPES
//////////////////////
D3DShape*	ShapeManager::GetShape(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}
D3DSphere*	ShapeManager::GetSphere(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (D3DSphere*)(*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}
D3DBox*		ShapeManager::GetBox(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (D3DBox*)(*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}

D3DLine*	ShapeManager::GetLine(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		if ((*it)->GetID() == id)
		{
			return (D3DLine*)(*it);
		}
	}

	//item wasn't found, return NULL ptr
	return NULL; 
}


////////////////////
// DELETE SHAPES
///////////////////
void ShapeManager::DeleteShape(int id)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
		{
			if ((*it)->GetID() == id)
			{
				delete (*it);
				m_vAllShapes.erase(it);
				return;
			}
		}

	//item wasn't found, do nothing
	return;
}

/////////////////
// CAMERA (VIEWS)
/////////////////
void ShapeManager::SetCameraView(D3DCamera *camView)
{
	m_pCurrentCamView = camView;
}


////////////////
// COLORS
///////////////
void ShapeManager::SetColor(int id, EShapeColor newColor)
{

	switch(newColor)
	{
	case RED:
		GetShape(id)->SetTexture(m_RedTexture);
		break;
	case GREEN:
		GetShape(id)->SetTexture(m_GreenTexture);
		break;
	case BLUE:
		GetShape(id)->SetTexture(m_BlueTexture);
		break;
	case GRAY:
		GetShape(id)->SetTexture(m_GrayTexture);
		break;
	case YELLOW:
		GetShape(id)->SetTexture(m_YellowTexture);
		break;
	}

	

}

void ShapeManager::SetLineColor(int id, D3DCOLOR newColor)
{
	GetShape(id)->SetColor(newColor);
}

void ShapeManager::SetRandomColor(int id)
{
	int num = (rand() % 4) + 1;

	switch(num)
	{
	case 1:
		GetShape(id)->SetTexture(m_YellowTexture);
		break;
	case 2:
		GetShape(id)->SetTexture(m_GreenTexture);
		break;
	case 3:
		GetShape(id)->SetTexture(m_BlueTexture);
		break;
	case 4:
		GetShape(id)->SetTexture(m_GrayTexture);
		break;
	}
}

////////////////////
// CHANGES
///////////////////
void ShapeManager::SetLine(int id, Vector3 vStartPos, Vector3 vEndPos)
{
	D3DLine* temp = GetLine(id);
	temp->SetPosition(vStartPos);
	temp->SetEndPosition(vEndPos);
	
}

void ShapeManager::SetPosition(int id, Vector3 vPos)
{
	GetShape(id)->SetPosition(vPos);
}


void ShapeManager::Update(float dt)
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->Update(dt);
	}
}
void ShapeManager::Render()
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->Render();
	}
}
	
void ShapeManager::OnResetDevice()
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->OnResetDevice();
	}
}
void ShapeManager::OnLostDevice()
{
	for (auto it = m_vAllShapes.begin(); it != m_vAllShapes.end(); it++)
	{
		(*it)->OnLostDevice();
	}
}