
#include "D3DApp.h"
#include "../FSM/MenuState.h"
#include "../Timer.h"
#include "Sprites/D3DSpriteManager.h"
#include "D3DText.h"
#include "GfxStats.h"
#include "../Input/DirectInput.h"
#include "../FSM/PhysicsDemo1.h"
#include "../FSM/PhysicsDemo2.h"
#include "../FSM/PhysicsDemo3.h"
#include "../FSM/Graphics2Demo.h"


D3DApp::D3DApp()
{
	m_pStateMachine = new FiniteStateMachine<D3DApp>(this);
	
}

D3DApp* D3DApp::Instance()
{
	static D3DApp Instance;

	return &Instance;
}

D3DApp::~D3DApp(void)
{

}
void InitMainWindow()
{

}

// this function initializes and prepares Direct3D for use

void D3DApp::InitD3D(HWND hWnd, HINSTANCE& hInst, bool bWindowed)
{
	m_hWnd = hWnd;
	m_hInst = hInst;
	m_bQuit = false;
    
	m_pD3DObject = Direct3DCreate9(D3D_SDK_VERSION);    // create the Direct3D interface
	

	// Find the width and height of window using hWnd and GetWindowRect()
	RECT rect;
	GetWindowRect(hWnd, &rect);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

    //D3DPRESENT_PARAMETERS D3Dpp;    // create a struct to hold various device information
	ZeroMemory(&m_D3Dpp, sizeof(m_D3Dpp));    // clear out the struct for use


    
	m_D3Dpp.hDeviceWindow					= hWnd;										// Handle to the focus window
	m_D3Dpp.Windowed						= bWindowed;								// Windowed or Full-screen boolean
	m_D3Dpp.AutoDepthStencilFormat		= D3DFMT_D24S8;								// Format of depth/stencil buffer, 24 bit depth, 8 bit stencil
	m_D3Dpp.EnableAutoDepthStencil		= TRUE;										// Enables Z-Buffer (Depth Buffer)
	m_D3Dpp.BackBufferCount				= 1;										// Change if need of > 1 is required at a later date
	m_D3Dpp.BackBufferFormat				= D3DFMT_X8R8G8B8;							// Back-buffer format, 8 bits for each pixel
	m_D3Dpp.BackBufferHeight				= height;									// Make sure resolution is supported, use adapter modes
	m_D3Dpp.BackBufferWidth				= width;									// (Same as above)
	m_D3Dpp.SwapEffect					= D3DSWAPEFFECT_DISCARD;					// Discard back-buffer, must stay discard to support multi-sample
	m_D3Dpp.PresentationInterval			= m_bVsync ? D3DPRESENT_INTERVAL_DEFAULT : D3DPRESENT_INTERVAL_IMMEDIATE; // Present back-buffer immediately, unless V-Sync is on								
	m_D3Dpp.Flags							= D3DPRESENTFLAG_DISCARD_DEPTHSTENCIL;		// This flag should improve performance, if not set to NULL.
	m_D3Dpp.FullScreen_RefreshRateInHz	= bWindowed ? 0 : D3DPRESENT_RATE_DEFAULT;	// Full-screen refresh rate, use adapter modes or default
	m_D3Dpp.MultiSampleQuality			= 0;										// MSAA currently off, check documentation for support.
	m_D3Dpp.MultiSampleType				= D3DMULTISAMPLE_NONE;						// MSAA currently off, check documentation for support.



    // Check device capabilities
	DWORD deviceBehaviorFlags = 0;
	m_pD3DObject->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &m_D3DCaps);

	// Determine vertex processing mode
	if(m_D3DCaps.DevCaps & D3DCREATE_HARDWARE_VERTEXPROCESSING)
	{
		// Hardware vertex processing supported? (Video Card)
		deviceBehaviorFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;	
	}
	else
	{
		// If not, use software (CPU)
		deviceBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING; 
	}
	
	// If hardware vertex processing is on, check pure device support
	if(m_D3DCaps.DevCaps & D3DDEVCAPS_PUREDEVICE && deviceBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING)
	{
		deviceBehaviorFlags |= D3DCREATE_PUREDEVICE;	
	}
	
	// Create the D3D Device with the present parameters and device flags above
	m_pD3DObject->CreateDevice(
		D3DADAPTER_DEFAULT,		// which adapter to use, set to primary
		D3DDEVTYPE_HAL,			// device type to use, set to hardware rasterization
		hWnd,					// handle to the focus window
		deviceBehaviorFlags,	// behavior flags
		&m_D3Dpp,					// presentation parameters
		&m_pD3DDevice);			// returned device pointer


	
	//////////////////////////////////////////////////////////////////////////
	// Create Sprite Object and Textures - for rotating text
	//////////////////////////////////////////////////////////////////////////

	// Create a sprite object, note you will only need one for all 2D sprites
	D3DXCreateSprite(m_pD3DDevice,&m_pD3DSprite);

	

	//////////////////////////////////////////////////////////////////////////
	// Init DirectShow
	//////////////////////////////////////////////////////////////////////////
	InitDirectShow();


	//////////////////////////////////////////////////////////////////////////
	// Load Default State
	//////////////////////////////////////////////////////////////////////////
	/*m_pStateMachine->SetCurrentState(D3DMenu1);
	m_pStateMachine->ChangeState(D3DMenu1);*/
	m_pStateMachine->SetInitialState(Gfx2Demo);
}

void D3DApp::Update(float dt) //updates scene
{
	//update stats
	GStats->update(dt);
	//update direct input
	gDInput->poll();
	//update scene
	m_pStateMachine->Update(dt);
	
}

// this is the function used to render a single frame
void D3DApp::Render()
{
	

	m_pStateMachine->Render();
	
}

//////////////////////////////////////////////////////////////////////////
// Utilities
//////////////////////////////////////////////////////////////////////////
int D3DApp::Run()
{

	

	 // enter the main loop:

    // this struct holds Windows event messages
    MSG msg;

	
	//this lets me quit the game :)
	while(D3DAPPI->GetQuit() == false)
    {
		
        // Check to see if any messages are waiting in the queue
        while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
            // translate keystroke messages into the right format
            TranslateMessage(&msg);

            // send the message to the WindowProc function
            DispatchMessage(&msg);
        }

        // If the message is WM_QUIT, exit the while loop
        if(msg.message == WM_QUIT)
            break;

        // Run game code here
       
		if (!IsDeviceLost() )
		{
			
			//regular running					
			Update(DXTIMER->GetDT());

			//debug running (static dt)
			//Update(.0009);
			Render();	

			
		}

		
		

		DXTIMER->Lap();
		
    }

	//clean up DirectX and COM
	//DirectX->CleanD3D();
	CleanD3D();

    // return this part of the WM_QUIT message to Windows
    return msg.wParam;
}
//changes states
void D3DApp::ChangeState(State<D3DApp>* pNewState)
{
	//make sure both states are valid before attempting to
	//call their methods
	assert (m_pCurrentState && pNewState);

	//call the exit method of the existing state
	m_pCurrentState->Exit(this);

	//change state to the new state
	m_pCurrentState = pNewState;

	//call the entry method of the new state
	m_pCurrentState->Enter(this);
}

void D3DApp::RevertToPreviousState()
{
	return;
}

void D3DApp::Poll() 
{
		
}

void D3DApp::Quit()
{
	m_bQuit = true;
}

bool D3DApp::IsDeviceLost()
{
      // Returns true if lost, false otherwise.
	// Get the state of the graphics device.
      HRESULT hr = m_pD3DDevice->TestCooperativeLevel();
      // If the device is lost and cannot be reset yet, then
      // sleep for a bit and we'll try again on the next
      // message loop cycle.
      if( hr == D3DERR_DEVICELOST )
      {
           Sleep(20);
           return true;
      }
      // Driver error, exit.
      else if( hr == D3DERR_DRIVERINTERNALERROR )
      {
           MessageBox(0, L"Internal Driver Error...Exiting", 0, 0);
           PostQuitMessage(0);
           return true;
      }
      // The device is lost but we can reset and restore it.
      else if( hr == D3DERR_DEVICENOTRESET )
      {
		  m_pStateMachine->OnLostDevice();
          m_pD3DDevice->Reset(&m_D3Dpp);
		  m_pStateMachine->OnResetDevice();
           // Not lost anymore.
           return false;
      }
      else
           // Not lost anymore.
           return false;
}

void D3DApp::EnableFullScreenMode(bool enable) // enables full screen mode
{
	// Switch to fullscreen mode.
	if( enable )
	{
		// Are we already in fullscreen mode?
		if( !m_D3Dpp.Windowed ) 
			return;

		int width  = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		m_D3Dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
		m_D3Dpp.BackBufferWidth  = width;
		m_D3Dpp.BackBufferHeight = height;
		m_D3Dpp.Windowed         = false;

		// Change the window style to a more fullscreen friendly style.
		SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_POPUP);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(m_hWnd, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);	
	}
	// Switch to windowed mode.
	else
	{
		// Are we already in windowed mode?
		if( m_D3Dpp.Windowed ) 
			return;

		RECT R = {0, 0, 800, 600};
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
		m_D3Dpp.BackBufferFormat = D3DFMT_UNKNOWN;
		m_D3Dpp.BackBufferWidth  = 800;
		m_D3Dpp.BackBufferHeight = 600;
		m_D3Dpp.Windowed         = true;
	
		// Change the window style to a more windowed friendly style.
		SetWindowLongPtr(m_hWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(m_hWnd, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	}

	// Reset the device with the changes.
	m_pStateMachine->OnLostDevice();
	D3DAPPI->GetD3DDevice()->Reset(&m_D3Dpp);
	m_pStateMachine->OnResetDevice();
}

	
//////////////////////////////////////////////////////////////////////////
// Accessors
//////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////
// Mutators
//////////////////////////////////////////////////////////////////////////
