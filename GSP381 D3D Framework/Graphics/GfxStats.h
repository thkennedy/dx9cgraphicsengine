#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include "../stdafx.h"

#pragma comment(lib, "winmm.lib")

// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "strmiids.lib")


#define GStats GfxStats::Instance()


class GfxStats
{

private:
	GfxStats();
	
    // Prevent copying
   /* GfxStats(const GfxStats& rhs);
    GfxStats& operator=(const GfxStats& rhs);*/
    ID3DXFont* mFont;
    float mFPS;
    float mMilliSecPerFrame;
    DWORD mNumSphere;
    DWORD mNumAABB;
	DWORD mNumOBB;

public:
      static GfxStats* Instance();
      ~GfxStats();
      void onLostDevice();
      void onResetDevice();

      void addSphere(DWORD n);
      void subVertices(DWORD n);
      void addAABB(DWORD n);
      void addOBB(DWORD n);

      
      void update(float dt);
      void display();
};
