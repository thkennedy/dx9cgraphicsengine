#include "D3DLine.h"
#include "D3DApp.h"
#include "D3DCamera.h"
#include "D3DVertex.h"


D3DLine::D3DLine(void)
{

	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	
	m_vOrientation = m_vPosition = m_vCenter = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(1.0f,1.0f,1.0f);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	
	//HRESULT hr =	D3DXCreateSphere(D3DAPPI->GetD3DDevice(), 1.0f, 40, 20, &m_pMeshLine, NULL);

   //	BuildFX();
	
	OnResetDevice();
}

D3DLine::D3DLine(LPD3DXLINE pLine,D3DXVECTOR3 vStartPosition, D3DXVECTOR3 vEndPosition, D3DCOLOR cColor, int id)
	:D3DShape(id)
{
	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	
	m_vOrientation = m_vPosition = m_vCenter = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vPosition = vStartPosition;

	m_vEndPosition = vEndPosition;

	
	m_pMeshLine = pLine;

   	
	
	m_cColor = cColor;
	
	
	
	//BuildFX();
	

	OnResetDevice();
}


D3DLine::~D3DLine(void)
{
}


void D3DLine::CalculateInternals()
{

	//////////////
	//	ROTATION
	//////////////

	D3DXMATRIX rotationMatrix;

	D3DXMatrixRotationQuaternion(&rotationMatrix, &m_qOrientation);

	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = matScale * rotationMatrix * matTranslate ;


}

void D3DLine::BuildFX()
{
	HRESULT hr;

	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/color.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech			= m_pEffect->GetTechniqueByName("ColorTech");
	m_hWVP			= m_pEffect->GetParameterByName(0, "gWVP");
	
		
	//m_pEffect->SetTexture(m_hTexture,m_Texture);

	


}

void D3DLine::Update(float dt)
{
				
	CalculateInternals();

}

void D3DLine::Render()
{
	D3DXMATRIX T = m_pCamView->GetView() * m_pCamView->GetProj();
	D3DXVECTOR3 points[2];

	points[0] = m_vPosition;
	points[1] = m_vEndPosition;

	m_pMeshLine->DrawTransform(points, 2, &T, m_cColor);
	
}

void D3DLine::OnResetDevice()
{
	m_pMeshLine->OnResetDevice();

	
}
void D3DLine::OnLostDevice()
{
	m_pMeshLine->OnLostDevice();
	
}


