#pragma once

#include "../stdafx.h"
#include <d3d9.h>
#include <d3dx9.h>
#include "../Physics/Vector3.h"
#pragma comment(lib, "winmm.lib")

// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")



class D3DText
{
protected:

	//////////////////////////////////////////////////////////////////////////
	// Location/Speed Variables
	//////////////////////////////////////////////////////////////////////////
	RECT *m_pDisplayRect;
	Vector3 m_vPosition;
	

	//////////////////////////////////////////////////////////////////////////
	// Window Vars
	//////////////////////////////////////////////////////////////////////////
	D3DDEVICE_CREATION_PARAMETERS m_d3dcpWindowParams;
	RECT m_rDisplay;

	//////////////////////////////////////////////////////////////////////////
	// Direct3D Variables
	//////////////////////////////////////////////////////////////////////////
	LPDIRECT3D9 m_pD3DObject;		// the pointer to our Direct3D interface
	LPDIRECT3DDEVICE9 m_pD3DDevice; // the pointer to the device class
	
	//////////////////////////////////////////////////////////////////////////
	// Font Variables
	//////////////////////////////////////////////////////////////////////////
	LPD3DXFONT			m_pD3DFont;		// Font Object	
	D3DCOLOR			m_FontColor;	// Font Color
	LPCSTR				m_sText;		// text displayed

	



public:
	D3DText(HWND hWnd, LPDIRECT3D9 m_D3DObject, LPDIRECT3DDEVICE9 m_D3DDevice);
	~D3DText(void);

	//////////////////////////////////////////////////////////////////////////
	// Utilities
	//////////////////////////////////////////////////////////////////////////
	void Update(float time_elapsed);
	void SyncDisplayWithPosition();
	void Render();
	void OnLostDevice();
	void OnResetDevice();

	//////////////////////////////////////////////////////////////////////////
	// Accessors
	//////////////////////////////////////////////////////////////////////////
	Vector3 GetPosition() {return m_vPosition;}
	RECT GetDisplayRect() {return m_rDisplay;}


	//////////////////////////////////////////////////////////////////////////
	// Mutators
	//////////////////////////////////////////////////////////////////////////
	void SetFontObj(LPD3DXFONT newFont){m_pD3DFont = newFont;}
	void SetPosition(Vector3 newPos);
	void SetText(LPCSTR newText){
		m_sText = newText;
	}
	void SetDisplayRect(RECT newRect) {m_rDisplay = newRect;}
	void SetDisplayRect(Vector3 topLeft, Vector3 bottomRight);
	void SetFontColor(D3DCOLOR newColor){m_FontColor = newColor;}



};

