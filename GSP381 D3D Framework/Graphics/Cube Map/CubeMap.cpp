#include "CubeMap.h"
#include "../D3DCamera.h"
#include "../D3DApp.h"


CubeMap::CubeMap(const std::string& filename, float skyRadius)
: mRadius(skyRadius)
{
	D3DXCreateSphere(D3DAPPI->GetD3DDevice(), skyRadius, 30, 30, &mSphere, 0);
	D3DXCreateCubeTextureFromFileA(D3DAPPI->GetD3DDevice(), filename.c_str(), &mEnvMap);

	ID3DXBuffer* errors = 0;
	D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/cubemap.fx", 0, 0, 0, 0, &mFX, &errors);
	if( errors )
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);

	m_hTech   = mFX->GetTechniqueByName("SkyTech");
	m_hWVP    = mFX->GetParameterByName(0, "gWVP");
	m_hEnvMap = mFX->GetParameterByName(0, "gEnvMap");

	// Set effect parameters that do not vary.
	mFX->SetTechnique(m_hTech);
	mFX->SetTexture(m_hEnvMap, mEnvMap);
}

CubeMap::~CubeMap()
{
	SAFE_RELEASE(mSphere);
	SAFE_RELEASE(mEnvMap);
	SAFE_RELEASE(mFX);
}

DWORD CubeMap::GetNumTriangles()
{
	return mSphere->GetNumFaces();
}

DWORD CubeMap::GetNumVertices()
{
	return mSphere->GetNumVertices();
}

IDirect3DCubeTexture9* CubeMap::GetCubeMap()
{
	return mEnvMap;
}

float CubeMap::GetRadius()
{
	return mRadius;
}

void CubeMap::OnLostDevice()
{
	mFX->OnLostDevice();
}

void CubeMap::OnResetDevice()
{
	mFX->OnResetDevice();
}

void CubeMap::Render()
{
	// Sky always centered about camera's position.
	D3DXMATRIX W;
	D3DXVECTOR3 p = m_pCamera->GetPosition();
	D3DXMatrixTranslation(&W, p.x, p.y, p.z);
	mFX->SetMatrix(m_hWVP, &(W*m_pCamera->GetViewProj()));
	
	UINT numPasses = 0;
	mFX->Begin(&numPasses, 0);
		mFX->BeginPass(0);
			mSphere->DrawSubset(0);
		mFX->EndPass();
	mFX->End();
}