#pragma once

#include "../d3dUtility.h"
#include <string>


class D3DCamera;

class CubeMap
{
private:

	ID3DXMesh* mSphere;

	float mRadius;

	IDirect3DCubeTexture9* mEnvMap;
	ID3DXEffect* mFX;

	D3DXHANDLE m_hTech;
	D3DXHANDLE m_hEnvMap;
	D3DXHANDLE m_hWVP;

	D3DCamera* m_pCamera;


public:
	CubeMap(const std::string& filename, float radius);
	~CubeMap();

	
	void Render();
	void OnLostDevice();
	void OnResetDevice();

	///////////////////
	// ACCESSORS
	//////////////////

	IDirect3DCubeTexture9* GetCubeMap();
	float GetRadius();
	DWORD GetNumTriangles();
	DWORD GetNumVertices();

	////////////////
	// MUTATORS
	///////////////
	void SetCamera(D3DCamera* cam){m_pCamera = cam;}

	

	
};

