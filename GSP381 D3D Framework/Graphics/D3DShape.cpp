
#include "D3DShape.h"
#include "D3DApp.h"
#include "../Physics/Quaternion.h"


D3DShape::D3DShape()
{
	m_iID = -1;

	m_qOrientation = D3DXQUATERNION(0.0f, 0.0f, 0.0f, 1.0f);
}

D3DShape::D3DShape(int id)
{
	m_iID = id;

	m_qOrientation = D3DXQUATERNION(0.0f, 0.0f, 0.0f, 1.0f);
}

D3DShape::~D3DShape()
{

}



/////////////////
//	MUTATORS
////////////////
void D3DShape::SetPosition(D3DXVECTOR3 pos)
{
	m_vPosition = pos;
}
void D3DShape::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z;
}
void D3DShape::SetPositionX(float x)
{
	m_vPosition.x = x;
}
void D3DShape::SetPositionY(float y)
{
	m_vPosition.y = y;
}
void D3DShape::SetPositionZ(float z)
{
	m_vPosition.z = z;
}

void D3DShape::SetRotation(D3DXVECTOR3 rot)
{
	m_vOrientation = rot;
}
void D3DShape::SetRotation(float x, float y, float z)
{
	m_vOrientation.x = x;
	m_vOrientation.y = y;
	m_vOrientation.z = z;
}
void D3DShape::SetRotationX(float x)
{
	m_vOrientation.x = x;
}
void D3DShape::SetRotationY(float y)
{
	m_vOrientation.y = y;
}
void D3DShape::SetRotationZ(float z)
{
	m_vOrientation.z = z;
}
void D3DShape::SetHeading(float yaw, float pitch, float roll)
{

	D3DXQuaternionRotationYawPitchRoll(&m_qOrientation, yaw, pitch, roll);

}

void D3DShape::SetHeading(D3DXQUATERNION qHeading)
{
	m_qOrientation = qHeading;
}

void D3DShape::SetTexture (D3DXHANDLE hTex, IDirect3DTexture9* texture)
{
	m_pEffect->SetTexture(hTex,texture);
}

void D3DShape::SetTexture (IDirect3DTexture9* texture)
{
	m_Texture = texture;
}


