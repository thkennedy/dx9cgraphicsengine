#include "D3DSphere.h"
#include "D3DApp.h"
#include "D3DCamera.h"
#include "D3DVertex.h"


D3DSphere::D3DSphere(void)
{

	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	
	m_vOrientation = m_vPosition = m_vCenter = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(1.0f,1.0f,1.0f);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	
	//HRESULT hr =	D3DXCreateSphere(D3DAPPI->GetD3DDevice(), 1.0f, 40, 20, &m_pMeshSphere, NULL);

   	//setting up light
	mLightVecW     = D3DXVECTOR3(75.0f,30.0f,75.0f);
	mDiffuseMtrl   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mDiffuseLight  = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	mAmbientMtrl   = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	mAmbientLight  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mSpecularMtrl  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mSpecularLight = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	mSpecularPower = 8.0f;
	
	BuildFX();
	
	OnResetDevice();

}

D3DSphere::D3DSphere(LPD3DXMESH pSphere, float radius, IDirect3DTexture9* pTexture, int id)
	:D3DShape(id)
{

	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	
	m_vOrientation = m_vPosition = m_vCenter = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3( radius, radius, radius);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	
	m_pMeshSphere = pSphere;

   	//setting up light
	mLightVecW     = D3DXVECTOR3(75.0f,30.0f,75.0f);
	mDiffuseMtrl   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mDiffuseLight  = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	mAmbientMtrl   = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);
	mAmbientLight  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mSpecularMtrl  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mSpecularLight = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	mSpecularPower = 8.0f;
	
	m_Texture = pTexture;
	
	
	
	BuildFX();
	

	OnResetDevice();

}


D3DSphere::~D3DSphere(void)
{
	
}

void D3DSphere::CalculateInternals()
{

	//////////////
	//	ROTATION
	//////////////

	D3DXMATRIX rotationMatrix;

	D3DXMatrixRotationQuaternion(&rotationMatrix, &m_qOrientation);

	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = matScale * rotationMatrix * matTranslate ;


}

void D3DSphere::BuildFX()
{
	HRESULT hr;

	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/ship.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech			= m_pEffect->GetTechniqueByName("ShipTech");
	m_hWVP			= m_pEffect->GetParameterByName(0, "gWVP");
	m_hTexture		= m_pEffect->GetParameterByName(0,"gTex");
	m_hWorldInvTrans = m_pEffect->GetParameterByName(0, "gWorldInvTrans");
	m_hLightVecW     = m_pEffect->GetParameterByName(0, "gLightVecW");
	m_hDiffuseMtrl   = m_pEffect->GetParameterByName(0, "gDiffuseMtrl");
	m_hDiffuseLight  = m_pEffect->GetParameterByName(0, "gDiffuseLight");
	m_hAmbientMtrl   = m_pEffect->GetParameterByName(0, "gAmbientMtrl");
	m_hAmbientLight  = m_pEffect->GetParameterByName(0, "gAmbientLight");
	m_hSpecularMtrl  = m_pEffect->GetParameterByName(0, "gSpecularMtrl");
	m_hSpecularLight = m_pEffect->GetParameterByName(0, "gSpecularLight");
	m_hSpecularPower = m_pEffect->GetParameterByName(0, "gSpecularPower");
	m_hEyePos        = m_pEffect->GetParameterByName(0, "gEyePosW");
	m_hWorld         = m_pEffect->GetParameterByName(0, "gWorld");
		
	m_pEffect->SetTexture(m_hTexture,m_Texture);

	


}

void D3DSphere::Update(float dt)
{
				
	CalculateInternals();

}

void D3DSphere::Render()
{
	HRESULT hr;

	#if 1
		// Setup the rendering FX
		hr = m_pEffect->SetTechnique(m_hTech);
		hr = m_pEffect->SetMatrix(m_hWVP, &(m_mTransform * m_pCamView->GetViewProj()));

		D3DXMATRIX worldInvTrans;
		D3DXMatrixInverse(&worldInvTrans, 0, &m_mTransform);
		D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
		

		hr =m_pEffect->SetMatrix(m_hWorldInvTrans, &worldInvTrans);
		hr =m_pEffect->SetValue(m_hLightVecW, &mLightVecW, sizeof(D3DXVECTOR3));
		hr =m_pEffect->SetValue(m_hDiffuseMtrl, &mDiffuseMtrl, sizeof(D3DXCOLOR));
		hr =m_pEffect->SetValue(m_hDiffuseLight, &mDiffuseLight, sizeof(D3DXCOLOR));
		hr =m_pEffect->SetValue(m_hAmbientMtrl, &mAmbientMtrl, sizeof(D3DXCOLOR));
		hr =m_pEffect->SetValue(m_hAmbientLight, &mAmbientLight, sizeof(D3DXCOLOR));
		hr =m_pEffect->SetValue(m_hSpecularLight, &mSpecularLight, sizeof(D3DXCOLOR));
		hr =m_pEffect->SetValue(m_hSpecularMtrl, &mSpecularMtrl, sizeof(D3DXCOLOR));
		hr =m_pEffect->SetFloat(m_hSpecularPower, mSpecularPower);
		hr =m_pEffect->SetMatrix(m_hWorld, &m_mTransform);
		hr =m_pEffect->SetTexture(m_hTexture,m_Texture);
		hr =m_pEffect->CommitChanges();

		

	#endif



		// Begin passes.
		UINT numPasses = 0;
		m_pEffect->Begin(&numPasses, 0);
		for(UINT i = 0; i < numPasses; ++i)
		{
			m_pEffect->BeginPass(i);
			// draw the spaceship
			hr = m_pMeshSphere->DrawSubset(0);    // draw the subset
		
			m_pEffect->EndPass();
		}
		m_pEffect->End();
	
	
}

void D3DSphere::OnResetDevice()
{
	m_pEffect->OnResetDevice();
	//D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);
}
void D3DSphere::OnLostDevice()
{
	m_pEffect->OnLostDevice();
}


