//=============================================================================
// cubemap.fx by Tim Kennedy
//
// 
// 
//=============================================================================

uniform extern float4x4 gWVP;
uniform extern texture	gEnvMap;
	
//my sampler
sampler EnvMapS = sampler_state
{
	Texture = <gEnvMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
	AddressV  = WRAP;
};


//my vert shader
struct OutputVS
{
    float4 posH		: POSITION0;
	float4 diffuse	: COLOR0;
	float2 tex0		: TEXCOORD0;
	

};

void SkyVS(float3 posL	:	POSITION0,
			out	float4	oPosH	:	POSITION0,
			out float3	oEnvTex	:	TEXCOORD0)
{
	// set z = w so that z/w = 1 and puts skydome always on far plane
	oPosH = mul(float4(posL, 1.0f), gWVP).xyww;

	//use skymesh vertex position, in local space, as index into cube map
	oEnvTex = posL;
}

float4 SkyPS (float3 envTex : TEXCOORD0) : COLOR
{
	return texCUBE(EnvMapS,envTex);
}

technique	SkyTech
{
	pass P0
	{
		vertexShader = compile vs_2_0 SkyVS();
		pixelShader = compile ps_2_0 SkyPS();

		CullMode = None;
		ZFunc = Always;		//this puts sky to depth
		
		//buffer
			StencilEnable	= TRUE;
			StencilFunc		= Always;
			StencilPass		= Replace;
			StencilRef		= 0; // clears to zero		
	}
}