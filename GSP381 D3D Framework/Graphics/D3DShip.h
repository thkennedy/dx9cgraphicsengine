#pragma once
#include "D3DShape.h"

#define MAXROTATION_Y	1.0f
#define MAXROTATION_X	0.7f
class D3DShip :
	public D3DShape
{
private:

	///////////////////
	// Movement
	/////////////////
	D3DXVECTOR3 m_vVelocity;
	float m_fSpeed;

	virtual void CalculateInternals();

	LPD3DXMESH m_pMeshShip;    // define the mesh pointer
	D3DMATERIAL9* m_pMaterial;    // define the material object
	DWORD m_wNumMaterials;    // stores the number of materials in the mesh
	
	D3DXVECTOR3 mLightVecW;		// light in world coords
	D3DXCOLOR   mAmbientMtrl;	// ambient mat color
	D3DXCOLOR   mAmbientLight;	// ambient light color
	D3DXCOLOR   mDiffuseMtrl;	// diffse mat color
	D3DXCOLOR   mDiffuseLight;	// diffuse light color
	D3DXCOLOR   mSpecularMtrl;	// specular mat color
	D3DXCOLOR   mSpecularLight;	// specular light color
	float       mSpecularPower;	// specular power


public:
	D3DShip(int id);
	~D3DShip(void);

	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();


	//////////////////////
	// UTILITIES
	/////////////////////

	
	
	/////////////////////////
	// ACCESSORS
	////////////////////////
	D3DXVECTOR3 GetVelocity(){return m_vVelocity;}
	float GetSpeed(){return m_fSpeed;}

	///////////////////////
	// MUTATORS
	//////////////////////
	void SetVelocity(D3DXVECTOR3 vel){m_vVelocity = vel;}
	void SetVelocity(float x, float y, float z){m_vVelocity += D3DXVECTOR3(x,y,z);}
	void ChangeVelocityX(float x){m_vVelocity.x = x;}
	void ChangeVelocityY(float y){m_vVelocity.y = y;}
	void ChangeVelocityZ(float z){m_vVelocity.z = z;}
	void SetSpeed(float speed){m_fSpeed = speed;}
	
};