
#include "DirectXBase.h"
#include "Sprites/D3DSpriteManager.h"


DirectXBase::DirectXBase()
{
	// Init or NULL objects before use to avoid any undefined behavior
	m_bVsync		= false;
	m_pD3DObject	= 0;
	m_pD3DDevice	= 0;

}


DirectXBase::~DirectXBase()
{
}


void DirectXBase::OnResetDevice() //for future use
{

}

void DirectXBase::OnLostDevice() // for future use
{

}

// this is the function that cleans up Direct3D and COM
void DirectXBase::CleanD3D(void)
{

	//*************************************************************************
	// Release COM objects in the opposite order they were created in

	// Texture
	SMI->ClearRegistry();


	// Sprite
	m_pD3DSprite->Release();

	// Font

	
	m_pD3DDevice->Release();    // close and release the 3D device

	

	m_pD3DObject->Release();    // close and release Direct3D

	//*************************************************************************
   
}



int DirectXBase::InitDirectShow()
{
	HRESULT hr;
	
	//init the COM
	CoInitialize(NULL);

	//create filter graph
	hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_IGraphBuilder,(void**)&m_pGraphBuilder);
	if (FAILED(hr))
		return -1;

	//create media control
	hr = m_pGraphBuilder->QueryInterface(IID_IMediaControl, (void**)&m_pMediaControl);

	//create media event object
	hr = m_pGraphBuilder->QueryInterface(IID_IMediaEvent, (void**)&m_pMediaEvent);


	return 0;
}

