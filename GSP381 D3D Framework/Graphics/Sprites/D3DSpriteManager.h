#pragma once
#include <vector>

#define SMI D3DSpriteManager::Instance()

class D3DSprite;


class D3DSpriteManager
{
private:
	D3DSpriteManager();

	//sprite registry
	std::vector<D3DSprite*> m_vSprites;

	//for managing sprites
	int m_NextID;

	int GetNextSpriteID(){return m_NextID++;}


public:
	~D3DSpriteManager() {};
	

	static D3DSpriteManager* Instance();
	
	//utilities
	void RegisterSprite(D3DSprite* newSprite);
	void RemoveSprite(D3DSprite* deleteSprite);
	void ClearRegistry();
	void UpdateSprites(float dt);
	void RenderSprites();


	//accessors


	//mutators

	
};


/*
#define DXTIMER Timer::Instance()

class Timer
{
private:
	LARGE_INTEGER Freq, Count;
	double begin, end, elapsed;
	Timer();
public:
	static Timer* Instance();

	void Start();
	void Stop();
	double GetTime();
	void Clear();

};
*/