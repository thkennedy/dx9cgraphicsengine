#include "D3DSpriteManager.h"
#include "../D3DSprite.h"


D3DSpriteManager::D3DSpriteManager(void)
{
	m_NextID = 1;
	m_vSprites.clear();

}

D3DSpriteManager* D3DSpriteManager::Instance()
{
	static D3DSpriteManager Instance;

	return &Instance;
}

void D3DSpriteManager::RegisterSprite(D3DSprite* newSprite) 
{
	//only add sprite if sprite hasn't already been registered
	if (newSprite->GetID() == -1)
	{
		//assign sprite an id
		newSprite->SetID(SMI->GetNextSpriteID());
		//put sprite on the list to be rendered
		m_vSprites.push_back(newSprite);
	}
	
}

void D3DSpriteManager::RemoveSprite(D3DSprite* deleteSprite)
{
	std::vector<D3DSprite*>::iterator it = m_vSprites.begin();

	unsigned int i;

	for (i=0, it; i < m_vSprites.size(); i++)
	{
		if ((*it) == m_vSprites[i])
		{
			m_vSprites.erase(it);
			return;
		}

		++it;
	}
}

void D3DSpriteManager::ClearRegistry() 
{
	//release all sprites

	std::vector<D3DSprite*>::iterator it = m_vSprites.begin();

	for (it; it != m_vSprites.end(); it++)
	{
		(*it)->ReleaseSprite();
	}

	//clear registry
	m_vSprites.clear();

	m_NextID = 0;
}

void D3DSpriteManager::UpdateSprites(float dt)
{
	for (auto it = m_vSprites.begin(); it != m_vSprites.end(); it++)
	{
		(*it)->Update(dt);
	}
}

void D3DSpriteManager::RenderSprites()
{
	std::vector<D3DSprite*>::iterator it = m_vSprites.begin();

	for (int i = 0; i < m_vSprites.size(); i++)
	{
		m_vSprites[i]->Render();
		//(*it)->Render();
	}
}

