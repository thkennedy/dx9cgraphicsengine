#pragma once

#include "directxbase.h"
#include "../Graphics/d3dUtility.h"
#include "../FSM/FSM.h"

#define D3DAPPI D3DApp::Instance()

//forward declarations
class D3DStates;
class D3DText;



class D3DApp :
	public DirectXBase
{
private:

	D3DApp();

	
	//////////////////////////////////////////////////////////////////////////
	// FSM Vars
	//////////////////////////////////////////////////////////////////////////
	FiniteStateMachine<D3DApp>* m_pStateMachine;
	//states
	State<D3DApp>* m_pCurrentState;
	State<D3DApp>* m_pPreviousState;
	D3DStates* m_pStates;

	//////////////////////////////////////////////////////////////////////////
	// Text Vars
	//////////////////////////////////////////////////////////////////////////
	std::vector<LPD3DXFONT> m_vD3DFontObjects;

	//fps
	D3DText *m_pFPS;

	//quit var
	bool m_bQuit;

	//is app paused?
	bool m_bAppPaused;


	
	
	
public:
	static D3DApp* Instance();
	~D3DApp(void);

	//************************
	//****Overrides	   ******
	//**********************
	
	void InitD3D(HWND hWnd, HINSTANCE& hInst, bool bWindowed);    // sets up and initializes Direct3D
	void InitMainWindow();
	void Update(float dt); //updates scene
	void Render();    // renders a single frame

	//////////////////////////////////////////////////////////////////////////
	// Utilities
	//////////////////////////////////////////////////////////////////////////
	//changes states
	int Run();
	void ChangeState(State<D3DApp>* pNewState);
	void RevertToPreviousState();
	void Poll();
	void Quit();
	bool IsDeviceLost();
	void EnableFullScreenMode(bool enable); // enables full screen mode
	
	//////////////////////////////////////////////////////////////////////////
	// Accessors
	//////////////////////////////////////////////////////////////////////////
	FiniteStateMachine<D3DApp>* GetFSM()const{return m_pStateMachine;}
	bool GetQuit() {return m_bQuit;}

	

	//////////////////////////////////////////////////////////////////////////
	// Mutators
	//////////////////////////////////////////////////////////////////////////
	void PauseApp() {m_bAppPaused = true;}
	void UnpauseApp() {m_bAppPaused = false;}

};

