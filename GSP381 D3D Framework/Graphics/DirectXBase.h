#pragma once

#include "../stdafx.h"

#include <d3d9.h>
#include <d3dx9.h>
#include <DShow.h>
#include "d3dUtility.h"



#pragma comment(lib, "winmm.lib")

// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "strmiids.lib")



//directx base class
class DirectXBase
{
protected:

	//variables

	//////////////////////////////////////////////////////////////////////////
	// Application Variables
	//////////////////////////////////////////////////////////////////////////
	HWND					m_hWnd;		// Handle to the window
	bool					m_bVsync;	// Boolean for vertical syncing
	HINSTANCE				m_hInst;	// instance variable 
	

	//////////////////////////////////////////////////////////////////////////
	// Direct3D Variables
	//////////////////////////////////////////////////////////////////////////
	IDirect3D9* m_pD3DObject;		// the pointer to our Direct3D interface
	IDirect3DDevice9* m_pD3DDevice; // the pointer to the device class
	D3DCAPS9 m_D3DCaps;				// device capabilities

	//////////////////////////////////////////////////////////////////////////
	// Sprite Variables
	//////////////////////////////////////////////////////////////////////////
	ID3DXSprite*		m_pD3DSprite;	// Sprite Object
	IDirect3DTexture9*	m_pTexture;		// Texture Object for a sprite
	LPD3DXSPRITE m_psprite;
	
	//////////////////////////////////////////////////////////////////////////
	// VERTEX AND INDEX BUFFERS
	//////////////////////////////////////////////////////////////////////////
	IDirect3DVertexBuffer9* m_VertexBuffer;
	IDirect3DIndexBuffer9*	m_IndexBuffer;
	

	

	
public:
	//////////////////////////////////////////////////////////////////////////
	// DirectShow Variables
	//////////////////////////////////////////////////////////////////////////
	IGraphBuilder*	m_pGraphBuilder;
	IMediaControl*	m_pMediaControl;
	IMediaEventEx*	m_pMediaEvent;
	IMediaPosition*	m_pMediaPosition;
	IVideoWindow*	m_pVideoWindow;

	D3DPRESENT_PARAMETERS	m_D3Dpp;	// presentation parameters



	//not much happens here - all is done in Init and CleanD3D
	DirectXBase();
	//destructor is virtual so class must be derived from before using
	virtual ~DirectXBase();

	//*******************************
	//******* Utilities *********** 
	//****************************



	//******* D3D ***********
	virtual void InitD3D(HWND hWnd, HINSTANCE& hInst, bool bWindowed) = 0;// sets up and initializes Direct3D
	int InitDirectShow();//sets up and inits directshow
	virtual void Update(float dt) = 0; //updates scene
	virtual void Render() = 0;    // renders a single frame
	void OnResetDevice(); //for future use
	void OnLostDevice(); // for future use
	void CleanD3D();    // closes Direct3D and releases memory
	
	

	
	//////////////////////////////////////////////////////////////////////////
	// ACCESSORS
	//////////////////////////////////////////////////////////////////////////
	HWND GetHWND() {return m_hWnd;}
	IDirect3D9* GetD3DObject() {return m_pD3DObject;}		// the pointer to our Direct3D interface
	IDirect3DDevice9* GetD3DDevice() {return m_pD3DDevice;} // the pointer to the device class
	ID3DXSprite*	GetD3DSpriteObject() {return m_pD3DSprite;}	// Sprite Object
//	IDirect3DTexture9*	GetD3DTextureObject{){return m_pTexture; }		// Texture Object for a sprite
	HINSTANCE& GethInst() {return m_hInst;}
	D3DPRESENT_PARAMETERS GetD3Dpp() {return m_D3Dpp;}
	
	IGraphBuilder* GetGraph() {return m_pGraphBuilder;}
	IVideoWindow* GetVidWindow() {return m_pVideoWindow;}
	IMediaControl* GetVidControl() {return m_pMediaControl;}
	IMediaEventEx*	GetVidEvent() {return m_pMediaEvent;}

	//////////////////////////////////////////////////////////////////////////
	// MUTATORS
	//////////////////////////////////////////////////////////////////////////
	
	void SetVidWindow(IVideoWindow* vidWindow){m_pVideoWindow = vidWindow;}
	void CleanUpDirectShow(){SAFE_RELEASE(m_pMediaControl);SAFE_RELEASE(m_pVideoWindow);SAFE_RELEASE(m_pGraphBuilder);}


	



};

