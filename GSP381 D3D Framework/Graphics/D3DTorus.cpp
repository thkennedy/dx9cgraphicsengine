#include "D3DTorus.h"
#include "D3DApp.h"
#include "D3DVertex.h"
#include "D3DCamera.h"
#include "../Objects/GameObjects/Score.h"
#include "../Sound/SoundEffect.h"
#include "../Physics/functions.h"


D3DTorus::D3DTorus(void)
{
	/////////////////////////
	// INTERNAL VARS
	/////////////////////////
	m_vVelocity = D3DXVECTOR3(0,0,0);

	m_fSpeed = 5.0f;

	m_bHit = false;	

	m_bRender = true;

	m_vOrientation = m_vPosition = m_vCenter = D3DXVECTOR3(0.0f,0.0f,0.0f);

	m_vScale = D3DXVECTOR3(0.75f,1.5f,1.0f);

	m_vPosition = D3DXVECTOR3(0.0f,0.0f,0.0f);

	HRESULT hr =	D3DXCreateTorus(D3DAPPI->GetD3DDevice(),3,15,16,16,&m_pMeshTorus,NULL);
	m_fRadius = 15.0f;

   	//setting up light
	mLightVecW     = D3DXVECTOR3(100.0, 150.0f, 0.0f);
	mDiffuseMtrl   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mDiffuseLight  = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mAmbientMtrl   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mAmbientLight  = D3DXCOLOR(0.6f, 0.6f, 0.6f, 1.0f);
	mSpecularMtrl  = D3DXCOLOR(0.8f, 0.8f, 0.8f, 1.0f);
	mSpecularLight = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
	mSpecularPower = 1.0f;

	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/goldring.jpg", &m_Texture);


	BuildFX();
	
	

	OnResetDevice();


}


D3DTorus::~D3DTorus(void)
{
}

void D3DTorus::CalculateInternals()
{

	//////////////
	//	ROTATION
	//////////////

	// a matrix to store the rotation information
	D3DXMATRIX matRotateY, matRotateX, matRotateZ;  

	
	 

	
	// build a matrix to rotate the model based on the increasing float value
    D3DXMatrixRotationX(&matRotateX, m_vOrientation.x);
	D3DXMatrixRotationY(&matRotateY, m_vOrientation.y);
	D3DXMatrixRotationZ(&matRotateZ, m_vOrientation.z);
	

	D3DXMATRIX rotationMatrix = matRotateX * matRotateY * matRotateZ;

	//////////////
	//	TRANSLATE
	//////////////

	D3DXMATRIX matTranslate;

	D3DXMatrixTranslation(&matTranslate,m_vPosition.x,m_vPosition.y,m_vPosition.z);

	//////////////
	//	SCALE
	//////////////

	D3DXMATRIX matScale;

	D3DXMatrixScaling(&matScale,m_vScale.x,m_vScale.y,m_vScale.z);

	///////////////
	//	TRANSFORM
	///////////////
	m_mTransform = rotationMatrix * matTranslate * matScale;


}

void D3DTorus::BuildFX()
{
	HRESULT hr;

	// Create the FX from a .fx file.
	ID3DXBuffer* errors = 0;
	hr = D3DXCreateEffectFromFileA(D3DAPPI->GetD3DDevice(), "Graphics/Techniques/ship.fx", 
		0, 0, D3DXSHADER_DEBUG, 0, &m_pEffect, &errors);
	if( errors ) 
		MessageBoxA(0, (char*)errors->GetBufferPointer(), 0, 0);	

	// Obtain handles.
	m_hTech = m_pEffect->GetTechniqueByName("ShipTech");
	m_hWVP  = m_pEffect->GetParameterByName(0, "gWVP");
	m_hTexture = m_pEffect->GetParameterByName(0,"gTex");
	m_hWorldInvTrans = m_pEffect->GetParameterByName(0, "gWorldInvTrans");
	m_hLightVecW     = m_pEffect->GetParameterByName(0, "gLightVecW");
	m_hDiffuseMtrl   = m_pEffect->GetParameterByName(0, "gDiffuseMtrl");
	m_hDiffuseLight  = m_pEffect->GetParameterByName(0, "gDiffuseLight");
	m_hAmbientMtrl   = m_pEffect->GetParameterByName(0, "gAmbientMtrl");
	m_hAmbientLight  = m_pEffect->GetParameterByName(0, "gAmbientLight");
	m_hSpecularMtrl  = m_pEffect->GetParameterByName(0, "gSpecularMtrl");
	m_hSpecularLight = m_pEffect->GetParameterByName(0, "gSpecularLight");
	m_hSpecularPower = m_pEffect->GetParameterByName(0, "gSpecularPower");
	m_hEyePos        = m_pEffect->GetParameterByName(0, "gEyePosW");
	m_hWorld         = m_pEffect->GetParameterByName(0, "gWorld");
		
	m_pEffect->SetTexture(m_hTexture,m_Texture);

	


}

void D3DTorus::Update(float dt)
{
	//Normalize velocity
	D3DXVec3Normalize(&m_vVelocity,&m_vVelocity);

	//scale the direction to speed
	D3DXVec3Scale(&m_vVelocity,&m_vVelocity,m_fSpeed);
	
	//movement update
	m_vPosition += m_vVelocity * dt;

	//rotate the ring
	m_vOrientation.z += .1 * dt;

	CollisionDet();
			
	CalculateInternals();

	if (m_vPosition.z >= 20.0f )
	{
		if (m_bHit == false)
		{
			Scoring->MissedRing();
			
		}

		//float x = RandInRange(-50,50);
		//float y = RandInRange(-50,50);
		//m_vPosition = D3DXVECTOR3(x,y,-2000);
		m_bHit = false;
		m_fSpeed += 3.0f;
	}
}

void D3DTorus::Render()
{
	if (m_bRender == true)
	{
		HRESULT hr;

	#if 1
		// Setup the rendering FX
		hr = m_pEffect->SetTechnique(m_hTech);
		hr = m_pEffect->SetMatrix(m_hWVP, &(m_mTransform * m_pCamView->GetViewProj()));

		D3DXMATRIX worldInvTrans;
		D3DXMatrixInverse(&worldInvTrans, 0, &m_mTransform);
		D3DXMatrixTranspose(&worldInvTrans, &worldInvTrans);
		m_pEffect->SetMatrix(m_hWorldInvTrans, &worldInvTrans);
		m_pEffect->SetValue(m_hLightVecW, &mLightVecW, sizeof(D3DXVECTOR3));
		m_pEffect->SetValue(m_hDiffuseMtrl, &mDiffuseMtrl, sizeof(D3DXCOLOR));
		m_pEffect->SetValue(m_hDiffuseLight, &mDiffuseLight, sizeof(D3DXCOLOR));
		m_pEffect->SetValue(m_hAmbientMtrl, &mAmbientMtrl, sizeof(D3DXCOLOR));
		m_pEffect->SetValue(m_hAmbientLight, &mAmbientLight, sizeof(D3DXCOLOR));
		m_pEffect->SetValue(m_hSpecularLight, &mSpecularLight, sizeof(D3DXCOLOR));
		m_pEffect->SetValue(m_hSpecularMtrl, &mSpecularMtrl, sizeof(D3DXCOLOR));
		m_pEffect->SetFloat(m_hSpecularPower, mSpecularPower);
		m_pEffect->SetMatrix(m_hWorld, &m_mTransform);
	#endif

		// Begin passes.
		UINT numPasses = 0;
		m_pEffect->Begin(&numPasses, 0);
		for(UINT i = 0; i < numPasses; ++i)
		{
			m_pEffect->BeginPass(i);
			// draw the spaceship
			m_pMeshTorus->DrawSubset(0);    // draw the subset
		
			m_pEffect->EndPass();
		}
		m_pEffect->End();
	
	
	}
}

void D3DTorus::OnResetDevice()
{
	m_pEffect->OnResetDevice();
	D3DXCreateTextureFromFile(D3DAPPI->GetD3DDevice(), L"Graphics/Textures/pyramid.jpg", &m_Texture);
}
void D3DTorus::OnLostDevice()
{
	m_pEffect->OnLostDevice();
}

bool D3DTorus::CollisionDet()
{
	if (m_bHit == false)
	{

		float x2, x1, y2, y1, z1, z2, r2, r1;
	
		x2 = m_vPlayerPos->x;
		x1 = m_vPosition.x;

		y2 = m_vPlayerPos->y;
		y1 = m_vPosition.y;

		z2 = m_vPlayerPos->z;
		z1 = m_vPosition.z;

		r2 = 5.0f;
		r1 = m_fRadius;

		if ( (x2 - x1 )*(x2-x1) + (y2 - y1)*(y2-y1) + (z2 - z1)*(z2 - z1) <= (r2+r1)*(r2 + r1) )
		{
			m_bHit = true;
			
			Scoring->HitRing();
			return true;
		}
	}

	return false;
}
