#pragma once
#include "D3DShape.h"



class D3DTorus : public D3DShape
{
private:

	/////////////////
	// COLLISION
	/////////////////
	D3DXVECTOR3 *m_vPlayerPos;
	float m_fRadius;

	bool m_bHit;
	bool m_bRender;

	///////////////////
	// Movement
	/////////////////
	D3DXVECTOR3 m_vVelocity;
	float m_fSpeed;

	virtual void CalculateInternals();

	LPD3DXMESH m_pMeshTorus;    // define the mesh pointer
	D3DMATERIAL9* m_pMaterial;    // define the material object
	DWORD m_wNumMaterials;    // stores the number of materials in the mesh
	
	D3DXVECTOR3 mLightVecW;		// light in world coords
	D3DXCOLOR   mAmbientMtrl;	// ambient mat color
	D3DXCOLOR   mAmbientLight;	// ambient light color
	D3DXCOLOR   mDiffuseMtrl;	// diffse mat color
	D3DXCOLOR   mDiffuseLight;	// diffuse light color
	D3DXCOLOR   mSpecularMtrl;	// specular mat color
	D3DXCOLOR   mSpecularLight;	// specular light color
	float       mSpecularPower;	// specular power



public:
	D3DTorus(void);
	~D3DTorus(void);


	///////////////
	// UTILITIES
	//////////////
	void BuildFX();
	virtual void Update(float dt);
	virtual void Render();
	virtual void OnResetDevice();
	virtual void OnLostDevice();

	bool CollisionDet();


	

	
	
	/////////////////////////
	// ACCESSORS
	////////////////////////
	D3DXVECTOR3 GetVelocity(){return m_vVelocity;}
	float GetSpeed(){return m_fSpeed;}

	///////////////////////
	// MUTATORS
	//////////////////////
	void SetVelocity(D3DXVECTOR3 vel){m_vVelocity = vel;}
	void SetVelocity(float x, float y, float z){m_vVelocity += D3DXVECTOR3(x,y,z);}
	void ChangeVelocityX(float x){m_vVelocity.x = x;}
	void ChangeVelocityY(float y){m_vVelocity.y = y;}
	void ChangeVelocityZ(float z){m_vVelocity.z = z;}
	void SetSpeed(float speed){m_fSpeed = speed;}
	void SetPlayerPos(D3DXVECTOR3 *pPos){m_vPlayerPos = pPos;}
};

