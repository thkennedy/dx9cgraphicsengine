
#include "MenuButton.h"
#include "../Graphics/D3DSprite.h"
#include "../Graphics/D3DText.h"
#include "../Graphics/Sprites/D3DSpriteManager.h"
#include "../Sound/SoundSys.h"

MenuButton::MenuButton(D3DSprite* buttonSprite, D3DText* buttonText)
{
	//id manager
	
	m_pButtonSprite = buttonSprite;
	//register sprite for rendering
	SMI->RegisterSprite(buttonSprite);

	m_pButtonText = buttonText;

	m_bHasSound = false;


}


MenuButton::~MenuButton(void)
{
	//perform on lost for font
	delete m_pButtonText;
}

bool MenuButton::OnClick()
{
	return 1;
}
bool MenuButton::OnScreen()
{
	return 1;
}
//no sound mouseover
bool MenuButton::MouseOver(POINT mousePos)
{
	//helper vars
	int upperX = m_pButtonSprite->GetPosition().x - (m_pButtonSprite->GetDimensions().x)/2, 
		lowerX = m_pButtonSprite->GetPosition().x + (m_pButtonSprite->GetDimensions().x)/2, 
		upperY = m_pButtonSprite->GetPosition().y - (m_pButtonSprite->GetDimensions().y)/2, 
		lowerY = m_pButtonSprite->GetPosition().y + (m_pButtonSprite->GetDimensions().y)/2;

	
	if (mousePos.x > upperX &&
		mousePos.x < lowerX &&
		mousePos.y > upperY &&
		mousePos.y < lowerY)
	{
		m_pButtonSprite->SetMouseOver(1);
		return 1;
	}
	else
	{
		m_pButtonSprite->SetMouseOver(0);
		return 0;
	}


	


	return 0;
}

//OVERLOAD with sound mouseover
bool MenuButton::MouseOver(POINT mousePos, SoundSys* sound)
{
	//helper vars
	int upperX = m_pButtonSprite->GetPosition().x - (m_pButtonSprite->GetDimensions().x)/2, 
		lowerX = m_pButtonSprite->GetPosition().x + (m_pButtonSprite->GetDimensions().x)/2, 
		upperY = m_pButtonSprite->GetPosition().y - (m_pButtonSprite->GetDimensions().y)/2, 
		lowerY = m_pButtonSprite->GetPosition().y + (m_pButtonSprite->GetDimensions().y)/2;

	
	if (mousePos.x > upperX &&
		mousePos.x < lowerX &&
		mousePos.y > upperY &&
		mousePos.y < lowerY)
	{
		m_pButtonSprite->SetMouseOver(1);
		//sound->play();
		return 1;
	}
	else
	{
		m_pButtonSprite->SetMouseOver(0);
		return 0;
	}


	


	return 0;
}


void MenuButton::Update(float time_elapsed)
{
	
	//if object has velocity, update the position
	if (m_vVelocity.MagnitudeSquared() > 0)
	{
		m_vPosition += m_vVelocity.ScaleReturn(time_elapsed);
		
	}

	SyncItems();

}

void MenuButton::Render()
{
	//only render it if its on the screen

	//sprite is already drawn by sprite manager, so just need to draw text
	m_pButtonText->Render();
}

void MenuButton::SyncItems()
{
	//sync up sprite and text with button
	m_pButtonSprite->SetPosition(D3DXVECTOR3(m_vPosition.x, m_vPosition.y, m_vPosition.z));
	m_pButtonText->SetPosition(m_vPosition);

}

void MenuButton::OnLostDevice()
{
	//perform on lost for font
	m_pButtonText->OnLostDevice();
}

void MenuButton::OnResetDevice()
{
	//perform onreset for font
	m_pButtonText->OnResetDevice();
}


//mutators
void MenuButton::SetPosition(Vector3 newPosition)
{
	m_vPosition = newPosition;
	SyncItems();
}