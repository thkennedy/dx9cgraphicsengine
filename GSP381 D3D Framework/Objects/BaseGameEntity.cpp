#include "BaseGameEntity.h"


BaseGameEntity::BaseGameEntity(void)
{
	m_vPosition = Vector3(0.0f,0.0f,0.0f); // set for the center of the world

	
}

BaseGameEntity::BaseGameEntity(Vector3 vPosition, bool bCanSleep)
	:m_vPosition(vPosition)
{
}

BaseGameEntity::~BaseGameEntity(void)
{
}

////////////////////////
// MUTATORS
///////////////////////

void BaseGameEntity::SetPosition(float x, float y, float z)
{
	m_vPosition.x = x;
	m_vPosition.y = y;
	m_vPosition.z = z; 
}

void BaseGameEntity::SetPosition(Vector3 vPosition)
{
	m_vPosition = vPosition;
}


