#include "D3DMenu.h"
#include "MenuButton.h"
#include "../Graphics/d3dUtility.h"
#include "../Graphics/D3DSprite.h"
#include "../Graphics/D3DText.h"
#include "../Sound/SoundSys.h"
#include "../Input/DirectInput.h"
#include "../Graphics/D3DApp.h"


D3DMenu::D3DMenu()
{
	m_iLeftOffscreen = -2000;
	m_iRightOffscreen = 2000;
	m_iUpOffscreen = -2000;
	m_iDownOffscreen = 2000;
	m_eDirection = NONE;
	m_vPosition = Vector3(0,0,0);
	m_eMenuJustification = JLEFT;

	m_sOnMouseOver->initialise();

	m_iMaxSpeed = 100;

	m_iLines = 0;
	
}

D3DMenu::~D3DMenu()
{
	//perform onlost for all buttons
	for (int i = 0; i < m_vMenuButtons.size(); i++)
	{
		delete m_vMenuButtons[i];
	}

	m_vMenuButtons.clear();
}



int D3DMenu::Update(float dt)
{
	
	//check mouseover
	m_iOverButton = MouseOver(gDInput->GetMousePos());

	//seek target
	
	Seek(m_vSeekTarget);

	//update position if there's any velocity
	if (m_vVelocity.MagnitudeSquared() > 1.5)
	{
		m_vPosition += m_vVelocity.ScaleReturn(dt);
		
	}
	//sync buttons positions 
	SyncButtons();

	//add in force accumulators
	m_vVelocity += m_vForceAccum.ScaleReturn(dt);
	
	//clear forceaccum
	m_vForceAccum.Clear();
	
	
	return m_iOverButton;
	

	
}

void D3DMenu::Render()
{
	//render the buttons
	std::vector<MenuButton*>::iterator it = m_vMenuButtons.begin();

	for (it; it != m_vMenuButtons.end(); it++)
	{
		(*it)->Render();
	}
}

void D3DMenu::SyncButtons()
{
	//syncs the buttons to a relative position with the menu objects's position.
	std::vector<MenuButton*>::iterator it = m_vMenuButtons.begin();

	for (it; it != m_vMenuButtons.end(); it++)
	{
		(*it)->SetPosition(m_vPosition + (*it)->GetOffset());
	}
}

int D3DMenu::MouseOver(POINT mousePosition)
{
	//iterate through buttons to see if any has mouse over it
	std::vector<MenuButton*>::iterator it = m_vMenuButtons.begin();
	
	bool overButton;
	
	
	for (it; it != m_vMenuButtons.end(); it++)
	{
		//trips logic if its over 
		overButton = (*it)->MouseOver(mousePosition);
		
		//returns the id of the button the mouse is over
		if (overButton)
			return (*it)->GetID();

	}

	return -1;
}

void D3DMenu::Seek(Vector3 target)
{
	//arrive algorithm from AI

	//dist to target position
	Vector3 toTarget;

	toTarget = m_vSeekTarget - m_vPosition;

	//find dist to target
	float dist = toTarget.Magnitude();

	//only calc if we're far away from target
	if (dist > 1)
	{
		const double decelerationTweaker = 0.3;

		double speed = dist / ((double)2 * decelerationTweaker);

		//make sure we dont' go too fast

		if (speed > m_iMaxSpeed)
			speed = m_iMaxSpeed;
		
		//calc desired velocity
		toTarget.Normalize();
		Vector3 desiredVelocity = toTarget.ScaleReturn(speed);

		//assign velocity
		m_vForceAccum = desiredVelocity - m_vVelocity;
	}
	else
	{
		m_vForceAccum.Clear();
		m_vVelocity.Clear();
	}

	
}

void D3DMenu::AddButton(MenuButton* newButton) 
{
	//create button offset in relation to the justification
	int screenWidth = D3DAPPI->m_D3Dpp.BackBufferWidth;
	int screenHeight = D3DAPPI->m_D3Dpp.BackBufferHeight;
	
	int xOffset;
	//case on justification
	switch(m_eMenuJustification)
	{
	case JLEFT:
		xOffset = screenWidth / 3;
		break;
	case JRIGHT:
		xOffset = screenWidth * 2 / 3;
		break;
	case JMIDDLE:
		xOffset = screenWidth / 2;
		break;
	}

	int yOffset;
	//case on number of buttons for y offset
	switch(m_vMenuButtons.size())
	{
	//no buttons - put in middle
	case 0:
		newButton->SetOffset(Vector3(xOffset,screenHeight /2,0));
		break;
	//already one button, move first button to 2/8, add second at 6/8
	case 1:
		//move button 1 to 2/8
		m_vMenuButtons[0]->SetOffset(Vector3(xOffset,(screenHeight/4),0));

		//set to 6/8
		
		newButton->SetOffset(Vector3(xOffset,screenHeight * 6 / 8,0));
		break;
	//two buttons present, move button 2 to 1/2 and add new button at 6/8
	case 2:
		//move button 2 to 1/2
		m_vMenuButtons[1]->SetOffset(Vector3(xOffset,(screenHeight/2),0));

		//add newbutton to 6/8
		yOffset = screenHeight * 6 / 8;
		newButton->SetOffset(Vector3(xOffset,yOffset,0));
		break;
	
		//three buttons present button 1 at 1/8, button 2 at 3/8 and 3rd button on 5/8, 4th buttons at 7/8
	case 3:
		//move button 1 to 1/8
		m_vMenuButtons[0]->SetOffset(Vector3(xOffset,(screenHeight/8),0));
		//move button 2 to 3/8
		m_vMenuButtons[1]->SetOffset(Vector3(xOffset,(screenHeight * 3 / 8),0));
		//move button 3 to 5/8
		m_vMenuButtons[2]->SetOffset(Vector3(xOffset,(screenHeight * 5 / 8),0));
		//new button adds at 7/8
		newButton->SetOffset(Vector3(xOffset,(screenHeight * 7 / 8), 0));
		

		break;
	default:
		break;
	}
	 
	m_vMenuButtons.push_back(newButton);
}

void D3DMenu::AddText(MenuButton* newButton)
{
	m_iLines++;

	//create button offset in relation to the justification
	int screenWidth = D3DAPPI->m_D3Dpp.BackBufferWidth;
	int screenHeight = D3DAPPI->m_D3Dpp.BackBufferHeight;
	
	int xOffset;
	//case on justification
	switch(m_eMenuJustification)
	{
	case JLEFT:
		xOffset = screenWidth / 3;
		break;
	case JRIGHT:
		xOffset = screenWidth * 2 / 3;
		break;
	case JMIDDLE:
		xOffset = screenWidth / 2;
		break;
	}

	newButton->SetOffset(Vector3(xOffset, screenHeight * m_iLines / 8,0));

	m_vMenuButtons.push_back(newButton);

}
void D3DMenu::CreateButton(HWND hWnd, LPDIRECT3D9 d3dObj, LPDIRECT3DDEVICE9 d3dDevice, ID3DXSprite* spriteObj,
						LPCTSTR NewTexture, D3DXVECTOR3 vSpriteCenter, LPCSTR text, int buttonID,
						Vector3 buttonPosition)
{
	//create a sprite, open set its texture
	D3DSprite *temp = new D3DSprite(d3dObj,d3dDevice,spriteObj);

	//give it a texture
	temp->SetTexture(NewTexture);

	//create a temp text object
	D3DText *tempText = new D3DText(hWnd, d3dObj,d3dDevice);

	//set text for object
	tempText->SetText(text);

	//put it into a button
	MenuButton* tempButton = new MenuButton(temp,tempText);

	//give the button an ID
	tempButton->SetID(buttonID);

	AddButton(tempButton);
}

void D3DMenu::CreateText(LPCSTR text, Justification justify)
{
	//create a sprite, open set its texture
	D3DSprite *temp = new D3DSprite(D3DAPPI->GetD3DObject(),D3DAPPI->GetD3DDevice(),D3DAPPI->GetD3DSpriteObject());

	//give it a texture
	temp->SetTexture(NULL);

	//create a temp text object
	D3DText *tempText = new D3DText(D3DAPPI->GetHWND(),D3DAPPI->GetD3DObject(),D3DAPPI->GetD3DDevice());

	//set text for object
	tempText->SetText(text);

	//put it into a button
	MenuButton* tempButton = new MenuButton(temp,tempText);

	//give the button an ID
	tempButton->SetID(-1);

	AddText(tempButton);
}

void D3DMenu::OffscreenLeft()
{
	//set menu start position
	m_vPosition = Vector3(0,0,0);

	//set direction var 
	m_eDirection = LEFT;

	//set target X and Y
	m_vSeekTarget = Vector3(m_iLeftOffscreen,0,0);
	
}
void D3DMenu::OffscreenRight()
{

	//set menu start position
	m_vPosition = Vector3(0,0,0);

	//set direction var 
	m_eDirection = RIGHT;

	//set target X and Y
	m_vSeekTarget = Vector3(m_iRightOffscreen,0,0);

}
void D3DMenu::OffscreenUp()
{
	//set menu start position
	m_vPosition = Vector3(0,0,0);

	//set direction var 
	m_eDirection = UP;

	//set target X and Y
	m_vSeekTarget = Vector3(0,m_iUpOffscreen,0);
}
void D3DMenu::OffscreenDown()
{
	//set menu start position
	m_vPosition = Vector3(0,0,0);

	//set direction var 
	m_eDirection = DOWN;

	//set target X and Y
	m_vSeekTarget = Vector3(0,m_iDownOffscreen,0);
}

void D3DMenu::OnscreenLeft()
{
	//set menu start position
	m_vPosition = Vector3(m_iLeftOffscreen,0,0);

	//set direction var 
	m_eDirection = LEFT;

	//set target X and Y
	m_vSeekTarget = Vector3(0,0,0);
}
void D3DMenu::OnscreenRight()
{
	//set menu start position
	m_vPosition = Vector3(m_iRightOffscreen,0,0);

	//set direction var 
	m_eDirection = RIGHT;

	//set target X and Y
	m_vSeekTarget = Vector3(0,0,0);
}
void D3DMenu::OnscreenUp()
{
	//set menu start position
	m_vPosition = Vector3(0,m_iUpOffscreen,0);

	//set direction var 
	m_eDirection = UP;

	//set target X and Y
	m_vSeekTarget = Vector3(0,0,0);
}
void D3DMenu::OnscreenDown()
{
	//set menu start position
	m_vPosition = Vector3(0,m_iDownOffscreen,0);

	//set direction var 
	m_eDirection = DOWN;

	//set target X and Y
	m_vSeekTarget = Vector3(0,0,0);
}

void D3DMenu::OnLostDevice()
{
	//perform onlost for all buttons
	for (auto it = m_vMenuButtons.begin(); it != m_vMenuButtons.end(); it++)
	{
		(*it)->OnLostDevice();
	}
}

void D3DMenu::OnResetDevice()
{
	//perform onreset for all buttons
	for (auto it = m_vMenuButtons.begin(); it != m_vMenuButtons.end(); it++)
	{
		(*it)->OnResetDevice();
	}
	//set new offsets

	//create button offset in relation to the justification
	int screenWidth = D3DAPPI->m_D3Dpp.BackBufferWidth;
	int screenHeight = D3DAPPI->m_D3Dpp.BackBufferHeight;
	
	int xOffset;
	//case on justification
	switch(m_eMenuJustification)
	{
	case JLEFT:
		xOffset = screenWidth / 3;
		break;
	case JRIGHT:
		xOffset = screenWidth * 2 / 3;
		break;
	case JMIDDLE:
		xOffset = screenWidth / 2;
		break;
	}

	int yOffset;
	//case on number of buttons for y offset
	
	//case on number of buttons for y offset
	switch(m_vMenuButtons.size())
	{
	//no buttons - do nothing
	case 0:
		break;
	// only 1 button, but it in middle
	case 1:
		m_vMenuButtons[0]->SetOffset(Vector3(xOffset,screenHeight /2,0));
		
		break;
	//two buttons present, move button 2 to 1/2 and add new button at 6/8
	case 2:
		// button 1 to 2/8
		m_vMenuButtons[0]->SetOffset(Vector3(xOffset,(screenHeight/4),0));

		//set to 6/8
		yOffset = screenHeight * 6 / 8;
		m_vMenuButtons[1]->SetOffset(Vector3(xOffset,yOffset,0));
		break;
	//three buttons 
	case 3:
		// button 1 to 2/8
		m_vMenuButtons[0]->SetOffset(Vector3(xOffset,(screenHeight/4),0));

		//button 2 to 1/2
		// button 1 to 2/8
		m_vMenuButtons[1]->SetOffset(Vector3(xOffset,(screenHeight/2),0));

		//button 3 to 6/8
		yOffset = screenHeight * 6 / 8;
		m_vMenuButtons[2]->SetOffset(Vector3(xOffset,screenHeight * 6 / 8,0));

		break;
	case 4:
		//move button 1 to 1/8
		m_vMenuButtons[0]->SetOffset(Vector3(xOffset,(screenHeight/8),0));
		//move button 2 to 3/8
		m_vMenuButtons[1]->SetOffset(Vector3(xOffset,(screenHeight * 3 / 8),0));
		//move button 3 to 5/8
		m_vMenuButtons[2]->SetOffset(Vector3(xOffset,(screenHeight * 5 / 8),0));
		//new button adds at 7/8
		m_vMenuButtons[3]->SetOffset(Vector3(xOffset,(screenHeight * 7 / 8),0));
		break;
	default:
		break;
	}

}

void D3DMenu::OnResetDeviceT()
{
	for (auto it = m_vMenuButtons.begin(); it != m_vMenuButtons.end(); it++)
	{
		(*it)->OnResetDevice();
	}

	//set new offsets

	//create button offset in relation to the justification
	int screenWidth = D3DAPPI->m_D3Dpp.BackBufferWidth;
	int screenHeight = D3DAPPI->m_D3Dpp.BackBufferHeight;
	
	int xOffset;
	//case on justification
	switch(m_eMenuJustification)
	{
	case JLEFT:
		xOffset = screenWidth / 3;
		break;
	case JRIGHT:
		xOffset = screenWidth * 2 / 3;
		break;
	case JMIDDLE:
		xOffset = screenWidth / 2;
		break;
	}

	for (int i = 0; i < m_vMenuButtons.size(); i++)
	{
		m_vMenuButtons[i]->SetOffset(Vector3(xOffset,screenHeight * (i+1) / 8, 0));
	}
}

void D3DMenu::SetPosition(Vector3 newPos) 
{
	m_vPosition = newPos;

	m_vSeekTarget = newPos;

	SyncButtons();
}