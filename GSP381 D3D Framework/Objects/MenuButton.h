#pragma once



#include <d3d9.h>
#include <d3dx9.h>
#include "../Physics/functions.h"
#include "DXBaseObject.h"
#include "../Physics/Vector3.h"

class D3DSprite;
class D3DText;
class SoundSys;


#pragma comment(lib, "winmm.lib")

// include the Direct3D Library file
#pragma comment (lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

class MenuButton : 	
	public DXBaseObject
{
private:
	

	////////////////////////////////
	//Button graphical vars
	///////////////////////////////
	D3DSprite*	m_pButtonSprite;
	D3DText*	m_pButtonText;

	////////////////////////////////
	//Button internal vars
	///////////////////////////////
	Vector3	m_vPosition;
	Vector3 m_vVelocity;
	int m_iButtonID;
	//for use in menus to get local offset
	Vector3 m_vOffset;

	////////////////////////////////
	//Sounds (optional)
	///////////////////////////////
	bool m_bHasSound;
	SoundSys *m_sOnMouseOver, *m_sOnClick;
		
public:
	MenuButton(){}
	MenuButton(D3DSprite* buttonSprite, D3DText* buttonText);
	~MenuButton(void);


	//utilities
	bool OnClick();
	bool OnScreen();
	bool MouseOver(POINT mousePos);
	bool MouseOver(POINT mousePos, SoundSys* sound);

	void Update(float time_elapsed);
	void Render();

	void SyncItems();

	void OnLostDevice();
	void OnResetDevice();

	//accessors
	Vector3 GetPosition(){return m_vPosition;}
	int GetID() {return m_iButtonID;}
	Vector3 GetOffset() {return m_vOffset;}


	//mutators
	void SetPosition(Vector3 newPosition);
	void SetVelocity(Vector3 newVelocity){m_vVelocity = newVelocity;}
	void SetID(int newID){m_iButtonID = newID;}
	void SetOffset(Vector3 offset){m_vOffset = offset;}



};

