#pragma once
#include "../Graphics/d3dUtility.h"
#include "../Objects/MenuButton.h"

#pragma warning(disable : 4995)


enum MenuDirection
{
	NONE,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

enum Justification
{
	JLEFT,
	JRIGHT,
	JMIDDLE
};


class SoundSys;


///////////////////////////////////////////////////
//Menu Object - has buttons and an orientation
//	for easy menu switching
//////////////////////////////////////////////////
class D3DMenu
{
private:
	std::vector<MenuButton*> m_vMenuButtons;
	Vector3 m_vPosition, m_vVelocity, m_vSeekTarget, m_vForceAccum;
	int m_iLeftOffscreen, m_iRightOffscreen, m_iUpOffscreen, m_iDownOffscreen, targetX, targetY;
	int m_iOverButton;	
	MenuDirection m_eDirection;
	Justification m_eMenuJustification;
	int m_iMaxSpeed;
	SoundSys *m_sOnMouseOver;

	////////////////
	// credits stuff
	///////////////

	int m_iLines;

public:
	D3DMenu();
	~D3DMenu();
	
	
	//////////////////////////////////////////////////////////////////////////
	// Utilities
	//////////////////////////////////////////////////////////////////////////
	int Update(float dt);
	void Render();
	void SyncButtons();
	int MouseOver(POINT mousePosition);
	void Seek(Vector3 target);


	void AddButton(MenuButton* newButton);
	void AddText(MenuButton* newButton);
	void CreateButton(HWND hWnd, LPDIRECT3D9 d3dObj, LPDIRECT3DDEVICE9 d3dDevice, ID3DXSprite* spriteObj,
						LPCTSTR NewTexture, D3DXVECTOR3 vSpriteCenter, LPCSTR text, int buttonID,
						Vector3 buttonPosition); 

	void CreateText(LPCSTR text, Justification justify);
	void OffscreenLeft();
	void OffscreenRight();
	void OffscreenUp();
	void OffscreenDown();

	void OnscreenLeft();
	void OnscreenRight();
	void OnscreenUp();
	void OnscreenDown();

	void OnLostDevice();
	void OnResetDeviceT();
	void OnResetDevice();

	
	//////////////////////////////////////////////////////////////////////////
	// Accessors
	//////////////////////////////////////////////////////////////////////////
	Vector3 GetPosition() {return m_vPosition;}
	Vector3 GetVelocity() {return m_vVelocity;}



	
	//////////////////////////////////////////////////////////////////////////
	// Mutators
	//////////////////////////////////////////////////////////////////////////
	void SetPosition(Vector3 newPos);
	void SetVelocity(Vector3 newVel) { m_vVelocity = newVel;}
	void SetMouseOverSound(SoundSys* sound) { m_sOnMouseOver = sound;}
	void SetJustification(Justification newValue){m_eMenuJustification = newValue;}
	void SetMaxSpeed(int maxSpeed){m_iMaxSpeed = maxSpeed;}
	void SetSeekTarget(Vector3 target){m_vSeekTarget = target;}
	
};

