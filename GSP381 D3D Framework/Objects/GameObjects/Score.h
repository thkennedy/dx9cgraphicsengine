#pragma once

#include "../../Graphics/d3dUtility.h"

class SoundEffect;

#define Scoring Score::Instance()

class Score
{
private:
	Score();
	
    ID3DXFont* mFont;
    float mScore;
    int mRingsHit;
	int mRingsMissed;
	float mMultiplier;
	SoundEffect* m_sHitRing;
	SoundEffect* m_sMissRing;
	
public:
	
	static Score* Instance();
    ~Score();
    void onLostDevice();
    void onResetDevice();
	
	void Init();

	void HitRing();
    void MissedRing();
    
    

    void Update(float dt);
    void Render();


};

