#include "../../Graphics/D3DApp.h"
#include "../../Sound/SoundEffect.h"
#include "Score.h"
#include <tchar.h>

Score* Score::Instance()
{
	static Score instance;

	return &instance;
}

Score::Score()
: mFont(0)
{
	D3DXFONT_DESC fontDesc;
	fontDesc.Height          = 24;
    fontDesc.Width           = 0;
    fontDesc.Weight          = 0;
    fontDesc.MipLevels       = 1;
    fontDesc.Italic          = false;
    fontDesc.CharSet         = DEFAULT_CHARSET;
    fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
    fontDesc.Quality         = DEFAULT_QUALITY;
    fontDesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
    _tcscpy(fontDesc.FaceName, _T("Times New Roman"));

	D3DXCreateFontIndirect(D3DAPPI->GetD3DDevice(), &fontDesc, &mFont);

	mScore = mRingsHit = mRingsMissed =  0;

	mMultiplier = 1.0f;

	m_sHitRing = new SoundEffect();
	m_sHitRing->initialise();
	m_sHitRing->load("Sound/ring.wav");

	m_sMissRing = new SoundEffect();
	m_sMissRing->initialise();
	m_sMissRing->load("Sound/doh.mp3");
}

Score::~Score()
{
	ReleaseCOM(mFont);

	delete m_sHitRing;
	delete m_sMissRing;
}

void Score::onLostDevice()
{
	mFont->OnLostDevice();
}

void Score::onResetDevice()
{
	mFont->OnResetDevice();
}

void Score::Init()
{
	mScore = mRingsHit = mRingsMissed =  0;

	mMultiplier = 1.0f;
}

void Score::HitRing()
{
	m_sHitRing->play();

	mRingsHit++;

	mScore += 100 * mMultiplier;

	mMultiplier += .5;
}

void Score::MissedRing()
{
	m_sMissRing->play();
	
	mRingsMissed += 1;

	mMultiplier = 1.0f;
}


void Score::Update(float dt)
{
	
	
}

void Score::Render()
{
	// Make static so memory is not allocated every frame.
	static char buffer[256];

	sprintf_s(buffer, "Player Score = %.2f\n"
		"Rings Hit: %i\n"
		"Rings Missed: %i\n"
		"Multiplier  %.2f X", mScore, mRingsHit, mRingsMissed, mMultiplier);

	RECT R = {D3DAPPI->GetD3Dpp().BackBufferHeight * 7/8, D3DAPPI->GetD3Dpp().BackBufferWidth * .5, 0, 0};
	//getting display rect
	
	mFont->DrawTextA(NULL, buffer,-1,&R,DT_CALCRECT, D3DCOLOR_XRGB(255,255,255));
	mFont->DrawTextA(NULL, buffer, -1, &R, DT_NOCLIP, D3DCOLOR_XRGB(255,255,255));
}