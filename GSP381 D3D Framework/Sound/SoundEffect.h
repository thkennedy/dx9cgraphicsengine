#pragma once

#include <fmod.hpp>



#pragma comment(lib, "fmodex_vc.lib")

class SoundEffect
{
public:
	private:
     bool on; //is sound on?
     bool possible; //is it possible to play sound?
     char * currentSound; //currently played sound
    //FMOD-specific stuff
     FMOD_RESULT result;
     FMOD_SYSTEM * fmodsystem;
     FMOD_SOUND * sound;
     FMOD_CHANNEL * channel;

public:
	SoundEffect();
	~SoundEffect();

     void initialise (void); //initialises sound

    //sound control
     void setVolume (float v); //sets the actual playing sound's volume
     void load (const char * filename); //loads a soundfile
     void unload (void); //frees the sound object
     void play (bool pause = false); //plays a sound (may be started paused; no argument for unpaused)
	 void playLooped (bool Pause = false); //plays sound looped
    //getters
     bool getSound (void); //checks whether the sound is on

    //setters
     void setPause (bool pause); //pause or unpause the sound
     void setSound (bool sound); //set the sound on or off

    //toggles
     void toggleSound (void); //toggles sound on and off
     void togglePause (void); //toggle pause on/off
};



