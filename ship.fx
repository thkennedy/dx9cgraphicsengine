//=============================================================================
// Ship.fx by Tim Kennedy
//
// 
// 
//=============================================================================

uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInvTrans;
uniform extern float4x4 gWVP;

uniform extern float4 gAmbientMtrl;
uniform extern float4 gAmbientLight;
uniform extern float4 gDiffuseMtrl;
uniform extern float4 gDiffuseLight;
uniform extern float4 gSpecularMtrl;
uniform extern float4 gSpecularLight;
uniform extern float  gSpecularPower;
uniform extern float3 gLightVecW;
uniform extern float3 gEyePosW;
uniform extern texture gTex;
	
//my sampler
sampler TexS = sampler_state
{
	Texture = <gTex>;
	MinFilter = Anisotropic;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	MaxAnisotropy = 8;
	AddressU  = WRAP;
    AddressV  = WRAP;
};


//my vert shader declaration
struct OutputVS
{
    float4 posH		: POSITION0;
	float2 tex0		: TEXCOORD0;
	float3 normalW	: TEXCOORD1;
	float3 posW		: TEXCOORD2;
	float3 toEyeW	: TEXCOORD3;
};

//vert shader method
OutputVS ShipVS(float3 posL : POSITION0, float3 normalL : NORMAL, float2 tex0: TEXCOORD0)
{
    // Zero out our output.
	OutputVS outVS = (OutputVS)0;

	// Transform normal to world space.
	float3 normalW = mul(float4(normalL, 0.0f), gWorldInvTrans).xyz;
	normalW = normalize(normalW);
	
	// Transform vertex position to world space.
	float3 posW  = mul(float4(posL, 1.0f), gWorld).xyz;

	
	outVS.posW = posW;
	outVS.normalW = normalW;
	outVS.toEyeW = normalize(gEyePosW - posW);
	
	// Transform to homogeneous clip space.
	outVS.posH = mul(float4(posL, 1.0f), gWVP);

	// pass on texture coords
	outVS.tex0 = tex0;
	 
	// Done--return the output.
    return outVS;
}


//pixel shader method
float4 ShipPS(OutputVS inVS) : COLOR
{
	float3 texColor = tex2D(TexS, inVS.tex0).rgb;
	
	//=======================================================
	// Compute the color: Equation 10.3.
	
	// Compute the vector from the vertex to the eye position.
	float3 toEye = normalize(gEyePosW - inVS.posW);
	
	// Compute the reflection vector.
	float3 r = reflect(-gLightVecW, inVS.normalW);
	
	// Determine how much (if any) specular light makes it into the eye.
	float t  = pow(max(dot(r, toEye), 0.0f), gSpecularPower);
	
	// Determine the diffuse light intensity that strikes the vertex.
	float s = max(dot(gLightVecW, inVS.normalW), 0.0f);
	
	// Compute the ambient, diffuse and specular terms separatly. 
	float3 spec = t*(gSpecularMtrl*gSpecularLight).rgb;
	float3 diffuse = s*(gDiffuseMtrl*gDiffuseLight).rgb;
	float3 ambient = gAmbientMtrl*gAmbientLight;
	
	// Sum all the terms together and copy over the diffuse alpha.
	float4 color;
	color.rgb = ambient + diffuse;
	color.a   = gDiffuseMtrl.a;
	spec = float4(spec, 0.0f);
	//=======================================================

	color.rgb = color.rgb * texColor;
    return float4(color.rgb + spec.rgb, color.a);	
}

static float3 attenuation = float3(0.0f, 0.5f, 0.0f);
float4	BlinnPhongPS(OutputVS inVS)	: COLOR
{
	float4 texColor = tex2D(TexS, inVS.tex0);

	float3 lightDir = normalize(gLightVecW - inVS.posW);
	
	// Blinn Phong calculation
	float3 h = normalize(lightDir + normalize(inVS.toEyeW));
	float t = pow(max(dot(h, inVS.normalW), 0.0f), gSpecularPower);
	
	float s = max(dot(lightDir, inVS.normalW), 0.0f);

	float3 spec = t * (gSpecularMtrl * gSpecularLight).rgb;
	float3 diffuse = s * (gDiffuseMtrl*gDiffuseLight).rgb;
	float3 ambient = (gAmbientMtrl * gAmbientLight);

	float d = distance(gLightVecW, inVS.posW);
	float a = attenuation.x + attenuation.y*d + attenuation.z*d*d;

	// Add lighting and texture color
	float3 color = ((diffuse+spec)*(texColor.rgb+ 1/a) + ambient);
	return float4(color, texColor.a * gDiffuseMtrl.a);
}

technique ShipTech
{
    pass P0
    {
		Lighting = TRUE;
		//FillMode = WireFrame;

        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_3_0 ShipVS();
        pixelShader  = compile ps_3_0 BlinnPhongPS();	//ShipPS();

	
    }
}
